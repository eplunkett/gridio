# Function to convert x and y data to weights
densityweight <- function(x, cellsize, make.grid=TRUE){
# Arguments:
#	x - a dataframe with x and y columns (other columns can be included) # must be projected 

	xo <- (min(x$x) %/% cellsize) * cellsize
	yo <- (min(x$y) %/% cellsize) * cellsize
	rc <- matrix(NA, nrow=nrow(x), ncol=2)
	rc[,1] <- (x$y - yo) %/% cellsize + 1  
	nrow <- max(rc[,1])
	rc[,1] <- nrow-rc[,1] + 1  # this makes the northernmost row the first instead of the last
	rc[,2] <- (x$x - xo) %/% cellsize +1
	ncol <- max(rc[,2])
	
   rc <- paste(rc[,1], rc[,2])
   counts <- as.matrix(table(rc))
   weights <- data.frame(id=rownames(counts), weight=1/as.numeric(counts))
   rc <- data.frame(id=rc, tempid=1:length(rc))
   x <- cbind(x, rc)
   x <- merge(x, weights, by="id")
   x <- x[order(x$tempid),  ]
  
 
  x <- x[,!names(x) %in% c("tempid", "id")]
  counts <- as.data.frame(counts)
  
  if(make.grid){
    names(counts) <- "count"
    counts$row <- as.numeric(sapply(rownames(counts), FUN=function(x) strsplit(x, " ")[[1]][1]))
    counts$col <- as.numeric(sapply(rownames(counts), FUN=function(x) strsplit(x, " ")[[1]][2]))
    g <- list(m=matrix(0, nrow=nrow, ncol=ncol), nrow=nrow, ncol=ncol, xll=xo, yll=yo, cellsize=cellsize)
    class(g) <- c("grid", class(g))
    g$m[as.matrix(counts[, c("row", "col")])] <- counts$count
    return(list(data=x, grid=g))
  } 
  return(list(data=x))
}
