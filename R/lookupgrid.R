lookupgrid <- function(source, d){
  # Called from readblockc
  # Function to return the row index of the source grid in the cachedata d
  # d is a table with the standard gridcache info  (source	cache	status	cachedate	sourcedate	reads	size)
  # source is the path to the grid 
  # case of both has already been sanatized
  
  #  1. If any ready versions of the grid last ready instance
  #  2. If no ready versions and one or more copying return the last copying instance
  #  Otherwise return NA
  #
  source <- tolower(source)
  dsource <- tolower(d$source)
  this.grid <- dsource == source
  is.ready <- d$status == "ready"
  is.copying <- d$status == "copying"
  
  sv <- this.grid & is.ready
  if(any(sv)){
    a <- which(sv)
    return(a[length(a)])
  }
  
  sv <- this.grid & is.copying
  if(any(sv)){
    a <- which(sv)
    return(a[length(a)])
  }
  return(NA)
}