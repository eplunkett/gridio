if(FALSE){
  
  # Compare mosaics
  grid <- "X:/LCC/GIS/Final/NER/caps_phase5/rawsettings/impervious_m"
  tif <- "X:/LCC/GIS/Final/NER/caps_phase5/rawsettings/impervious_tm"
  verifytifcopy(grid, tif)
  
  # Compare grids
  grid <- "X:/LCC/GIS/Final/NER/caps_phase5/rawsettings/impervious_m/impervious_03/impervious_03"
  tif <- "X:/LCC/GIS/Final/NER/caps_phase5/rawsettings/impervious_tm/impervious_tm_03.tif"
 
   verifytifcopy()
  
}


verifyconversion <- function(grid, tif, err = FALSE){
  #  This function is to verify whether the data in a grid  is  preserved in 
  #   a  tif copy. In our testing copying with both gdal_translate and 
  #  arcpy can result in corrupted no data values. This reads the grid
  # with gridio and the tif with terra (backed by gdal) and compares the result.
  # Arguments:
  #   grid : the grid or mosaic that was copied
  #   tif  : the tif copy  of the grid  or mosaic
  #   err : if TRUE throw an error if there's a  problem otherwise return a list indicating what happened.
  # Value : Either a list (for single  rasters)  or a data.frmame (for mosaics)  with these items or columns:
  #   problem : TRUE if the tif isn't identical to the grid
  #   NA.only : TRUE if the only difference is that NA values in the grid have value  in the tif.
  #   NA.out :  If NA.only then this is the value that NA was converted to.
  #   If the case of of a mosaic it will be a data.frame with one row per pane.
  
  gridinit()
  gd <- griddescribe(grid)
  
  if(gd$mosaic){
    # If it's a mosaic compare panes one by one
    panemap <- gd$mosaicinfo$panemap
    panes <- which(panemap)
    
    td <- griddescribe(tif)
    stopifnot(td$mosaic)  #   if grid is mosaic tiff should  be too
    stopifnot(grepl("_tif$", td$mosaicinfo$type))   #  verify that tif is a tif mosaic
    stopifnot(! grepl("_tif$", gd$mosaicinfo$type))  # and grid  is  a  grid  mosaic
    stopifnot(isTRUE(all.equal(panes, which(td$mosaicinfo$panemap))))
    
    n  <- length(panes)
    result <- data.frame(problem = rep(NA, n), NA.only = rep(NA, n), NA.out = rep(NA, n))
    rownames(result)  <- paste0("Pane ", panes)
    
    for(i in seq_along(panes)){
      p <- panes[i]
      if(.verbosity() > 0)  
        cat("Comparing pane ", p, " (", i, " of ", length(panes), ")\n", sep ="")
      s <- panepath(grid, gd$mosaicinfo$name, p, length(panemap), tiff = FALSE)
      d <- panepath(tif, td$mosaicinfo$name, p, length(panemap), tiff = TRUE)
      r <- verifyconversion(s, d)
      for(col in colnames(result)){
        result[[col]][i] <- r[[col]]
      }
    }  # end pane comparison loop
    
    if(err &  any(result$problem)){
      # Assemble error message:
      message <- "Copy is not identical."
      sv <- result$NA.only
      if(any(sv))
        message <- paste0(message, "  Only lost NA - pane (new value):",  paste(paste0(panes[sv], "(", result$NA.out[sv], ")"), collapse  =", ") )
      sv2 <- is.na(result$problem) |  result$problem  & !sv
      if(any(sv2)){
        message <- paste0(message, " Panes where data didn't match: ", paste(panes[sv2], collapse  = ",  ") )
      }
      stop(message)
    }
    
    return(result)  # mosaic return 
  } # end if mosaic
  
  setwindow(grid) 
  
  d <- terra::rast(tif)
  
  # Check for matching extent
  stopifnot(gd$ncol == terra::ncol(d))
  stopifnot(gd$nrow == terra::nrow(d))
  rd <- terra::res(d)
  stopifnot(isTRUE(all.equal(gd$cellsize, rd[1] )))
  stopifnot(isTRUE(all.equal(gd$cellsize,  rd[2] )))
  stopifnot(isTRUE(all.equal(gd$xll, terra::xmin(d) )))
  stopifnot(isTRUE(all.equal(gd$yll, terra::ymin(d) )))  
  rm(rd)
  
  # Check for matching data
  # I'm going to read complete lines
  colsize <- gd$ncol
  max.cells <- 3000^2
  rowsize <- floor(max.cells/colsize)
  
  result <- list(problem = FALSE, NA.only = FALSE, NA.out = NA)
  
  tileinit(rowsize = rowsize, colsize = colsize, buffer = 0) 
  if(.status$verbosity > 0)
    cat("Comparing grid and tif files\n")
  for(i in 1:ntiles()){
    settile(i)
    if(.status$verbosity > 0) cat(".")
    a <-  readtile(grid)
    av <- as.numeric(t(a$m))
    
    td  <- tiledetails(coreonly = TRUE)
    bv <- terra::values(d, mat = FALSE, row = td$startrow, col = td$startcol, nrows = td$nrow,  ncols = td$ncol)
    if(!isTRUE(all.equal(av,  bv))){
      result$problem <- TRUE
      sv <- is.na(av) & !is.na(bv) # select cells with lost NA encoding
      if(isTRUE(all.equal(av[!sv], bv[!sv]))){  #  if all non-na values are preserved
        result$NA.only <- TRUE
        NA.out <- bv[sv][1]
        if(all(bv[sv] %in% NA.out)){
          result$NA.out = NA.out
        }  # end if all NA values converted tothe same value
      } # end if all problems are related to lost NA
      break # only continue until first probblem tile
    }# end if not equal
  } # end tile loop
  
  if(err == TRUE){
    if(result$problem){
      message  <- paste0("Tif copy (", tif, ") doesn't match soure grid.")
      if(result$NA.only) 
        message  <- paste0(message, " The only differences were in NA values which were converted to ", result$NA.out )
      stop(message)
    }
  }
  return(result)
  
} # end function


  
  
  
