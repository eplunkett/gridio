friskcache <- function(freeup = 0, deletecopies = TRUE, getlock = TRUE, test = FALSE){
  
  # Arguments:
  #    freeup - space to free (mb)
  #    deletecopies - TRUE if ‘copying’ grids should be deleted whether or not 
  #                   they’re stale
  #                   AND unindexed grids are deleted (copying grids show up as unindexed until
  #                     copying completes)
  #    getlock - if TRUE, gets a lock on cacheinfo at start and release it 
  #              when done
  #    test - passed to check grid. If TRUE the grid will be tested by calling 
  #           griddescribe and readblock.  This would make frisking super slow
  
  
  
  #   FRISKCACHE writes a line to x:\anthill\friskcache.log (or whatever the 
  #     anthill path is) whenever run.  The log consists of the following fields:
  #   
  #   date      - date and time
  #   computer  - computer name (short name)
  #   duration  - run duration (hh:mm:ss)
  #   result    - summary of result, either 'ok' or 'n errors', where n is the sum
  #               of missing, expired, copying, and unindexed.
  #   missing   - number of missing/corrupt grids cleared
  #   expired   - number of expired grids deleted
  #   copying   - number of hung copy attempts cleared
  #   unindexed - number of unindexed grids deleted
  #   purged    - number of grids deleted to make room
  #   GBcleaned - amount of space (GB) freed up while cleaning expired grids, 
  #               stale copies, and unindexed grids from the cache.  Calculated  
  #               by taking difference of free disk space after and before.
  #   GBfreed   - amount of space (GB) freed up from 'freeup' argument.  Sum of 
  #               sizes of grids from indices.
  
  
  
  stopifnot(deletecopies %in% c(TRUE, FALSE))
  if(.status$verbosity > 0) cat("friskcache called\n")
  
  ci <- getcacheconfig()
  if(!ci$on){
    if(.status$verbosity > 0) 
      cat("Caching isn't on. Nothing to frisk.\n")
    return(invisible(NULL)) 
  }
  
  
  mb.per.gb <- .status$mb.per.gb # base 10 
  
  start.time <- Sys.time()
  start.free <- fileinfo(paste0(ci$cachepath, "/index/cacheinfo.txt"))$free  # MB
  sname <- anthill::short.name()
  
  if(freeup == 0){
    anthill:::log.message(message = paste0("friskcache (R) is cleaning up grid cache on ", sname))
  } else {
    anthill:::log.message(message = paste0("friskcache (R) is freeing up space in grid cache on ", sname))
  }
  
  # Create empty log details (used for logging details of changes made to index during frisking)  
  log.details <- data.frame(cachepath = character(0), sourcepath = character(0), message = character(0))
  
  log.info <- list(date = format(Sys.time(), format = anthill:::tm.time.format()),
                   computer = sname,
                   system = "R",
                   duration = NA_character_,
                   result = NA_character_,
                   missing = 0, 
                   expired = 0, 
                   copying = 0, 
                 
                   unindexed = 0, 
                   purged = 0, 
                   GBcleaned = 0,
                   GBfreed = 0
  )
  
  msg <- paste(sname, "-R", sep="")
  
  
  
  if(getlock){
    main.lock.path <- paste0(anthill::short.name(), " cache") 
    #  start.cache.timer()
    anthill::get.lock(main.lock.path, message=msg, slash=FALSE)
    exit.fun <- function(){
      anthill::return.lock(main.lock.path, slash = FALSE)
    }
    on.exit( exit.fun() )
  } else {
    exit.fun <- function() return()
  }
  
  # Recheck to make sure that someone else hasn't already freed up space
  if(freeup != 0){
    a <- readLines(paste0(ci$cachepath, "/index/cacheinfo.txt") )
    cachesize <- as.numeric(a[[2]])  # cache size in MB
    free <- fileinfo(paste0(ci$cachepath, "/index/cacheinfo.txt"))$free
    available <- min(free - ci$minfree*mb.per.gb , ci$maxsize* mb.per.gb - cachesize)
    if(available > 0.5 * freeup){ # If someone else freed up space while we were waiting (and even if others wrote some grids afterwards)
      anthill:::log.message(message = paste0("friskcache (R) unnecessary on ", sname, ". Another thread probably just did it."))      
      log.info$result <- "Unnecessary frisk"
      log.info$duration <- anthill::formatelapsedtime(seconds = as.numeric(difftime(Sys.time(),start.time, units = "sec")))
      log.file <-   paste0(anthill:::.settings$path, "/friskcache.log")
      append <- file.exists(log.file)
      anthill::tm.write.table(log.info, file = log.file, append = append, col.names = !append)
      return(TRUE)  # TRUE indicates success.
    }
  }
  
  # Intitial scan 
  # index by index look for stuff in the index that doesn't exist on disk
  indicies <- vector(mode = "list", length = ci$nbins)
  for(i in 1:ci$nbins){
    if(.status$verbosity > 0 && (i %% 5) == 0 ) 
      cat("Scanning index", i, "of", ci$nbins, "\n")
    lock <- paste0(sname, " ", getcachepath(i))
    anthill::get.lock(path = lock, slash = FALSE, message = msg)
    on.exit({
      anthill::return.lock(path = lock, slash = FALSE, msg = msg)
      exit.fun()
    })
    
    index <- getcachedir(i)
    
    if(nrow(index) == 0){
      anthill::return.lock(path = lock, slash = FALSE, message = msg)
      on.exit(exit.fun())
      indicies[[i]] <- index
      next
    }
    keep <- rep(TRUE, nrow(index))
    
    for(j in 1:nrow(index)){
      delete <- FALSE
      if(index$status[j] == "expired"){
        log.info$expired <- log.info$expired + 1
        keep[j] <- FALSE
        delete <- TRUE
        if(.status$verbosity > 1) 
          cat("deleting expired grid:", index$cache[j] , "\n")
        if(ci$logdetails){
          a <- data.frame(cachepath = index$cache[j],  sourcepath = index$source[j], message =  "expired")
          log.details <- rbind(log.details, a)
        }
      } else if(!is.na(index$cache[j]) && index$cache[j] == index$source[j]){ # old cache in place grid. Don't ever want to delete source!
        keep[j] <- FALSE
        delete <- FALSE
        
      } else if(index$status[j] == "copying"){
        stale <- difftime(Sys.time(), index$cachedate[j], units = "days") > 1 
        if(deletecopies || stale ){
          keep[j] <- FALSE
          delete <- TRUE
          log.info$copying <- log.info$copying + 1
          if(.status$verbosity > 1) 
            cat("deleting stale copying grid:", index$cache[j] , "\n")
          if(ci$logdetails){
            a <- data.frame(cachepath = index$cache[j],  sourcepath = index$source[j], message =  "copying")
            log.details <- rbind(log.details, a)
          }
        }
      }else if(checkraster(x = index$cache[j], test = test)$problem){
        keep[j] <- FALSE
        delete <- TRUE 
        log.info$missing <- log.info$missing + 1
        if(.status$verbosity > 1) 
          cat("deleting problem grid:", index$cache[j] , "\n")
        if(ci$logdetails){
          a <- data.frame(cachepath = index$cache[j],  sourcepath = index$source[j], message =  "missing")
          log.details <- rbind(log.details, a)
        }
      }
      if(delete){
        target <- gsub(paste0("^(", ci$cachepath, "/[[:digit:]]*)/.*$"), "\\1", index$cache[j])  
        unlink(x = target, recursive = TRUE)
      }
    }  # end loop throw rows in this index file
    
    if(!all(keep)){ # if we are dropping any rows
      index <- index[keep, , drop = FALSE]
      putcachedir(index, grid = i)
    }
    indicies[[i]] <- index
    anthill::return.lock(path = lock, slash = FALSE, message = msg) 
    on.exit(exit.fun())
  } # end loop through index files
  index <- do.call(rbind, args = indicies) # assemble complete index
  
  
  # Delete directories in cache that aren't in the index
  if(deletecopies){
    l <- list.files(ci$cachepath, pattern = "^[[:digit:]]*$")  
    index$id <- gsub(paste0(ci$cachepath, "/([[:digit:]]*)/.*$"),"\\1", index$cache, ignore.case = TRUE)
    extra <- setdiff(l, index$id)
    log.info$unindexed <- log.info$unindexed + length(extra)
    
    if(length(extra) > 0){ 
      if(.status$verbosity > 0)
        cat("deleting ", length(extra), "directories from gridcache that are not in index.\n")
      unlink(x = paste0(ci$cachepath, "/", extra), recursive = TRUE)
      
      if(ci$logdetails){
        a <- data.frame(cachepath =  paste0(ci$cachepath, "/", extra), sourcepath = "", message = "unindexed" )
        log.details <- rbind(log.details, a)
      }
    }
  }
  
  # Assess how much space we have
  cachesize <- sum(index$size, na.rm = TRUE)
  free <- fileinfo(paste0(ci$cachepath, "/index/cacheinfo.txt"))$free
  available <- min(free - ci$minfree * .status$mb.per.gb , ci$maxsize* .status$mb.per.gb - cachesize)  # unused capacity  
  
  log.info$GBcleaned <- max(0, round((free - start.free) / .status$mb.per.gb , 3))
  
  
  # If we need to free up more space
  if(freeup > 0){
    if(.status$verbosity > 3) cat("Freeup =", freeup, "starting to purge\n")
    b <- .pick.losers(index, freeup)
    success <- b$success
    if(success){
      sv <- index$source %in% b$losers
      cut <- index[sv, , drop = FALSE]  # grids to cut 
      log.info$purged <- nrow(cut)
      log.info$GBfreed <- round( sum(cut$size, na.rm = T) / .status$mb.per.gb, 3)
      if(.status$verbosity > 1) 
        cat("deleting", nrow(cut), " grids from index to make space for new grids\n")
      if(ci$logdetails){
        a <- data.frame(cachepath =  cut$cache, sourcepath =cut$source, message = "purged" )
        log.details <- rbind(log.details, a)
      }
      index <- index[!sv, ]  # removed them from index
      cachesize <- sum(index$size, na.rm = TRUE)
      cut$hash <- getcachepath(grid = cut$source, justhash = TRUE)
      for(hash in unique(cut$hash)){
        # delete stale grids from disk and index file here.
        targets <- cut$cache[cut$hash == hash]  #  cached grids to be removed
        index.lock.path <- paste0(anthill::short.name(), " ", getcachepath(grid = hash))
        # start.cache.timer()  # We've decided for now not to add frisk time to cachewait
        anthill::get.lock(index.lock.path, message=msg, slash=FALSE)
        on.exit({ # in case there are errors
          anthill::return.lock(index.lock.path, slash=FALSE)
          exit.fun()
        })
        d <- getcachedir(grid = hash)
        d <- d[!d$cache %in% targets, , drop = FALSE]  # drop deleted rows from index
        putcachedir(d, grid = hash, unlock = FALSE)
        anthill::return.lock(index.lock.path, slash=FALSE)
        on.exit(exit.fun())
        target.dirs <- gsub(paste0("^(", ci$cachepath, "/[[:digit:]]*)/.*$"), "\\1", targets)    
        unlink(x = target.dirs, recursive = TRUE)          
      }
    } else {
      if(.status$verbosity > 3) cat("Unable to freeup space\n")
    }
  } else {  
    success <- TRUE  # if freeup == 0 success is always true
  }  
  
  
  # Update cache size on disk
  a <- readLines(paste0(ci$cachepath, "/index/cacheinfo.txt") )
  last.id <- as.numeric(a[[1]])
  writeLines(as.character(c(last.id, cachesize)), con = paste0(ci$cachepath, "/index/cacheinfo.txt") )
  
  
  log.info$duration <- anthill::formatelapsedtime(seconds = as.numeric(difftime(Sys.time(),start.time, units = "sec")))
  
  n.errors <- with(log.info, sum(missing, expired, copying, unindexed, na.rm = TRUE))
  log.info$result <- ifelse(n.errors <= 0, "ok", paste(n.errors, "errors"))
  
  
  log.file <-   paste0(anthill:::.settings$path, "/friskcache.log")
  append <- file.exists(log.file)
  anthill::tm.write.table(log.info, file = log.file, append = append, col.names = !append)
  
  
  anthill:::log.message(message = paste0("friskcache (R) is done on ", sname))
  
  if(ci$logdetails){
    if(nrow(log.details) == 0)
      log.details <- rbind(log.details, data.frame(cachepath = "", sourcepath = "", message = "nochange"))
    final.details <- data.frame(
      date = format(Sys.time(), format = anthill:::tm.time.format()),
      computer = sname,
      system = "R", 
      cachepath = log.details$cachepath, 
      sourcepath = log.details$sourcepath, 
      message = log.details$message)
    
    log.file <-   paste0(anthill:::.settings$path, "/friskcachedetails.log")
    append <- file.exists(log.file)
    anthill::tm.write.table(final.details, file = log.file, append = append, col.names = !append)
    
  }
  
  
  
  # Note main lock returned by on.exit (if need be)
  
  return(success)
} # end friskcache function