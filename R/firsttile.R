firsttile <-
function( ){
# Ethan Plunektt Jan 2011
# This function sets the tile indices to the first tile
  .status$tilescheme$i <- 1
  .status$tilescheme$j <- 1
  .status$tilescheme$tileno <- 1
  if(!is.na(.status$subtilescheme$nc)) stinit() # initialize this tile's sub-tile info
}

