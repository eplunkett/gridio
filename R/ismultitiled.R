
ismultitiled <- function(x){
  # Returns TRUE if x is a multitiled grid FALSE otherwise (including for geoTIFF files)
  if(istif(x)) 
    return(FALSE)
  l <- list.files(x, pattern = "w[[:digit:]]+x\\.adf$")
  if(length(l) > 1) return(TRUE)
  if(length(l) <= 1 ) return(FALSE)  
  
}
