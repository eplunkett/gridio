if(FALSE){
  dem <- c("x:/gis/dem")
  demsmooth <- "x:/gis/demsmooth"
  outgrid <- "x:/gis/diff"
  
  library(gridio2)
  gridinit()
  setwindow(dem)
  tileinit(100, 0)
  
  library(anthill); config()
  
  if(FALSE){
    sd <- 120
    kernelsmooth(dem, makegaussiankernel,  outgrid=demsmooth,  sd=sd, max.r=sd*3, kernel.dim=13,  overwrite=TRUE)
  }
  
  
  FUN <- function(a, b, ...) a - b
  grids <- c(dem, demsmooth)
  vars <- c("a", "b")
  na.value = -9999
  as.integer = FALSE
  cacheok = TRUE
  as.mosaic=FALSE
  tileinit(1000, 0)
  tilenumber=1
  
  
  
  FUN <- function(a) round(a)
  grids <- dem
  vars <- "a"
  na.value = -9999
  as.integer = TRUE
  cacheok = TRUE
  as.mosaic=FALSE
  tileinit(1000, 0)
  tilenumber=1
  outgrid <- "X:/gis/demround"
  tilefun(FUN, grids, vars, outgrid, tilenumber=1)
  
  
  f1 <- function(a, b, ...) a - b 
  f2 <- function(a, b, c, ...) a * b * c 
  FUN <- list(f1, f2)
  outgrid <- c("x:/gis/diff", "x:/gis/product")
  
  tilefun(FUN, grids, vars, outgrid, tilenumber=1, extra.args=list(c=1) )
  
  gridfun(FUN, grids, vars, outgrid, extra.args=list(c=1), overwrite=TRUE)
  
  res <- readgrid(outgrid)
  plot(res)
  
  
}
  
  

tilefun <- function(FUN, grids, vars, outgrid, tilenumber, na.rm=TRUE, na.value = -9999, as.integer=FALSE, 
                    cacheok=TRUE, as.mosaic, extra.args){
  # Arguments
  #	FUN - a fitted model on which the predict function can be called
  # 	grids - a vector of gridpaths 
  #	vars - a vector of variable names one for each grid in grids
  #	gridout - a path to an output grid
  #	tilenumber - the tilenumber to process
  #  na.rm - if TRUE all cells that have NA in any of the input grids won't be passed to fit (and will receive NA) otherwise whatever data is available in the grid for those cells will be passeed to fit.
  #  list - If predictions are desired from more than one fit object than they can be passed as a list via this argument
  #		In which case outgrid should be a vector of output grids one for each fit in list.
  
  # na.value - this value is used to transmit NA to the underlying C code. Set it to a value that doesn't occur in your dataset (default is -9999)
  # as.integer - Indicates whether each new grid will be floating point or integer.  Can be a vector of the same length as list
  #	... - other arguments to be passed to the predict when called on fit
  
  if(!getwindow()$set) stop("gridinit must be called prior to this function.")
  if(is.na(tilescheme()$totaltiles)) stop("tileinit must be called prior to this function")
  if(missing(as.mosaic)) as.mosaic <- .status$as.mosaic
 
  
  ng <- length(grids) 
  if(length(vars) != ng ) stop("grids and vars should be of the same length.")
  
  settile(tilenumber)
  td <- tiledetails()
  
  d <- matrix(NA, ncol=length(grids), nrow=(td$nrow*td$ncol)) # make empty matrix to store the data
  d <- as.data.frame(d)
  
  # Make FUN into a list if it isn't one
  if(inherits(FUN, "function")){
    FUN <- list(FUN)
  } 
  
  if(length(FUN)!= length(outgrid)) stop("There must be the one and only one outgrid for each element in FUN.")
  
  
  if(length(as.integer)==1) as.integer <- rep(as.integer, length(FUN))
  
  if(length(as.integer)!= length(FUN))
    stop("If as.integer is specified it must either be a single value to be used for all functions or a vector of values of the same length as FUN")
  
  # assemble matrix of prediction data from grids
  for(i in 1:ng){
    cat("Reading tile ", tilenumber, " from grid \"", grids[i], "\"\n", sep="")
    g <- readtile(grids[i], na.value=na.value, cacheok=cacheok)
    if(na.rm && all(is.na(g$m))){
      cat("First grid is all NA in this tile.  Moving on.")
      return() 
    }
    d[,i] <- as.vector(g$m)
    colnames(d)[i] <- vars[i]
  }
  cat("Done reading tiles\n")
  
  # Setup selection vector that indaicates which rows to run on when na.rm is TRUE
  ncells <- nrow(d)
  if(na.rm){
    sv <- apply(d, 1, function(x) !any(is.na(x)))  # selection vector to select non-na-containing rows
    if(sum(sv) == 0){
      cat("All NA. Moving on.")
      return() # nothing to do
    }
    d <- d[sv, , drop=FALSE]
  }

  # Convert d to list
  d <- as.list(d)
  
  # Construct argument list
  if(!missing(extra.args)){
    stopifnot(is.list(extra.args))
    d <- c(d, extra.args)
  }
  
  
  # loop through FUNCTIONS 
  for(i in 1:length(FUN)){
    f <- FUN[[i]]
    cat("Applying function", i, "\n")
    
    
    if(na.rm){
      pred <- vector(mode="numeric", length=ncells) # preallocate prediction vector
      pred[]<- NA
      pred[sv] <- do.call(what="f", args=d)
    } else {
      pred <-do.call(what="f", args=d)
    }
    cat("Writing results to \"", outgrid[i],"\"\n", sep="" )
    g$m <- matrix(pred, nrow=g$nrow, ncol=g$ncol)
    writetile(g, outgrid[i], na.value=na.value, as.integer=as.integer[i], as.mosaic=as.mosaic)
  }
  
}


