
# Example usage
if(FALSE){  # Skip example usage when sourcing the file (sourcing just defines the function)
 
  ### Setup a temporary directory with a sample grid in it 
  # Get path to example dem grid (included with package)
  library(gridio2)
  library(anthill)
  config()
  
  datapath <- system.file("exampledata", package="gridio2")  # grid path to example grid
  # datapath <- "X:/Users/ethan/gridio/inst/exampledata/" # for development

  datapath <- shortPathName(datapath)
  datapath <- paste(datapath, "/.", sep="")
  
  # Make a temporary directory to write new grids to
  # Must be on drive associated with a gridserver 
  dir <- "X:/temp/pgridpredicttest/"
  dir.create(dir)
  
  # Copy example data into temporary directory
  file.copy(datapath, dir, recursive=TRUE)
  # system(paste("open", dir)) # if you want to look at contents of temporary directory
  ###  Done setting up directory
  
 
  
  # Set paths
  n <- c("dem", "gradient", "obs")
  paths <- as.list(paste0(dir, "\\", n, ".tif"))
  names(paths) <- n
  

  cleanup() # disconnect from any prior gridservers 
  gridinit() # initialize local gridserver with base path in temporary dir 
  setwindow(paths$dem) # setwindow to dem file
  
  ### Make up some sample data
  # based on a north south gradient and some noise
  g <- readgrid(paths$dem)
  gradient <- seq(20, 100, length.out=g$nrow)
  g$m[,] <- gradient 
  g$m <- g$m + rnorm(g$nrow*g$ncol)
  writegrid(g, paths$gradient)
  elev <- readgrid(paths$dem)
  grad <- g
  d <- data.frame(elev=as.numeric(elev$m), grad=as.numeric(grad$m)) 
  d$y <- d$elev + d$grad + rnorm(nrow(d), 0, 10)
  obs <- g
  obs$m <- matrix(d$y, nrow=obs$nrow, ncol=obs$ncol)
  plot(obs)
  writegrid(obs, paths$obs)
  
  # Make fit object
  f <- lm(y~elev+grad, data=d)
  dir <- gsub("\\\\", "/", dir)
  
  #### The real example starts here
    
  
  tilepredict.args <- list(fit=f, grids= c(paths$dem, paths$gradient) , 
                           vars=c("elev", "grad"),outgrid=paste(dir, "/pred",sep=""))
  
  pgridpredict(tilepredict.args=tilepredict.args, gridserver="EthanXP", 
               port=3333, path=path, name="pgridpredicttest", 
               refgrid=tilepredict.args$grids[1], tilesize=100, maxthreads=3, 
               host = short.name() )  # Do not use 100 as your tilesize!
  
}





pgridpredict <- function(tilepredict.args, # named list of arguments to tilepredict (tilenumber can be ommitted)
                         tilesize,   # the tilesize to use 
                         tiles,           # An integer vector indicating a subset of tiles to process (optional)
                         refgrid,         # depricated
                         gridserver,      # depricated
                         port,            # depricated
                         name,            # the project name (NOT optional)
                         owner,           # the project owner. Optional if set.owner() has been called
                         refmosaic,       # depricated
                         ...              # other arguments to launch.project such as:
                                          #   priority, and maxthreads
){
  # pgridpredict  
  # Ethan Plunkett July 30, 2013
  
  # This is a parallel version of grid predict.  It uses anthill to launch a project on the cluster
  #  with one task with subtasks for each tile in landscape.
  # It's recommended that maxthreads be set low initialy and then increased only until gridwait
  #  begins to increase. 
  
  # Simplified and modernized in Dec. 2022
  #   Now uses refgridinit() on first grid to connect to gridservers
  #   Saves and restores initial connections
  #   Saves local connections and gridinfo and restores them to anthill threads
  #   Drops all arguments about grids, gridservers, etc.
  #   Requires that gridservers.txt be within anthill directory.
  # 
  
  
  # Throw warning on deprecated arguments
  passed <- names(as.list(match.call())[-1])
  deprecated_args <-  c("refgrid", "gridserver", "port", "refmosaic") 
  if(any(deprecated_args %in% passed)){
    warning("Deprecated arguments used: ", 
            paste(intersect(deprecated_args, passed), collapse = ", "),
            "( As of Dec 2022)." )
  }
  
  # Validate input on tiles and tilesize
  if(!missing(tiles) && missing(tilesize))
    stop("Tilesize must be specified if using the tiles argument.")
  if(missing(tilesize)) 
    tilesize <- 2000
  if(!anthillrunning())
    stop("Anthill must be running for this function to work.")
  
  con <- saveconnections()
  on.exit(restoreconnections(con))

  grids <- tilepredict.args$grids
  refgridinit(grids[1])
  
  # Generate or check the tile list
  tileinit(tilesize, buffer=0)
  if(missing(tiles)){
    tiles <- 1:ntiles()
  } else {
    stopifnot(all(tiles %in% 1:ntiles()))
  }
  
  # Check for coinciding grids and add griddescriptions to cache
  w <- getwindow()
  for( i in seq_along(grids)){
    stopifnot( coincide( griddescribe( grids[i] ), w ) )
  }
  gi <- savegridinfo()
  
  # Make function to do the actual predictions
  predtile <- function(tile){
    library(gridio2)
    restoreconnections(con)
    restoregridinfo(gi)
    tileinit(tilesize, buffer=0)
    tilepredict.args$tilenumber <- tile
    do.call(tilepredict, args=tilepredict.args)
  }
  
  anthill::simple.launch(call = "predtile()",
                         name=name, 
                         subtask = anthill::make.range(values = tiles), 
                         subtaskarg = "tile",
                         owner=owner, 
                         objects = c("tilepredict.args", 
                                      "tilesize", 
                                      "predtile", 
                                      "con",
                                      "gi"), 
                          ...)
  
  
}
