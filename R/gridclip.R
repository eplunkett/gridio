gridclip <- function(x,y){
  #------------------------------------------------------------------------------------------------#
  # Ethan Plunkett    Sept 2013
  # Function to clip the data in x to where it overlaps the extent of y. 
  #  x must be a grid) y can be a grid or any other list containing the grid alignment info.
  # 
  #  Notes:
  #    Each cell's position relative to the projection is maintained.  
  #    y must have the same cell snapping as x.  
  #    If they don't overlap at all it will return a gridlike object with NA in each component
  #    
  #  Help should refrence matchextent and viceversa
  #------------------------------------------------------------------------------------------------#
  checkcellalignment(x, y)
  
  # Note these are cell centers
  x.xrange <- c2x(c(1, x$ncol), x)
  y.xrange <- c2x(c(1, y$ncol), y)
  x.yrange <- r2y(c(x$nrow, 1), x)
  y.yrange <- r2y(c(y$nrow, 1), y)
  
  # If they don't overlap return an empty grid
  overlap <- function(a, b) ( max(a[1],b[1]) <= min(a[2], b[2])) 
  if(!overlap(x.xrange, y.xrange) || !overlap(x.yrange, y.yrange)){ 
    res <- list(m=NA, nrow=NA, ncol=NA, xll=NA, yll=NA, cellsize=NA)
    class(res) <- c("grid", class(res))
    return(res)
  } 
    
  # calculate the range of the overlap
  xrange <- c(max(x.xrange[1], y.xrange[1]), min(x.xrange[2], y.xrange[2]))
  yrange <- c(max(x.yrange[1], y.yrange[1]), min(x.yrange[2], y.yrange[2]))
  
  # Make a new empty grid covering the overlap
  nrow <- round( (yrange[2] - yrange[1])/x$cellsize + 1 )
  ncol <- round( (xrange[2] - xrange[1])/x$cellsize + 1 )
  yll <- yrange[1] - x$cellsize/2
  xll <- xrange[1] - x$cellsize/2
  
  new <- list(m=matrix(NA, nrow, ncol), nrow=nrow, ncol=ncol, xll=xll, yll=yll, cellsize=x$cellsize)
  class(new) <- c("grid", class(new))
  
  
  #  Copy the values over where they overlap
  new$m[,] <- 
    x$m[y2r(yrange[2], x):y2r(yrange[1], x),x2c(xrange[1], x):x2c(xrange[2], x)]
  
  return(new)
}
