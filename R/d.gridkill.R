.gridkill <-
function(x){
  # gridkill    Ethan Plunkett   Jan 2011
  # Function to delete grids
  # Arguments:
  #x : a single path or a vector of paths indicating grids to delete
  #  It is important to use this function rather because deleting grid files with system tools
  #   will result in a malformed info directory potentially currupting other grids
  #   in the same directory.
  for (i in x) {
    done <- FALSE
    .status$gsdowntime <- 0  # The amount of time the gridserver has been down
    
    
    if(.status$echoccalls){
      cat(".C(\"R_gridkill\",\n\t", 
          "integer(1),", "\n\t",
          "as.character(", shQuote(i), "),", "\n\t",
          "PACKAGE= ", shQuote(.status$dllname), ")", 
          "# from .gridkill()\n", sep = "")
    }
    
    while (!done) {
      st <- system.time({
        r <- .C("R_gridkill",
                integer(1),
                as.character(i),
                PACKAGE = .status$dllname)
      })
      .status$gridwait <- .status$gridwait + as.numeric(st[3])
      done <- .donetrying(code=r[[1]], st, reset=FALSE)
    }

    if (r[[1]]== -10007)
      warning("Grid \"", i, "\"  wasn't found.")
    else
      parseerror(r[[1]], "gridkill")
    }
  invisible()
}

