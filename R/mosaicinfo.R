mosaicinfo <- function(path, lock=anthillrunning(), cache = TRUE){
  # Note path is the path to a mosaic grid (not to the description file within it)
  # As of 5/17/2022 I'm supporting limited relative paths to the source raster.  
  # "".\" now indicates the mosaic and source share a directory.  S
  # mosaicinfo expands the ".\" into the full path so functions that
  # call mosaicinfo are unaffected by the change.

  
  path <- gsub("\\\\", "/", path)  
  path <- gsub("/*$", "", path)
  
  if(cache){
    # If caching is on and the path is in the cache return it
    
    if(path %in% ls(.status$gridinfo)  && !is.null(.status$gridinfo[[path]]$mosaicinfo))
      return(.status$gridinfo[[path]]$mosaicinfo)
    
  }
  
  dir.path <- path
  path <- paste(path, "/mosaicdescrip.txt", sep="")
  
  # Read data generating a list of variables and values based on lines with =
  if(lock){
    anthill::get.lock(dir.path)
    on.exit(anthill::return.lock(dir.path))
  }
 
  count <- 1
  while(count <= 3){
    a <- tryCatch(  md <- readLines(con=path), error = identity)
    if(inherits(a, "error") || length(md) <= 1){
      if(gridioverbosity() > 0)
        cat("Failed to read ", path, "on attempt", count, "\n")
      Sys.sleep(0.5 * count)
      
      count <- count + 1
      
      next
    }
    break
  }
  if(inherits(a, "error"))  
    stop("Could not read mosaicdescrip.txt for mosaic : ", dir.path)
  
  if(lock){
    anthill::return.lock(dir.path)
    on.exit()
  }
  
  md <- gsub("[;].*$", "", md) # drop ; and rest of line.
  md <- grep("=", md, value=TRUE)
  md <- strsplit(md, "=")
  md <- md[sapply(md, function(x) length(x) == 2)]  # drop rows that don't have two items (would be caused by multiple "=" signs.)
  
  vars <- sapply(md, function(x) x[1])
  vars <- gsub("^[[:space:]]*|[[:space:]]*$", "", vars)
  
  values <- sapply(md, function(x) x[2])    
  values <- gsub("\\\\", "/", values)  # replace "/" with "\"    
  values <- gsub("^[[:space:]]*|[[:space:]]*$", "", values)    
  tvi <- grep( "[^.[:digit:]]", values) # text value index     
  result <- list()
  
  for(i in 1:length(vars)){
    val <- ifelse(i %in% tvi, gsub("^['\"]|['\"]$", "", values[i]), as.numeric(values[i]))
    result[[i]] <- val 
    names(result)[i] <- vars[i]
  }

  # check to make sure all the expected variables are there
  expected.vars <-  c("name", "type", "timestamp", "source", "sourcetimestamp", "extent", 
                      "rowsize", "colsize", "rows", "cols", "panemap")
  if(!all(expected.vars %in% names(result) ))
    stop("Missing variable(s):", paste(setdiff(expected.vars, names(result)), collapse=", "))

  # If compression is missing add it to results with default of "no"
  if(! "compression" %in% names(result))
    result$compression <- "no"
  expected.vars <- c(expected.vars, "compression")
    
  # Cleanup extent
  extent <- eval(parse(text=paste("c(", result$extent, ")")))
  extent <- as.list(extent)
  names(extent) <- c("ncol", "nrow", "xll", "yll", "cellsize", "nodata", "type")
  extent <- extent[!names(extent) %in% "nodata"]
  
  if(!extent$type %in% 1:2) stop("Unknown type")
  extent$type <- ifelse(extent$type==1, "integer", "real")
  result$extent <- extent
  
  # convert panemap to a logical vector
  result$panemap <- as.logical(as.numeric(strsplit(result$panemap, "")[[1]]))
  
  # Make source path "/"
  result$source <- gsub("\\\\", "/", result$source)
  # Substantiate limited relative path (only works if in same directory as mosaic)
  if(grepl("^./", result$source)){
    result$source <- gsub("^./", "", result$source)
    result$source <- paste0(dirname(dir.path), "/", result$source)  # dir.path is the mosaic directory path so need parent dir
  }
  
  # Drop unexpected variables 
  result <- result[expected.vars]
  return(result)
}
