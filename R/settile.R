settile <-
function(tile, ts){
# Ethan Plunektt Jan 2011
# This function sets the tile indices (i, j, and tileno in tilescheme) to the proper values for the tile number
# it invisibly returns TRUE as long as the specified tile exists.  If the specified tile doesn't exist it 
# it invisibly returns FALSE and set's tileno, i, and j to zero (in .status$tilescheme)
#  Note - i refers to the tile row number and j the tile column number
#   Nov. 8, 2021  Optional second argument ts is an alternative means of specificing the tiles scheme to act on.
#     if not used behavior is unchanged. However, if ts is supplied it is modiefied and returned and 
  #   the gloval tiles scheme is unchanged.
  passed.ts <- TRUE    # 
  if(missing(ts)){
    passed.ts <- FALSE
    ts <- tilescheme()
  }
  
  if(is.na(ts$totaltiles))
    stop("gridinit, setwindow, and tileinit or subtileinit must all be called prior to this function.")  
  
  if(length(tile) != 1) stop("Tile must be a singe value.")
  
  if(tile > ts$totaltiles){ # if indexing the tile has moved us outside the window
    ts$i <- 0# then set the indices to zero
    ts$j <- 0
    ts$tileno <- 0
    if(passed.ts) stop("i or j is out of range")
    return(invisible(FALSE))
  }
  ts$j <- tile %% ts$cols
 	if(ts$j == 0) ts$j <- ts$cols
  ts$i <- ((tile-1) %/% ts$cols) + 1
  ts$tileno <- tile
 
  if(passed.ts){
    return(ts)
  }  else  {
    .status$tilescheme  <- ts
    if(!is.na(.status$subtilescheme$nc))
      stinit()
  }    
  return(invisible(TRUE))
  
}

