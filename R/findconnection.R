if(FALSE){
  cleanup()
  gridinit()
  setwindow(list(xll=0, yll=0, nrow=12, ncol=15, cellsize=10))
  gridinit(add=TRUE)
  setwindow(list(xll=0, yll=0, nrow=6, ncol=8, cellsize=20))
  gridinit(add=TRUE)
  x <- list(xll=0, yll=0, nrow=12, ncol=15, cellsize=10)
  findconnection(x)
  
  w <- list(xll=0, yll=0, nrow=12, ncol=15, cellsize=10)
  cleanup()
  gridinit()
  setwindow(w)
  gridinit(drive="x:/", add=TRUE)
  setwindow(w)
  findconnection(w)
  findconnection(w, path="X:/junk/stuff/agrid")
  findconnection(w, path="c:/junk/stuff/agrid")
  gridinit(drive="C:/", add=TRUE)
  setwindow(w)
  findconnection(w, path="c:/junk/stuff/agrid")
  
}

findconnection <- function(x, path="", pane){
  
  # x is a list of xll, yll, nrow, ncol, cellsize which is compared to those in the connection list
  # pane is a pane id if specified it overides x.
  
  # path is the path of the grid to be read (optional)
  #  if more than one connection matches the grid extent then if path is specified than the 
  # connection with a matching drive is chosen. If the path or the connecions's drives are unspecified
  # then the first matching connection will be returned.
  
  # Preallocate vectors for keeping track of which connections match the arguments
  drivematches <-  extentmatches <- rep(FALSE, nrow(.status$connections))

  # Identify matching extents
  if(missing(pane)){
  for(i in 1:length(extentmatches))
    if(coincide(as.list(.status$connections[i, ]), x)) extentmatches[i] <- TRUE
  } else {
    extentmatches <- .status$connections$pane %in% pane
  }
  # if there are one or no matching extents or drive is undefinied we don't need to worry 
  # about the drive and we'll just return the first matching extent or NA for no matches.
  if(sum(extentmatches) ==0) return(NA)
  if(sum(extentmatches) == 1 | path=="" | is.na(path)) return(which(extentmatches)[1])
  
  # More than one extent matches. We'll use drive to find the best match if we can.    
  path <- tolower(path)
  path <- gsub("\\\\", "/", path)
  
  # Determine whether each drive matches
  for(i in 1:length(drivematches)){
    d <- as.character(.status$connections$drive[i])
    if(is.na(d) || d == ""){
      drivematches[i] <- NA 
    }else {
      p <- substr(path, 1, nchar(d))
      drivematches[i] <- p==d 
    }
  }
  
  a <- drivematches & extentmatches
  
  if( sum(!is.na(a) & a) > 0) return(which(a)[1])  # one or more matches for both extent and drive return the first
  
  a[is.na(a)] <- TRUE
  if(sum(a) > 0) return(which(a)[1])  # if no complete matches accept matches where the drive is undefined
    
  return(which(extentmatches)[1])  # fall back position if none of the drive matching worked out just take the first extent that matches.
}
