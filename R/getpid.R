getpid <- function(){
  # getpid      Ethan Plunkett
  #  Return the process ID associated with the R session it is called from.
  # This is used by anthill but since it relies on edi's DLL I put it here.
  # Anthill uses a less
  # efficient cumbersome process to get the PID if gridio2 isn't loaded.
  r <- .C("R_getpid", integer(1), PACKAGE = .status$capslib)
  return(r[[1]])

}
