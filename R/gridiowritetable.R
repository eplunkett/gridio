gridiowritetable <- function(x, file,  row.names=FALSE, sep="\t", quote=FALSE, na="", ...){
  # write.table wrapper for gridio2.  
  # It sets a bunch of the options for the standard write.table command and it operates in a loop
  # catching errors and retrying up to 5 times before reporting an error.
  
  
  i <- 0
  while(TRUE){    
    i <- i +1
    a <- tryCatch(
      write.table(x=x, file=file, row.names=row.names, sep=sep, quote=quote, na=na, ...),
      error=function(e) e) # end tryCatch
    if(!inherits(a, "error")) return(a)
    cat("Write table collision on try ", i, " with file ", file, ": ", a$msg, "\n", sep="")
    Sys.sleep(.2)
  }
  print(paste("Write table failed", i, "times. On file", file))
  stop(a$message)
  
}
