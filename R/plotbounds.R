if(FALSE){
  
  x <- list(xll=0, yll=10, nrow=200, ncol=300, cellsize=10)
  gridinit()  
  setwindow(x)
  plotextent()
  tileinit(80, 0)
  plotextent()

  settile(8)
  plotbounds(tiledetails(), border="blue")
  
  subtileinit(80, buffer=20, 200)
  plotextent()
  plotextent(x)
  
}

plotbounds <- function(x, border="red", lwd=2, ...){
  b <- getbounds(x)
  polygon(x=c(b[1,], b[1, 2:1]), y=rep(b[2, ], each=2), border=border, lwd=lwd, ...)  
}
