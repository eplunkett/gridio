if(FALSE){
  
  source <- "X:/LCC/GIS/Work/Ancillary/Reference/ref_ner_m"
  destination <- "X:/Working/ethan/temp2/converted2_tm"
  
  # Note this mosaic failed to convert  cleanly with gdal_translate alone (usearcpy = FALSE)
  source <- "X:/LCC/GIS/Final/NER/caps_phase5/grids/rawgradient"  
  destination <- "X:/Working/ethan/temp2/rawgradient_tm"
  script.path <-  "X:/Users/Ethan/gridio/inst/python/CopyRaster.py"  # need to pass this b/c not  yet in built package
  
  convertmosaic(source, destination, crs = "EPSG:5070", script.path = script.path)
  
}


convertdslmosaics <- function(source, ...){
  convertmosaics(source = source, crs = "EPSG:5070", ...)
} # end function
 
convertmosaics <- function(source, ...){
  # for converting multiple mosaics at once
  result <- data.frame(source = source, error = FALSE, message = "")
  for(i in seq_along(source)){
    a <- tryCatch(convertmosaic(source = source[i], ...), error = function(e) e )
    if(inherits(a, "error")){
      result$error[i] <- TRUE
      result$message[i] <-  gsub("[\t\n\r]", " ", a$message)
    }
    
  } # end for
  return(result)

} # end function



convertmosaic <- function(source, destination, usearcpy = TRUE, 
                          deletesource = TRUE, verify = TRUE, ...){
  # Function to convert grid based mosaic to tif based mosaic
  # Arguments:
  #   source: path to grid based mosaic
  #   destination: path to tif mosaic
  #   usearcpy : if TRUE use ArcPy to do initial conversion to tifs
  #     in testing gdal_translate didn't handle NA values in  the grids well - for some
  #     grids it converted them to values. If TRUE use gdal_translate only.
  #  ... used to pass arguments on to convertgrid()  potentially:
  #   python.path : path to python (only relevant if usearcpy=TRUE)
  #     if missing use .status$python.path see setpythonpath
  #   script.path : path to template script will be set automatically
  #     included as an argument mostly for debugging.
  #   crs : passed to  gdal_translate with -a_srs to force new projection definition
  #      without reprojecting.  Use to add crs to output mosaic.  Currently only
  #      implemented when usearcpy  = TRUE (use arcpy  uses arcpy + gdal_translate)
  #    verify : only implemented with usearcpy=TRUE if TRUE use gridio and terra
  #     to compare data in source and destination mosaics.

  
  friskmosaics(source, err = TRUE)
 
  if(missing(destination)){
    a  <- gsub("\\\\$|/$$", "", source, ignore.case = TRUE)
    a <- gsub("_m$", "", a, ignore.case = TRUE)
    destination <- paste0(a, "_tm")
    if(.verbosity() > 0)
      cat("Mosaic path not specified using: ", destination, "\n")
  }
  
  sd <- mosaicinfo(source)
  w <- sd$extent
  rowsize <- sd$rowsize
  colsize <- sd$colsize
  panemap <- sd$panemap
 
  destname <- basename(destination)
  dir.create(destination, showWarnings = FALSE)
  
  panes <- which(panemap)
  for(i in seq_along(panes)){
    p <- panes[i]
    if(.verbosity() > 0)  
      cat("Converting pane ", p, " (", i, " of ", length(panes), ")\n", sep ="")
    s <- panepath(source, sd$name, p, length(panemap), tiff = FALSE)
    d <- panepath(destination, destname, p, length(panemap), tiff = TRUE)
    if(usearcpy){
      convertgrid(s,  d, overviews = FALSE, verify = verify, ...)
    } else {
      if(.status$verbosity >  2)
        cat("Using gdalUtilities::gdal_translate equivalent system command:\n\t",
            "gdal_translate ", s, " ", d, "  -co compress=LZW -stats\n", sep  ="")
      gdalUtilities::gdal_translate(s, d, co ="compress=LZW", stats = TRUE)
      
    }
  }
  dd <- sd
  dd$type <- paste0(sd$type, "_tif")
  dd$name <- destname

  writemosaicdesc(dd, destination, updatetimestamp = FALSE, compression = "compressed")
  
  # Copy old stitch log if it exists
  ssl <- paste0(source, "/stitchlog.txt")  # source stitch log
  if(file.exists(ssl)){
    dsl <- paste0(destination, "/stitchlog.txt")
    file.copy(ssl, dsl)
  }
  
  # Delete source mosaic if appropriate all must be satisfied
  #   1. conversion was verified 
  #   2. A complete version of the mosaic exists
  #
  if(deletesource && usearcpy && verify){
    # If we are deleting source and we've verified (happens in the arcpy version only) then
    # delete the source mosaic
  
      cat("Deleting source mosaic:", source, "\n")
      Sys.sleep(1)
      a <- unlink(gsub("/$|\\\\$", "", source), recursive = TRUE)
      if(a == 1) stop("Mosaic conversion completed and verified but source mosaic could not be deleted")
  } # end if deleting
  
}