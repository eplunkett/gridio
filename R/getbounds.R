getbounds <- function(x){  
  # returns the bounds of the object (in same format used by bbox function in package sp
  if(is.character(x)){
    x <- griddescribe(x[1])
    if(x$type == "missing") {
      stop("getbounds called with a character argument that is not", 
           " a path to a grid on disk: ", x[1])
    }
  } 
  if (inherits(x, "list") && 
      all(c("xll", "yll", "nrow", "ncol", "cellsize") %in% names(x))) {
    b <- data.frame(min=c(x$xll, x$yll), 
                    max=c(x$xll + x$ncol * x$cellsize, x$yll + x$nrow * x$cellsize) )
    rownames(b) <- c("x", "y")
    return(b)
  }  
  stop("Couldn't get bounds")
}

