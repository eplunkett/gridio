newtile <- function(coreonly=FALSE){
  #------------------------------------------------------------------------------------------------#
  #  newtile   Ethan Plunkett    March 8 2012
  # 
  #  funciton to create an empty grid matching the extent of the current tile (including it's buffer)
  #   (this is intended for pre-allocating a grid to be filled while processing in subtiles)
  #  Changes :
  #    March 14, added coreonly argument
  #------------------------------------------------------------------------------------------------#
  td <- tiledetails(coreonly=coreonly)
  result <- list()
  result$m <- matrix(NA, nrow=td$nrow, ncol=td$ncol)
  for(i in c( "xll", "yll", "nrow", "ncol","cellsize"))
    result[[i]] <- td[[i]]
  class(result) <- c("grid", class(result))
  return(result)
}
