
cacheinit <- function(){
  # Creates an empty gridcache.txt on the local machine.  Previous versions of gridcache.txt are 
  # deleted.  Also clears the entire gridcache directory.  Use with care!
  #
  #         
  ci <- getcacheconfig(reread=TRUE)
  
  if(!ci$on)  return()  # if caching isn't on then don't do anything
  
  
  cp <- gsub("(/|\\\\)$", "", ci$cachepath)
  
  if(file.exists(cp)){
    cat("This will delete the entire gridcache on this machine: ", cp, "are you sure? (y/n)\n")
    a <- readLines(n=1)
    if(! a %in% c("y", "Y", "YES", "yes", "Yes")){
      cat("cacheinit canceled\n")
      return(invisible(NULL))
    } 
    a <- unlink(cp,  recursive=TRUE, force=TRUE)
    if(a == 1) stop("Couldn't delete cachdir.  Sorry")
  }
  dir.create(ci$cachepath)
  dir.create(paste0(ci$cachepath, "/index"))
  
  
  
  l <- paste(.status$cachecolumns, collapse="\t")
  files <- paste0(ci$cachepath, "/index/", 1:ci$nbins , ".txt" )
  for(file in files)
    writeLines(l, con=file)
  
  file <- paste0(ci$cachepath, "/index/cacheinfo.txt" )
    writeLines(c("0", "0"), con = file)
  
  cat("Cacheinit complete.\n")
}

