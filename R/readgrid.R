readgrid <-
function(path, as.matrix=FALSE, na.value=-9999, cacheok=FALSE){
#--------------------------------------------------------------------------------------------------#
#  readgrid    Ethan Plunkett Jan 2011
#  Function to read in an ESRI grid file.  
#  gridinit and setwindow must be called first
#  Argument:
#    path : the path to the grid to be read (relative to the path set in gridinit)
#    no.data.value : the value used to send no data to edi's C function
# NA's will be converted to this value in the matrix when it is passed to the C function.  
# Capslib with then write out the grid with NA substitued for this value.  The only important thing
# is that this be a value that doesn't occur legitimately (as a non-NA) in the grid. 
#--------------------------------------------------------------------------------------------------#
# Arguments: (for C function)  
#  grid_name     (char*) : path (optional) and name of the target grid relative to the declared 
#    root of the grid pool  
#  missing_value (int)   : value in grid to be converted into ESRI MISSINGDATA
#  mtx_grid*  (F8/I4) : pointer to empty matrix for storing the result
#--------------------------------------------------------------------------------------------------#


  d <- griddescribe(path)
  if(d$type == "missing") stop("The grid ", path, " was not found.") 
  
	return(readblock(path, 1, 1, nrow=d$nrow, ncol=d$ncol, as.matrix=as.matrix, na.value=na.value, 
                   cacheok=cacheok, desc=d))

  
}

