importantconfig <- function(){
  #  import ant config
  #  if anthill  is running import it's configuration information
  if(anthillrunning()){
    # If anthill is running anthill pull variables from anthill's configuration
    arcgisversion <- anthill:::.settings$arcgisversion
    setpythonpath(arcgisversion = arcgisversion)
    
    # For these three variables copy the value from anthill (set by anthill config.txt)
    # to gridio2.
    for(var in c("arcgishost", "reference", "tiff")){
      a <- get(var, envir = anthill:::.settings)
      if(a %in% c("reference"))
        a <- gsub("\\\\", "/", a)
      if(!is.null(a) && !is.na(a) && a != "")
        assign(var, a, envir = .status)
    }
  } else {
    setpythonpath(arcgisversion = .status$arcgisversion)
  }
  
  
}