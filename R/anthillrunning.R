
anthillrunning <- function(){
  return("package:anthill" %in% search() && 
           !is.null(anthill:::.settings$path) && 
           !is.na(anthill:::.settings$path) && 
           !anthill:::.settings$path == ""
  )
}

