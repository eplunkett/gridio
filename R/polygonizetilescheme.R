


polygonizetilescheme <- function(subtiles, coreonly = TRUE ){
  # Create polygons for each pane and saved in a cached file that is specific to the extent and the pane size.
  # (or load from cached file - to save time.)
  
  
  tiles <- seq_len(ntiles())
  
  # By default include subtiles if they are configured (with subtileinit)
  if(missing(subtiles)) 
    subtiles <- !is.na(.status$subtilescheme$rows) 
  
  
  if(subtiles & is.na(.status$subtilescheme$rows) )
    stop("subtiles cannot be polygonized if not defined - see subtileinit function")
  
  if(is.na(tilescheme()$rowsize))
    stop("tilescheme is not defined - see tileinit")
  
  
  if(subtiles){
    polycoords <- vector(mode = "list", length = ntiles())
    ids <- vector(mode = "list", length = ntiles())
    tiledata <- vector(mode = "list", length = ntiles())
    for(i in seq_along(tiles)){ 
      tile <- tiles[i]
      settile(tile)
      
      subpolycoords <- vector(mode = "list", length = nsubtiles())
      subtiledata <- data.frame(tile = tile, 
                                subtilenumber = seq_len(nsubtiles()),
                                subtileid = NA)
      for(j in 1:nsubtiles()){   
        setsubtile(j)
        id <- stid()
        subtiledata$subtileid[j] <- id
        st <- subtiledetails(coreonly=coreonly)
        xmin <- st$xll
        xmax <- st$xll + st$ncol * st$cellsize
        ymin <- st$yll 
        ymax <- st$yll +st$nrow * st$cellsize
        coords=cbind(c(rep(xmin, 2), rep(xmax, 2), xmin), c(ymin, ymax, ymax, ymin, ymin))
        subpolycoords[[j]]  <- coords
      } # end subtile loop
      polycoords[[i]] <- subpolycoords
      tiledata[[i]] <- subtiledata
      
    }
    tiledata <- as.data.frame(do.call("rbind", args = tiledata ))
    polycoords <- do.call("c", args = polycoords) # from nested list to list
  } else {   ## No subtiles:
    polycoords <- vector(mode = "list", length = ntiles())
    tiledata <- data.frame(tile = tiles, row = NA, col = NA)
    for(i in tiles){ 
      settile(i)
      st <- tiledetails(coreonly=coreonly)
      ts <- tilescheme()
      tiledata$row[i] <- ts$i
      tiledata$col[i] <- ts$j
      xmin <- st$xll
      xmax <- st$xll + st$ncol * st$cellsize
      ymin <- st$yll 
      ymax <- st$yll +st$nrow * st$cellsize
      coords=cbind(c(rep(xmin, 2), rep(xmax, 2), xmin), c(ymin, ymax, ymax, ymin, ymin))
      polycoords[[i]] <- coords
    } # end tile loop
  }  # end no subtiles
  
  l <- lapply(polycoords, function(x) sf::st_polygon(list(x)))
  sf_polys <- tiledata
  sf_polys$geometry <- sf::st_sfc(l)
  sf_polys <- sf::st_as_sf(sf_polys)
  rownames(sf_polys) <- NULL
  
  ac <- .status$activeconnection$connection
  if(!is.na(ac)){
    ref <- .status$connections$refgrid[ac]
    if(!is.na(ref))
      sf::st_crs(sf_polys) <- sf::st_crs(rasterinfo(ref)$crs)
  }
  return(sf_polys)
}  # end function definition