if(FALSE){
  
  library(gridio2)
  library(anthill)
  config()
  set.owner("ebp")
  
  
  x <- "X:/LCC/GIS/Future/version5/1/"
  
  x <- "X:/LCC/GIS/Future/phase3baseline/"
  
  x <- "X:/LCC/GIS/Work/Biotic/species/"
  
  
  # 1. Scan for raster files
  a <- scanrasters(x)
  
  # 2. Delete redundant grid mosaics (These exist as tiff mosaics as well)
  print(a$redundant.grid.mosaics) # look at them
  
  if(FALSE){  # Don't do this unless run manually:
  unlink(a$redundant.grid.mosaics, recursive = TRUE)
  }
  
  # 2. Delete redundant GRID files ??
  print(a$redundant.grids)

  ### I'm not sure how to go about this given that there may be non-redundant 
  ### grids in the same directory so can't just unlink the ones we don't want
  ### as that would result in a corrupt Arc Workspace
  
  # 3. Convert unconverted mosaics
  # Not doing all at once.  
  b <- a$unconverted.mosaics
  b <- b[grep("LCAD2010", b)]
  
  compress(a$uncompressed.tiffs[1])
  launchcompression(a$uncompressed.tiffs[-1])
  
  b <- a$uncompressed.tiffs
  b <- b[!grepl("speciesSummary", b)]
  
  launchcompression(b)

    library(gridio2)
  
  x <- "X:/LCC/GIS/Work/Biotic/species/"
  
  exclude <- c("X:/LCC/GIS/Work/Biotic/species/2010/", 
               "X:/LCC/GIS/Work/Biotic/species/Climate/",
               "X:/LCC/GIS/Work/Biotic/species/phase5/")
  a <- scanrasters(x, exclude = exclude)
  
  
}

scanrasters <- function(x, delay = 7, exclude){
  # Function to scan for unconverted or uncompressed raster and mosaic files
  # 
  #  x = path to a directory to be recursively scanned for grids, tifs, and mosaics
  #  delay = time in days after mosaic or tif creation for which we consider it 
  #    compressable. Files created within the last [delay] days will be 
  #   listed as compressed even if they aren't. This allows avoiding compressing 
  #    recently created files that might still be in active use.
  #  exclude = vector of paths to exclude.
  # 
  
  cat("Scanning ", x, "for mosaics, TIFFs, amd grids.\n")
  system.time(
    a <- list.files(x, "^mosaicdescrip.txt$|.tif$|^sta.adf$", recursive = TRUE, full.names = TRUE)
  )
  
  a <- gsub("//", "/", a)
  
  if(!missing(exclude)){
    cat("Excluding:\n\t", paste(exclude, collapse = "\n\t"), "\n", sep = "")
    exclude <- paste(paste0("^", tolower(exclude)), collapse = "|") # convert to reg expression forcing the beginning of the string to match
    sv <- !grepl(pattern = exclude, a, ignore.case = T)
    cat("Excluded:", sum(!sv), "files. This count includes rasters inside of mosaics.\n"  )
    a <- a[sv]  
  }
  # Break out into tifs, grids, and mosaics and then separate the tifs and grids into mosaic files and
  # and stand alone (non-mosaic) raster files.
  
  tifs <-a[grep(".tif$", a)]
  mds <- a[grep(".txt", a)]
  mosaics <- gsub("mosaicdescrip.txt", "", mds)
  tifdirs <- gsub("/[^/]*$", "/", tifs)  # the directory that holds the tif (will be compared to mosaics)
  grids <- a[grep("sta.adf$", a)] 
  grids <- gsub("/sta.adf$", "", grids)
  grid.dirs <- gsub("[^/]*/[^/]*$", "", grids) # directory that would be the mosaic directory if the grids was in a mosaic
  
  sv <- tifdirs %in% mosaics
  mosaictifs <- tifs[sv]
  othertifs <- tifs[!sv]
  
  sv <- grid.dirs %in% mosaics
  mosaicgrids <- grids[sv]
  othergrids <- grids[!sv]  # stand alone grids
  
  cat("Scanning mosaicdescp.files for compression, format, and timestamps\n")
  mosaic.compressed <- rep(FALSE, length(mosaics))
  mosaic.timestamps <- rep(NA_character_, length(mosaics))
  mosaic.types <- rep(NA_character_, length(mosaics))
  report.increment <- ceiling(length(mds)/30)
  for(i in seq_along(mds)){
    md <- mosaicinfo(mosaics[i], lock = FALSE)
    mosaic.types[i] <- md$type
    mosaic.timestamps[i] <- md$timestamp
    mosaic.compressed[i] <- md$compression != "no"
    if(i %% report.increment == 0)
      cat(".")
  }
  cat("\n")
  mosaic.is.tif <- grepl("tif$", mosaic.types)
  
  expected.types <- c("original_tif", "original", "derived_tif", "derived")
  if(!all(mosaic.types %in% expected.types))
    cat("Unexpected mosaic types:\n\t'", paste(setdiff(unique(mosaic.types), expected.types ), collapse = "'\n\t'"), "'\n", sep ="")
  
  mosaic.timestamps <- as.POSIXct(mosaic.timestamps, format = .status$timeformat)
  lag <- Sys.time() - mosaic.timestamps
  
  unconverted.mosaics <- mosaics[!mosaic.is.tif]
  uncompressed.mosaics <- mosaics[mosaic.is.tif & !mosaic.compressed & lag > delay]
  
  cat("Scanning non-mosaic tifs for DEFLATE compression\n")
  othertif.compressed <- rep(FALSE, length(othertifs))
  othertif.timestamps <- as.POSIXct(rep(NA, length(othertifs)))
  is.geotiff <- rep(TRUE, length(othertifs))
  rasterinfo.failed <- rep(FALSE, length(othertifs))
  report.increment <- ceiling(length(othertifs)/30)
  for(i in seq_along(othertifs)){
    
    e <- tryCatch({
      ti <- rasterinfo(othertifs[i])
    }, error = identity)
    
    if(inherits(e, "error")){
      rasterinfo.failed[i] <- TRUE
      othertif.compressed <- FALSE
      next
    }
    if(ti$type == "missing"){
      # Sort out the standard tiffs
      is.geotiff[i] <- FALSE
      next 
    }
    othertif.compressed[i] <- "COMPRESSION=DEFLATE" %in% ti$imagestructuremetadata
    othertif.timestamps[i] <- file.info(othertifs[i])$mtime
    if(i %% report.increment == 0)
      cat(".")
  }
  cat("\n")
  

  # othertif.timestamps <- as.POSIXct(othertif.timestamps)

  lag <- Sys.time() - othertif.timestamps
  uncompressed.tiffs  <- othertifs[!othertif.compressed & lag > delay  & is.geotiff & !rasterinfo.failed ]
  not.geotiff <- othertifs[!is.geotiff]  
  info.failed <- othertifs[rasterinfo.failed]
  
  othertifs <- othertifs[is.geotiff & !rasterinfo.failed]
  rm(is.geotiff, othertif.compressed, othertif.timestamps, lag)  
  
  # Look for redundancy to report
  base <- list(grid = gsub("_s/*$|/$", "", othergrids), 
               gmosaic = gsub("_m/*$|/$", "", unconverted.mosaics ), 
               tiff = gsub(".tif$|_s.tif$", "", othertifs), 
               tmosaic = gsub("_tm/*$|/$", "", mosaics[mosaic.is.tif]) )
  
  redundant.grid.mosaics <- unconverted.mosaics[base$gmosaic %in% base$tmosaic]
  redundant.grids <- othergrids[base$grid %in% base$tiff | base$grid %in% base$tmosaic]
  redundant.tiffs <- othertifs[base$tiff %in% base$tmosaic]


  
  # drop trailing slashes and double slashes
  for(a in c("uncompressed.tiffs", "uncompressed.mosaics", "unconverted.mosaics", 
             "othergrids", "redundant.grid.mosaics", "redundant.grids", "redundant.tiffs", "not.geotiff")){
    
    b <- get(a)
    b <- gsub("//", "/", b)
    b <- gsub("/$", "", b)
    assign(a, value = b)
    
    
  }
  
  
  # Drop the redundant mosaics from the unconverted mosaics
  unconverted.mosaics <- setdiff(unconverted.mosaics, redundant.grid.mosaics)
  othergrids <- setdiff(othergrids, redundant.grids)  
  
  cat("Dir: ", x, " has:\n", 
      "\t", length(unconverted.mosaics), " unconverted (grid based) mosaics (+", length(redundant.grid.mosaics), " redundant)\n",
      "\t", length(othergrids), " independent unconverted grids (+", length(redundant.grids),  " redundant)\n", 
      "\t", length(uncompressed.mosaics), " of ", sum(mosaic.is.tif), " TIFF mosaics are uncompressed\n",
      "\t", length(uncompressed.tiffs), " of ", length(othertifs), " independent TIFFs are uncompressed\n",
      "\t", length(redundant.grid.mosaics), " grid based mosaics are redundant (also exist as TIFF based mosaics)\n",
      "\t", length(redundant.grids), " grids are redundant (also exist as TIFFs or TIFF based mosaics)\n",
      "\t", length(redundant.tiffs), " TIFFs are redundant (also exist as TIFF based mosaics)\n", 
      sep=""
  )
  if(length(not.geotiff) > 0) cat("\t", length(not.geotiff), " .tif files aren't geoTIFFs\n", sep = "")
  if(length(info.failed) > 0) cat("\t", length(info.failed), " .tif files failed to scan with rasterinfo and were excluded from the analysis\n", sep="")
  
  return(list(uncompressed.tiffs =  uncompressed.tiffs, 
              uncompressed.mosaics = uncompressed.mosaics, 
              unconverted.mosaics = unconverted.mosaics, 
              unconverted.grids = othergrids, 
              redundant.grid.mosaics = redundant.grid.mosaics, 
              redundant.grids = redundant.grids,
              redundant.tiffs = redundant.tiffs, 
              not.geotiff = not.geotiff, 
              rasterinfo.failed = info.failed
              ))
  
}
