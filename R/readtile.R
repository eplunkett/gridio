readtile <-
function(path, coreonly=FALSE, as.matrix=FALSE, na.value=-9999, cacheok=FALSE, ...){
#--------------------------------------------------------------------------------------------------#
#  readtile      Ethan PlunkettJanuary 2011
#  This function reads the current tile from a grid based on the tile index, stored in tilescheme.
#  
#  Path is the only required argument.
#
#  Arguments :
#    path :  the path to the grid 
#     coreonly : if TRUE the function will return only the core of the current tile
#     as.matrix : if TRUE the function will return a matrix otherwise it will return a "grid" object     
#     na.value : the value used to encode NA's during transfer.  Should be a value that does not
#        occur in your data.  
#--------------------------------------------------------------------------------------------------#
if(is.na(.status$tilescheme$tileno)) stop("You must call tileinit or subtileinit prior to readtile.")

buffer <-  ifelse(coreonly, 0, .status$tilescheme$buffer)
if(.status$tilescheme$tileno == 0) stop("Current tile is undefined. Call settile or firsttile.")
d <- tiledetails() 
readblock(path=path, startrow=d$startrow, startcol=d$startcol, nrow=d$nrow, ncol=d$ncol, 
          buffer=buffer, as.matrix=as.matrix, na.value=na.value, cacheok=cacheok, ...)
}

