.makewindow <- function(nrow, ncol, xll, yll, cellsize){

  stop("makewindow is obsolete")
  #--------------------------------------------------------------------------------------------------#
  # Arguments (c function):
  #    err  (int*)   :  pointer to a  variable containing the error code
  #    y_count   (int*)    : pointer to a variable containing the number of rows in the target grid
  #    x_count   (int*)    : pointer to a variable to receive the number of columns in the target grid
  #    y_ll      (double*) : pointer to a variable to receive the y coordinate of lower left corner
  #    x_ll      (double*) : pointer to a variable to receive the x coordinate of lower left corner
  #    cell_size (double*) : pointer to a variable to receive the size of a cell in map units
  #	  block_only(int*)    : pointer to a variable to receive the server mode: 1 if only block
  #			IO is permitted, 0 otherwise
  #
  #--------------------------------------------------------------------------------------------------#
  if(.status$verbosity >= 5){
    cat("\n.makewindow called\n")
  }
  l <- c(nrow, ncol, yll, xll, cellsize)
  if(any(is.na(l))) stop("NA argumenents not allowed")
  if(any(is.null(l)))stop("NULL arguments not allowed")
  done <- FALSE
  .status$gsdowntime <- 0  # The amount of time the gridserver has been down
  while(!done){
    st <- system.time({
      r <- .C("R_makewindow",
              integer(1),
              as.integer(nrow),
              as.integer(ncol),
              as.double(yll),
              as.double(xll),
              as.double(cellsize),
              integer(1),
              PACKAGE = .status$dllname)
    })
    .status$gridwait <- .status$gridwait + as.numeric(st[3])
    done <- .donetrying(code=r[[1]], st, reset=FALSE)
  }


  parseerror(r[[1]], where="makewindow")
  r <- r[-1]

  names(r) <- c("nrow", "ncol", "yll", "xll", "cellsize", "tileonly")
  r$tileonly <- as.logical(r$tileonly)
  r <- r[c("nrow", "ncol",  "xll", "yll", "cellsize", "tileonly")]  # reorder and drop "grid"
  r$set <- TRUE


  invisible(r)
}
