newmosaic <- function(path, as.integer=FALSE){
  # Arguments:
  # path : the path where the new mosaic should be created
  # as.integer : if TRUE the new mosaic grid will be an integer grid.  Otherwise it will be contain
  #     floating point numbers.
  #  This function creates a new, empty, mosaic on disk at the specified path.
  # See also makemosaic to create a non-empty mosaic grid from a complete grid.
  #
  # This funciton is dependent on anthill because it uses the locking mechanisms of anthill.  
  #  If anthill isn't loaded it will skip the locking. If anthill is loaded than it must be 
  #   configured to support locking.
  #
  
  if(missing(as.integer)) as.integer = FALSE   # necessary when missing value is passed via ...
  
  if(is.na(.status$mosaic$rowsize)) stop("mosaicinit must be called prior to newmosaic.")
  path <- gsub("\\\\", "/", path)
  path <- gsub("/$", "", path)
  

  
  locking <- "anthill" %in% .packages()
  
  if(locking){
    anthill::get.lock(path=path, message=paste("newmosaic (R-", anthill::get.thread(), ")", sep="") )
    on.exit(anthill::return.lock(path)) # executes on completion or error
  }

  
  if(ismosaic(path, lock=FALSE)) return(invisible())
  
  if(file.exists(path)){
    l <- list.files(path)
    if(length(l)!=0) stop("Can't create mosaic at: ", path, " that directory isn't empty.")
  }
  
  makedir(path, recursive=TRUE, locking=FALSE) 
  
  panemap <- paste(rep("0", .status$mosaic$panes), collapse="")
  name <- gsub("^.*/", "", path)
  now <- format(Sys.time(), format=.status$timeformat)
  
  mosaictype <- ifelse(.status$tiff, "original_tif", "original")
  cat(mosaictype, "\n")
  # Write descriptor file
  lines <- c( 
    paste("name = '", name,"'", sep=""), 
    paste("type = '", mosaictype, "'", sep=""), 
    paste("timestamp = '", now , "'", sep=""), 
    "source = ''",
    "sourcetimestamp = ''", 
    paste("extent = ", 
          with(.status$mosaicwindow,
               paste(
                 ncol, 
                 nrow, 
                 formatdouble(xll), 
                 formatdouble(yll), 
                 formatdouble(cellsize), 
                 "-9999",
                 ifelse(as.integer, 1, 2), sep=",")), sep=""),
    paste("rowsize = ",.status$mosaic$rowsize, sep=""),
    paste("colsize = ", .status$mosaic$colsize, sep=""),
    paste("rows = ", .status$mosaic$rows, sep=""), 
    paste("cols = ",.status$mosaic$cols, sep=""), 
    paste("panemap = '", panemap, "'", sep="")
  )
  writeLines(lines, con=paste(path, "/mosaicdescrip.txt", sep=""))
  cleargridinfo(path)
  
  
}
