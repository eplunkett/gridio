pointstogrid <- function(x, y, z, buffer, cellsize, grid, FUN=sum, ...){
	# Function to make a grid from x, y and optionally z values
	# The new grid will have zero for cells that are occupied 
	# Arguments :
	#	x, y,  coordinates of points in map units
	#  buffer, the buffer to extend around the grid around the points in mapunits
	#	cellsize, the dimension of each cell in map units
	#  grid (optional) - a grid can be used to define the new extent.  If supplied the extent of the new grid will match the extent of grid and cellsize and buffer will be ignored
	#	FUN the function to be used to combine multiple z values when more than one point occupies a cell
	#	... other arguments to be passed to FUN
	
	if(length(x) != length(y)) stop("x and y must be of equal length")

	if(missing(grid)){
		grid <- list()
		grid$xll=((min(x, na.rm=TRUE)-buffer) %/% cellsize) * cellsize
		grid$yll= ((min(y, na.rm=TRUE)-buffer) %/% cellsize) * cellsize
		grid$cellsize = cellsize
		grid$nrow = (max(y)+buffer - grid$yll) %/% cellsize + 1
		grid$ncol = (max(x)+buffer - grid$xll) %/% cellsize + 1
		grid$m <- matrix(0, grid$nrow, grid$ncol)
		class(grid) <- c("grid", class(grid))
	} else {  # grid supplied 
		if(!inherits(grid, "grid")) stop("grid must be a object of class grid.  See ?readgrid.")
		grid$m[,] <- 0
	}
	
	
	if(missing(z)) z <- rep(1, length(x)) else 
		if(length(z) != length(x)) stop("z, x, and y must be of equal length")
	pts <- cbind( y2r(y, list=grid), x2c(x, list=grid))
	ids <- paste(pts[,1], pts[,2])
	sv <- duplicated(ids)
	dup.rows <- which(sv)
	if(length(dup.rows > 1)) for(i in 1:length(dup.rows)){
		# Note there's probably some quicker way to do this without a loop but I can't think of it right now
		# i <- dups[1]
		
		first <- which(ids == ids[dup.rows[i]])[1]
		z[first] <- FUN(z[which(ids == ids[dup.rows[i]])])
	}
	z <- z[!sv]
	pts <- pts[!sv, ]	
		
	grid$m[pts] <- z
	
	
 	grid <- grid[ match(c("m", "xll", "yll", "nrow", "ncol", "cellsize"),names(grid))] # put in proper order
	class(grid) <- c("grid", class(grid))  # re assign the class
	
	return(grid)
}

