coincide <- function(x, y){
  #------------------------------------------------------------------------------------------------#
  #  coincide      Ethan Plunkett      Feb 24, 2012
  #
  #  This function returns TRUE if a and b are coincident (matching in extent, cellsize, and cell 
  #   allignement) and FALSE otherwise.
  #   Arguments:
  #     x, y :  grids or other list objects with elements ( xll, yll, nrow, ncol, cellsize) such as
  #     the objects returned from griddescribe and tiledetails.  
  # 
  #------------------------------------------------------------------------------------------------#
    for(a in  c("xll", "yll", "nrow", "ncol", "cellsize")){
      if(!a %in% names(x)) stop("x is missing a", a, "element. Cannot check congruency.")
      if(!isTRUE(all.equal(x[[a]], y[[a]], check.attributes=FALSE, check.names=FALSE))){
        #  cat("x and  y have different values for", a, "\n")
        return(FALSE)
      }
    }
    return(TRUE)
  }
