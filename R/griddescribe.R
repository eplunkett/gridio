griddescribe <- function(path, getstats = FALSE, cache = TRUE){
  
  # path <- "C:/temp/gridiotest/makemosaic/dem/" ; getstats=TRUE
  path2 <- formatgridinfopath(path)
  
  # If caching is on and the path is in the cache return it
  if(cache && !getstats  && path2 %in% ls(.status$gridinfo)){
    return(.status$gridinfo[[path2]])
  }
  
  locking <- anthillrunning()
  v <- .status$verbosity
  if(v > 1) cat("griddescribe called on ", path, "\n")
  if(is.na(x=.status$activeconnection$connection)) stop("You should call gridinit prior to using this function. (activeconnection$connnection is NA)")
  
  
  # Determine if it's a mosaic
  mdp <- paste(path, "/mosaicdescrip.txt", sep="")
  if(locking){ # get lock
    anthill::get.lock(path)
    on.exit(anthill::return.lock(path))
  }
  
  mosaic <- file.exists(mdp)
  if(mosaic)
    md <- mosaicinfo(path, lock=FALSE, cache = FALSE)
  
  if(locking){  # return lock
    anthill::return.lock(path)
    on.exit()
  }
  
  
  if(mosaic){
    if(v > 2) cat("\tmosaic\n")
    
    result <- c(md$extent, list(min=NA, max=NA, mean=NA, sd=NA, mosaic=TRUE, mosaicinfo=md))
    
    if(getstats){
      # Make character vector of pane ids (padded with zeros)
      panes <- as.character(which(md$panemap))
      digits <- nchar(length(md$panemap))
      makename <- function(name, i){  # function to make the filename from the mosaicname and the panenumber
        num <- paste(paste(rep(0, digits), collapse="", sep=""), i, sep="")
        num <- substr(num, start=nchar(num)- digits +1 , stop=nchar(num) )
        name <- substr(name, start=1, stop=13-digits)
        return(paste(name, num, sep=""))
      }
      panenames <- makename(md$name, panes)  
      files <- paste(path, "/", panenames, "/", panenames, sep="")
      
      minval <- NA
      maxval <- NA
      for(i in 1:length(files)){
        d <- griddescribe(files[i], getstats = TRUE)
        if(!is.na(d$min))
          minval <- min(minval, d$min, na.rm=TRUE)
        if(!is.na(d$max))
          maxval <- max(maxval, d$max, na.rm=TRUE)
      }
      result$min <- minval
      result$max <- maxval
    }
    
  }  else {
    
    # The grid isn't a mosaic 
    oac <- .status$activeconnection
    
    setcache(x=TRUE)
    activateconnection()
    result <- .griddescribe(path, updatestats = getstats)
    gaps <- c(0, .1, .1, .2, .2)
    # This next section is here because in local mode 
    # there's a slight chance .griddescribe will fail and report a grid as missing when it's there
    # if someone else is writing to the grid with a server.
    # To get around this we first try a few extra times locally and then connect to the proper server.
    i <- 1
    while(result$type == "missing" && i <= length(gaps)){  # try some more locally
      Sys.sleep(gaps[i])
      result <- .griddescribe(path)
      i <- i + 1
      if(.status$verbosity > 4 ) cat("retrying missing grid attempt", i, "\n")
    }
    if(result$type == "missing"){  # try with the gridserver (once)
      con <- findconnection(x=.status$referencewindow, path=path)
      if(!is.na(con)){
        setconnection(con)
        setcache(FALSE)
        activateconnection()
        result <- .griddescribe(path, getstats)
      }
    }
    
    setcache(oac$cache)
    setconnection(oac$connection)
    activateconnection()
    if(!is.na(oac$pendingconnection)) setconnection(oac$pendingconnection)
    if(!is.na(oac$pendingcache)) setcache(oac$pendingcache)
    
    
    
    result <- c(result, list(mosaic=FALSE))
    
  } # end else (grid is not mosaic)
  
  class(result) <- c("griddescription", class(result))
  
  
  if(gridioverbosity() > 2)
    cat("caching gridinfo for: ", path2, '\n')
  
  addtogridinifo(path2, result)
  
  
  
  return(result)
  
}
