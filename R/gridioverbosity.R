gridioverbosity <- function(x){
  if(missing(x))
    return(.status$verbosity)
  x <- as.numeric(x)[1]
  .status$verbosity <- x
}

.verbosity <- function()
  .status$verbosity