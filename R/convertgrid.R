if(FALSE){
  script.path <- "X:/Users/Ethan/gridio/inst/python/CopyRaster.py"
  source <- "X:/LCC/GIS/Final/NER/caps_phase5/grids/rawgradient/rawgradient03/rawgradient03"  
  destination <- "X:/Working/ethan/temp2/rawgradient03.tif"
  
  file.exists(destination)
  convertgrid(source,  destination, crs ='EPSG:5070', script.path = script.path)
  
}


convertdslgrids <- function(source, ...){
  # for dsl use to force cleaner projection definition
  convertgrids(source, crs = "EPSG:5070", ...)
}

convertgrids <- function(source, ...){
  # function to convert multiple grids to tif
  # returning a table with any errors (doesn't throw errors)
  destination <- paste0(source, ".tif")
  result <- data.frame(source = source, destination = destination, error = FALSE, message = "")
  for(i in seq_along(source)){
    a <- tryCatch(convertgrid(source = source[i], ...),  error = function(e) e)
    if(inherits(a, "error")){
      result$error[i] <- TRUE
      result$message[i] <-  gsub("[\t\n\r]", " ", a$message)
    }
  } # end for
  return(result)
}
  
  

convertgrid <- function(source, destination, verify=TRUE, crs, overviews = TRUE, python.path, script.path){
  # Function to convert a grid to a tif
  #     calls arcpy to copy source to a temporary tif and then
  #      gdal_translate to  convert the temporary tif to destination
  # This was made for for internal use by convertmosaic() to convert
  #   grid based mosaics to tif based mosaics.
  #
  #     Using gdal_translate directly on the arc grids resulted in 
  #    about 25% of our initial tests in corrupt NA values. Converting with 
  #    arcpy didn't exhibit the same problems but only produces
  #    tiled tif  and  we want striped so I'm using a two step process
  #    (1) arcpy copyraster management t (2) gdal_translate
  #  
  #
  #  Ethan Plunkett Nov 2021
  #  
  #  Arguments
  #    source :  path to a grid to be converted
  #    destination: path to a tif to be created from  source
  #    python.path : path to python on the system  (optional)
  #    script.path :  path to the template python script (almost never needed)
  #       only use if you need to modify the  script  because  of changes in python
  #       or for testing and debugging.
  #    verify  : if TRUE verify the final copy by comparing it to the
  #      source data, reading the source with gridio (1) and the tif
  #      with terra/gdal.  This may only work temporarilly 
  #       as gridio2 does not support grids
  #    overviews : if TRUE (the default) create overviews for the destination file
  #      overviews will have 
  #     crs :  if supplied the projection will be changed
  #      to this (without reprojecting). Can be an epsg code  eg : "EPSG:5070" 
  #      or the path to a wkt file. Many of the dsl mosaics
  #      I'm converting don't have projection info; this can be 
  #      used to add it. 
  #     
  
  if(missing(destination))
    destination <- paste0(source, ".tif")
  
  if(file.exists(destination))
    stop("destination already exists", destination)
  
  if(!grepl("\\.tif$", destination))
    stop("Destination should be a tif file")
  
  intermediate <- gsub(".tif$", "_temp.tif", destination)  # output from arcpy conversion
  intermediate2 <- gsub(".tif$", "_temp2.tif", destination)  # output from gdal translate conversion
  
  if(file.exists(intermediate))
    stop("intermediate file already exists", intermediate,  "\n")
  if(file.exists(intermediate2))
    stop("intermediate file already exists", intermediate2,  "\n")
  
    
  if (missing(python.path)) 
    python.path <- .status$pythonpath
  if (missing(script.path)) 
    script.path = system.file("python/CopyRaster.py", 
                              package = "gridio2")
  workspace <- dirname(destination)
  complete <- paste0(destination, "_pycomplete.txt")
  temp.script.path <- paste0(destination, "_copyraster.py")
  
  # Edit python script 
  l <- readLines(con=script.path)
  l <- gsub("[source]", source, l,  fixed=TRUE)
  l <- gsub("[destination]", intermediate, l, fixed=TRUE)
  l <- gsub("[workspace]", workspace, l, fixed=TRUE)
  l <- gsub("[complete]", complete, l,  fixed=TRUE )
  
  temp.script.path <- paste0(destination, "_copyraster.py")
  writeLines(text=l, con=temp.script.path)

  command <- paste(shQuote(python.path), shQuote(temp.script.path))
  if(gridioverbosity() > 0)
    cat("Executing system command: ", command, "\n")
  a <- system(command= command, intern=TRUE)
  if(!file.exists(complete))
    stop("Arc Python conversion script failed to complete.")
  
  # Remove temporary files
  file.remove(temp.script.path)
  file.remove(complete)

  # If output file isn't there (yet) wait for it
  for(i in 1:4){
    if(i == 5)
      stop("Intermediate file: '", intermediate, "' appears to be missing (maybe the arcpy raster copy failed?).", sep="")
    if(file.exists(intermediate))
      break
    Sys.sleep(0.1)
  } 
  
  # Use gdal to make striped tif (intermediate2 file)
  if(gridioverbosity() > 0) 
    cat("Converting  to striped tif with gdal_translate\n")
  if(!missing(crs)){
    gdalUtilities::gdal_translate(intermediate, intermediate2, stats = TRUE, co = c("compress=DEFLATE"), a_srs = crs )
  }  else {
    gdalUtilities::gdal_translate(intermediate, intermediate2, stats = TRUE, co = c("compress=DEFLATE") )
  }
  Sys.sleep(0.1)
  if(!file.exists(intermediate2))  
    stop("gdal_translate failed to produce a file")
  
  # Copy to final destination
  rasterPrep::renameTif(intermediate2, destination)
  Sys.sleep(0.1)
  

  if(gridioverbosity() > 1) cat("Deleting intermediate tif\n")
  rasterPrep::deleteTif(intermediate)
  
  # calculate stats and histogram
  a <- utils::capture.output(
    gdalUtilities::gdalinfo(destination, stats = TRUE, hist = TRUE))
 
  # Add overviews 
  if(overviews){
    if(gridioverbosity() > 1) cat("Adding overviews to ", destination, "\n")
    a <- rasterinfo(destination)
    use.nearest <- grepl("int|byte", a$type, ignore.case = TRUE)
    method <- ifelse(use.nearest, "nearest", "average")
    rasterPrep::addOverviews(destination, method = method)
  }
  
  if(verify){
    verifyconversion(grid = source, tif = destination, err  = TRUE)
  }
  
  
  if(gridioverbosity() > 0) cat("Done")
  
}  # end function