setcache <- function(x){
  # x = a logical indicating whether or not the pending connection should be too a cached or standard version of the connection
  if(!is.logical(x) || is.na(x)) stop("x must be TRUE or FALSE")
  .status$activeconnection$pendingcache <- x
}
