subtileinit <- function(rowsize, buffer, tilesize, nr, colsize=rowsize, nc=nr){ 
  #------------------------------------------------------------------------------------------------#
  # subtileinit    Ethan Plunkett     March 6, 2012   
  # 
  #   This function initializes both a tilescheme and a subtile scheme to facilitate reading a grid
  #    in large chuncks (tiles) and processing in smaller chunks (subtiles).
  #  
  # arguments : 
  #    rowsize, colsize : the row and column dimensions of each subtile
  #    tilesize : tilesize sets the approximate cell dimension of each tile.  The tilesize 
  #      is constrained to be a multiple of the dimensions of the subtiles and so the realized tile
  #      size may be different than what is specified by this argument.  For finer control use nc,
  #      and nr instead. If tilesize is specified it will overide nc and nr.
  #    nr, nc : the number of  subtiles to incoporate into each tile (along the y and x dimensions).  
  #      Eg: if  rdim is 100 and nr=5 then then each subtile with be 100 cells high and each tile
  #      will be 500 cells high.
  # 
  #------------------------------------------------------------------------------------------------#
  w <- getwindow()
  if(!w$set) stop("setwindow must be called prior to this function")
  
  # Make sure inputs are integers
  rowsize <- floor(rowsize)
  buffer <- floor(buffer)
  colsize <- floor(colsize)
  
  # rowsize <- 100; colsize <- 150; buffer=15; tilesize=3000
  if(!missing(tilesize)){
    tilesize <- floor(tilesize)
    nr <- tilesize %/% rowsize  + round((tilesize %% rowsize)/rowsize)
    nc <- tilesize %/% colsize  + round((tilesize %% colsize)/colsize)
  }
  
  # guarentee integers (only relevant if tilesize wasn't set)
  nc <- floor(nc)
  nr <- floor(nr)
  
  #
  tileinit(rowsize=nr*rowsize, buffer=buffer, colsize=nc*colsize)  
  # and expand  lastrows and lastcols so they are a multiple of the subtile dimensions
  lr <- .status$tilescheme$lastrows 
  .status$tilescheme$lastrows <- ceiling(lr/rowsize)*rowsize
  lc <- .status$tilescheme$lastcols
  .status$tilescheme$lastcols <- ceiling(lc/colsize) *colsize
  
  # Intialize subtile scheme permenent components
  .status$subtilescheme$nc <- nc 
  .status$subtilescheme$nr <- nr
  .status$subtilescheme$rowsize <- rowsize
  .status$subtilescheme$colsize <- colsize
  
  # initialize subtile scheme's tile specific components 
  stinit()
  
}

