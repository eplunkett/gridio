# Note there are three ways in which this function manages the grid processing
#	1. The simplest if tilesize is set to 0 then the entire grid is read, upscaled, smoothed, downscaled and written
#	2. If tilesize is not 0 than tiles of that size (plus a buffer) are read, upscaled, smoothed, downscaled and (just the core of the tile) written,
#	3. If sd is really big than the tiles are read and upscaled in a loop until the entire upscaled grid is in memory.  

gaussiansmooth.character <- function(
  x, sd, max.r=3*sd, kernel.dim=21, outgrid,  overwrite=FALSE, 
  tilesize, max.p.na=1, sample.points, weights, method, 
  no.match=0, tiles, cacheok=TRUE, use.old=FALSE, na.value = NA, ...){
  
  if(!missing(tiles) & missing(tilesize)) 
    stop("If you specify tiles to run you must also specify the tilesize.")
  if(missing(tilesize)) tilesize = 2000
  
  
  if(!getwindow()$set) stop("gridinit must be called prior to this function.")
  xinfo <- griddescribe(x)
  if(xinfo$type == "missing") stop("Grid", x, "not found.")

  if(!coincide(xinfo, getwindow()))
      stop("Window extent does not match grid. Call setwindow first.")
  
  if(missing(method)) method <- 3
  if(!method %in% 1:3) stop("Method must be 1, 2, or 3")
    
  if(missing(outgrid)){
    no.out <- TRUE
  } else {
    no.out <- FALSE
    if(griddescribe(outgrid, cache = FALSE)$type != "missing"){
      if(overwrite) 
        gridkill(outgrid)
      else
        stop("grid \"", outgrid, "\" already exists use overwrite=TRUE or a different output grid.")
    }	
    if(.status$as.mosaic){
      newmosaic(outgrid, as.integer=FALSE)
    } else {
      gridcreate(outgrid, as.integer=FALSE)
    }
    
  }
  
  if(missing(sample.points) & no.out) 
    stop("There's no sense in running if you don't specify either an outgrid or sample.points")
  
  if(!missing(sample.points)){
    if(!all(c("x", "y") %in% names(sample.points))) 
      stop("sample.points must have columns named \"x\" and \"y\"")
    sampled.values <- numeric(nrow(sample.points))
    sampled.values[] <- NA
    sample=TRUE
  } else {
    sample=FALSE
  }
  
  
  
  # Method 1 - Read grid as a whole
  if(tilesize ==0 || method==1) {  # special case attempt to do it all in one shot
    if(gridioverbosity() > 0)
      cat("Reading grid ", x, "\n", sep="")
    g <- readblock(path=x, startrow=1, startcol=1, nrow=xinfo$nrow, ncol=xinfo$ncol, cacheok=cacheok)
    if(sample){
      res <- gaussiansmooth(g, sd=sd,max.r=max.r, kernel.dim=kernel.dim, 
                            sample.points=sample.points, max.p.na=max.p.na, 
                            weights=weights, no.match=no.match, use.old=use.old, 
                            na.value = na.value, ...)	
      rm(g)
      gc()
      if(!no.out) writeblock(res$grid, path=outgrid)
      return(res$sample)
    } else {
      gs <- gaussiansmooth(g, sd=sd,max.r=max.r, kernel.dim=kernel.dim, 
                           weights=weights, no.match=no.match, use.old = use.old, 
                           na.value = na.value, ...)
      rm(g)
      gc()
      if(!no.out) writeblock(gs, path=outgrid)
      return(invisible())
    }
  }
  
  # Stuff For both methods 2 and 3
  
  otilesize <- tilesize  # original tilesize
  
  # Calculate scaling details
  s <- kernelscalingdetails(max.r = max.r, 
                            tilesize = tilesize, 
                            cellsize = xinfo$cellsize, 
                            kernel.dim = kernel.dim,
                            method = method)
  factor <- s$factor
  buffer <- s$buffer
  tilesize <- s$tilesize  # realized tilesize 
  
  if(gridioverbosity() > 0){
    cat("Upscaling factor:", factor, "(cells)\nRealized tilesize:", 
        tilesize, "(cells)\n")
    cat("Buffer:", buffer, "\n")
  }
  tileinit(tilesize, buffer)
  if(missing(tiles)){
    tiles <- 1:ntiles()
  } else {
    if(otilesize != tilesize) 
      stop("Realized tilesize (", tilesize, ")", 
           "does not match tilesize argument (", otilesize, ") ", 
           "but tiles are specified. Either omit tiles or recalculate based ", 
           "on realized tilesize.")
      if(!all(tiles %in% 1:ntiles())) 
        stop("Not all tiles are in this tile scheme")
  }
  
  
  # Method 2 - Do each tile completely
  if(method==2){
 
    for(i in tiles){
      if(gridioverbosity() > 0)
        cat("Working on tile ", i, " of ", ntiles(), "\n", sep = "")
      settile(i)
      g <- readtile(x, cacheok=cacheok)
      if(sample){
        tc <- tiledetails()  # tile core
        xmin <- tc$xll
        ymin <- tc$yll
        xmax <- tc$xll + tc$ncol*tc$cellsize
        ymax <- tc$yll + tc$nrow * tc$cellsize
        sv <- sample.points$x > xmin & sample.points$x <= xmax  & 
          sample.points$y > ymin & sample.points$y <= ymax # selection vector for points in range
        res <- gaussiansmooth(g, sd=sd,max.r=max.r, kernel.dim=kernel.dim, 
                              sample.points=sample.points[sv, ],
                              max.p.na=max.p.na, weights=weights, 
                              no.match=no.match, use.old=use.old, na.value = na.value, ...)  
        sampled.values[sv] <- res$sample
        sg <- res$grid
        rm(res)
      } else {
        sg <- gaussiansmooth(g, sd=sd,max.r=max.r, kernel.dim=kernel.dim, 
                             max.p.na=max.p.na, weights=weights, 
                             no.match=no.match, use.old = use.old, na.value = na.value, ...)  
      }
      if(!no.out) writetile(sg, outgrid)
    }
  } 
  if(method ==3){
    
    # Method 3 - Upscale each tile individually, smooth the entire thing, downscale and write tiles individually
    
    # Make a blank grid to store the upscaled landscape
    # It will have an extra, one cell wide band around the edge  (so that when we dowscale we can interpolate to the original extent)
    cellsize <- xinfo$cellsize*factor
    nr <- ceiling(xinfo$nrow/factor) + 2
    nc <- ceiling(xinfo$ncol/factor) + 2 
    oyul <- xinfo$yll + xinfo$cellsize * xinfo$nrow # orignal y of upper left corner
    yul <- oyul + cellsize
    yll <- yul - nr * cellsize  
    #  yll<- xinfo$yll - (nr*factor - xinfo$nrow) - cellsize # the bottom corner of the upscaled grid will be bumped down to the bottom of the upscaled pixel
    xll <- xinfo$xll - cellsize
    ug <- list(m=matrix(NA,nrow=nr, ncol=nc), nrow=nr, ncol=nc, xll=xll, yll=yll, cellsize=cellsize) # preallocate whole upscaled grid
    class(ug) <- c("grid", class(ug))
    
    for(i in tiles){
      settile(i)
      if(gridioverbosity() > 0)
        cat("Reading tile", i, "of", ntiles(), "\n")
      g <- readtile(x, cacheok=cacheok)
      if(!missing(weights)){ 
        if(all(c("value", "weight") %in% colnames(weights))){
          g <- swap(g, weights$value, weights$weight, no.match=no.match)
        } else if(ncol(weights) == 2) {
          g <- swap(g, weights[1,], weights[,2], no.match=no.match)
        } else {
          stop("If weights are supplied and the dataframe has more than two ", 
               "columns it must have columns labled \"value\" and \"weight\".")
        }
      }
      g <- upscale(g, factor=factor, max.p.na=max.p.na, use.old=use.old)
      startcol <- round( 2 + (tilescheme()$j-1)*tilescheme()$colsize/factor )
      endcol <- round (startcol + g$ncol-1 )
      startrow <- round (2 +  (tilescheme()$i-1)*tilescheme()$rowsize/factor )
      endrow <- round (startrow + g$nrow - 1)
      ug$m[startrow:endrow, startcol:endcol]  <- g$m
    }
    if(gridioverbosity() > 0)
      cat("Smoothing entire upscaled grid\n")
    
    ug <- gaussiansmooth(ug, sd=sd, max.r=max.r, kernel.dim=kernel.dim, 
                         max.p.na=max.p.na, use.old = use.old, ...) 
    
    
    for(i in tiles){
      settile(i)
      startcol <- round( 1 + (tilescheme()$j-1)*tilescheme()$colsize/factor )
      endcol <- round( startcol + ceiling(tiledetails()$ncol/factor) +1  )
      startrow <- round( 1 +  (tilescheme()$i-1)*tilescheme()$rowsize/factor )
      endrow <- round( startrow + ceiling(tiledetails()$nrow/factor) + 1)
      
      cellsize<- ug$cellsize
      ncol= round(endcol - startcol + 1)
      nrow= round(endrow-startrow+1)
      xll = ug$xll + (startcol-1)*cellsize
      yll = ug$yll + (ug$nrow - endrow) * cellsize
      sg <- list(m=ug$m[startrow:endrow, startcol:endcol], nrow=nrow, ncol=ncol, xll=xll, yll=yll, 
                 cellsize=cellsize)
      class(sg) <- c("grid", class(sg))
      
      if(gridioverbosity() > 0)
        cat("Downscaling and writing tile", i, "\n")
      sg <- downscale(sg, match.extent=TRUE, factor=factor)
      sg <- matchextent(sg, tiledetails())
      
      if(sample){
        tc <- tiledetails()  # tile core
        xmin <- tc$xll
        ymin <- tc$yll
        xmax <- tc$xll + tc$ncol*tc$cellsize
        ymax <- tc$yll + tc$nrow * tc$cellsize
        sv <- sample.points$x > xmin & sample.points$x <= xmax  & sample.points$y > ymin & 
          sample.points$y <= ymax # selection vector for points in range
        
        rowcol <- matrix(NA, nrow=sum(sv), ncol=2) # row and column index of each sample point
        rowcol[,1] <- y2r(sample.points$y[sv], sg)
        rowcol[,2] <- x2c(sample.points$x[sv], sg)
        sampled.values[sv] <- sg$m[rowcol]
      }
      
      if(!no.out) writetile(sg, outgrid)    
    }
  } # End method 3
  
  if(sample) return(sampled.values)
}
