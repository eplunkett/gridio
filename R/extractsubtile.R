extractsubtile <- function(x, coreonly=FALSE, safe=FALSE){
  #------------------------------------------------------------------------------------------------#
  #  extractsubtile      Ethan Plunkett                                          March 8 2012
  #
  #  function to extract the current subtile from a grid object in memory
  #
  #  arguments:
  #    x : a grid that coincides with the current tile (including the buffer).  Typically
  #      produced by readtile.
  #    coreonly : controls buffer inclusion in the subtile. If TRUE  only the core of the subtile
  #      is extracted otherwise both the core and the buffer is extracted.
  #    safe : if TRUE check to make sure x coincides with the current tile in the tilescheme.
  #   value :
  #      a grid matching the extent of the subtile and, if coreonly is FALSE, its buffer.
  #
  #   Note: bad things will happen if x doesn't coincide with the current tile in the tilescheme
  #    (with its buffer). Setting safe to TRUE will check this assumption with a little extra 
  #     overhead.
  #------------------------------------------------------------------------------------------------#
  if(safe) if(!coincide(x, tiledetails(coreonly=FALSE)))
    stop("x doesn't coincide with the current tile.")
  ri <- stri(coreonly=coreonly)
  ci <- stci(coreonly=coreonly)
  result <- list(m=x$m[ri,ci],
                 nrow=length(ri),
                 ncol=length(ci),
                 xll=x$xll + (ci[1]-1)*x$cellsize,
                 yll=r2y(ri[length(ri)], x) - .5 * x$cellsize,
                cellsize=x$cellsize  )
  class(result) <- c("grid", class(result))
  return(result)
} 
