stinit <- function(){
  #------------------------------------------------------------------------------------------------#
  # stinit    Ethan Plunkett     March 6, 2012   
  # 
  #   function to initialize an individual subtile once the overall subtile scheme is initialized
  #
  #   this is a internal function (not meant for end users).
  #   it is called by settile (if there is a subtile scheme) and subtileinit
  #
  #------------------------------------------------------------------------------------------------#
  if(is.na(.status$subtilescheme$nc)) stop("subtilescheme not initialized. Call subtileinit")
  .status$subtilescheme$i <- 1       #  the row index of the current subtile 
  .status$subtilescheme$j <- 1       #  the column index of the current subtile
  
  td <- tiledetails()
  rows <- td$nrow/.status$subtilescheme$rowsize # number of rows of subtiles in the current tile
  cols <- td$ncol/.status$subtilescheme$colsize
  if(!isTRUE(all.equal(rows%%1, 0)) | !isTRUE(all.equal(cols %% 1, 0)))
    stop("The tilesize doesn't seem to be a multiple of the subtile dimension.  Something is wrong.")
  .status$subtilescheme$rows <- rows
  .status$subtilescheme$cols <- cols      #  the number of columns of subtiles in the current tile (often the same as nc)
  .status$subtilescheme$index=1     #  the index of the current subtile (an integer between 1 and n) 
  .status$subtilescheme$n=rows * cols       #  the number of subtiles in the current tile
  
}
