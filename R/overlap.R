overlap <- function(x, y){
  # Function to determine if two objects containing extent information overlap.
  #  The objects must be lists with xll, yll, nrow, ncol, and cellsize) 
  #  all grids qualify.
  # The two objects must be in the same projection but don't necessarily have to snap
  # for this function to work.
  
  # Find the x and y range of the two objects these are based on cell edges
  x.xrange <- c2x(c(1, x$ncol) - 0.5 * x$cellsize, x + 0.5 * x$cellsize) 
  y.xrange <- c2x(c(1, y$ncol) - 0.5 * y$cellsize, y + 0.5 * y$cellsize) 
  x.yrange <- r2y(c(x$nrow, 1) - 0.5 * x$cellsize, x + 0.5 * x$cellsize)
  y.yrange <- r2y(c(y$nrow, 1) - 0.5 * y$cellsize, y + 0.5 * y$cellsize)
  
  # Determine if they overlap
  rangeoverlap <- function(a, b) ( max(a[1],b[1]) < min(a[2], b[2])) # <= needed if cell centers but not here
  res <- rangeoverlap(x.xrange, y.xrange) &&  rangeoverlap(x.yrange, y.yrange)
  return(res)
}
