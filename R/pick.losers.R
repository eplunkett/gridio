# function to decide which grids to delete
.pick.losers <- function(d, sn){
  # d = assembled index of directory
  # sn = the additional space needed in MB
  a <- d$reads
  a <- gsub( "^.*/", "", a)
  d$last.read <- as.POSIXct(a, format=.status$timeformat)
  
  d <- d[d$status != "copying", ,drop = FALSE]  # can't delete copying grids!
  d <- d[difftime(Sys.time(), d$last.read, units = "hours") > 1 & !is.na(d$last.read), , drop = FALSE] 
  d <- d[order(d$last.read, decreasing=FALSE),  , drop = FALSE]  # sort with oldest first
  
  cs <- cumsum(d$size)
  sv <- cs > sn
  success <- any(sv)  # indicates we can't make space
  
  if(success){
    last.row <- min(which(sv))   # identify how many we have to delete to meet  
  } else {
    last.row <- nrow(d)
  }
  
  
  
  return(list(losers = d$source[1:last.row], cut = cs[last.row], success = success))
} # end pick losers function
