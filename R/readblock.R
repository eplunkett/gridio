if(FALSE){
  
  # Setup cluster and servers
  library(anthill)
  #system("x:/Anthill/package/Locklib/lock_server.exe 3340" , wait=FALSE, intern=FALSE, invisible=FALSE, show.output.on.console=FALSE)
  config(path="x:/anthill/control/")
  # launch.gridservers()
  
  # Load package and update all the changed functions
  source("x:/gridiolib/gridio.test/update.package.R")
  
  cleanup()
  reference <- "c:/temp/gridiotest/makemosaic/demmosaic/"
  
  d <- mosaicinfo(reference, FALSE)
  gs <- readgridservers()
  mosaicinit(servers=gs, reference=reference)
  .status$connections
  .status$activeconnection
  reference2 <- "c:/temp/gridiotest/makemosaic/dem"
  gridinit(add=TRUE)
  setwindow(reference2)
  
  # setup for line by line execution of function
  path <- reference
  startrow=30
  startcol=20
  nrow=100
  ncol=100
  cacheok=TRUE
  as.matrix=FALSE
  na.value=-9999
  cacheok=FALSE
  as.integer=FALSE
  buffer=0

  
  b <- readblock(reference, startrow=startrow, startcol=startcol, nrow=nrow, ncol=ncol, cacheok=TRUE)
  b2<- readblock(reference2, startrow=startrow, startcol=startcol, nrow=nrow, ncol=ncol, cacheok=TRUE)
  
  
  tileinit(100, 0)
  result <- b2

  result$m[,] <- NA
  plotextent()
  for(i in 1:ntiles()){
    settile(i)
    tile <- readtile(path=path)
    result <- overlay(tile, result)
  }
  
  
  plot(b)
  plot(b2)
  
  all.equal(b, b2)
  
  if(!isTRUE(all.equal(b, b2))) all.equal(b, b2)
  

}


readblock <- function(path, startrow, startcol, nrow, ncol, buffer=0, extent, as.matrix=FALSE, 
                      na.value=-9999, as.integer, cacheok=FALSE, desc){ 
  # Note extent is an alternative way to specify the block -  it uses xll, yll, nrow, ncol, and 
  #     cellsize.  It is converted to startrow, startcol, nrow, and ncol in .readblock.
  #  Note 2 as.integer was there so we could skip a call to griddescribe but with caching and 
  #    mosaicing we always do a call to griddescribe so it should probably be dropped.
  # Note 3 desc is an argument so that when read grid is called griddescribe can be called just once
  #   end users aren't expected to include this argument
  path <- gsub("\\\\", "/", path) # standardize on "/" as delimiter
  path <- gsub("/$", "", path)  # drop trailing "/"
  if(missing(desc)){
    desc <- griddescribe(path)  
  }

  if(desc$type == "missing")stop("There is no grid at the path '", path, "'.")  
  if(!is.character(path)) stop("path must be a character indicating the path to a grid you would like to read.")
  if(length(path) != 1) stop("path must point to one and only one grid.")
  
  # If it's not a mosaic 
  if(!desc$mosaic){
    con <- findconnection(x=desc, path=path)
    if(is.na(con))
      stop("There's no connection for the grid you want to read from. See gridinit and setwindow")
    setconnection(con)
    return(readblockc(path=path, startrow=startrow, startcol=startcol, nrow=nrow, ncol=ncol, buffer=buffer,
                      extent=extent, as.matrix=as.matrix, na.value=na.value, as.integer=as.integer,
                      cacheok=cacheok, desc=desc))
  }
  
  # If it is a mosaic
  if(is.na(.status$mosaic$rowsize))
    stop("You must call mosaicinit before trying to read from a mosaic.")
  if(!coincide(desc, .status$mosaicwindow)) 
    stop("Mosaic grid doesn't conform to mosaic window.")
  md <- desc$mosaicinfo
  
  if(!grepl("tif$", x = md$type))
    stop("Mosaic is not tif based")
  
  if(!all(isTRUE(all.equal(.status$mosaic$rowsize, md$rowsize)), 
          isTRUE(all.equal(.status$mosaic$colsize, md$colsize)) ))
    stop("Mosaic grid's rowsize and tilesize don't match the scheme defined by mosaicinit")
  
  # Define the extent of the result (without the buffer)
  if(!missing(extent)){
    res <- list(
      xll=extent$xll,
      yll=extent$yll,
      nrow=extent$nrow,
      ncol=extent$ncol, 
      cellsize = extent$cellsize)    
  } else {
    res <- list(
      xll = desc$xll + desc$cellsize * ( startcol - 1),
      yll = desc$yll + desc$cellsize * (desc$nrow - (startrow - 1) - nrow) ,
      nrow=nrow,
      ncol=ncol,
      cellsize=desc$cellsize)
  }
  
  # if the buffer isn't zero expand the extent of the result to include the buffer
  if(buffer!= 0){ 
    res$xll <- res$xll - res$cellsize * buffer
    res$yll <- res$yll - res$cellsize * buffer
    res$nrow <- res$nrow + buffer * 2
    res$ncol <- res$ncol + buffer * 2
  }
  
  # turn the result extent into a grid
  res <- c(list(m=matrix(NA, nrow=res$nrow, ncol=res$ncol)), res)
  class(res) <- c("grid", class(res))
    
  # Loop through panes reading in the overlapping portion
  for(pane in which(md$panemap)){  # Note use the panemap from the file in case not all panes have data
    # pane <- 5
    x <- res    # So I can recycle code
    y <- panedetails(pane)
    
    # Calculate the range of each object.  Note these are cell centers.
    x.xrange <- c2x(c(1, x$ncol), x)  
    y.xrange <- c2x(c(1, y$ncol), y)
    x.yrange <- r2y(c(x$nrow, 1), x)
    y.yrange <- r2y(c(y$nrow, 1), y)
    
    # If they don't overlap move on to the next pane
    rangeoverlap <- function(a, b) ( max(a[1],b[1]) <= min(a[2], b[2])) 
    if(!rangeoverlap(x.xrange, y.xrange) || !rangeoverlap(x.yrange, y.yrange)) next
    
    # calculate the range of the overlap  (still cell centers of overlapping cells)
    xrange <- c(max(x.xrange[1], y.xrange[1]), min(x.xrange[2], y.xrange[2]))
    yrange <- c(max(x.yrange[1], y.yrange[1]), min(x.yrange[2], y.yrange[2]))
    
    # Calculate the extent for the overlap zone
    overlap.extent <- list(
      xll = xrange[1] - x$cellsize * 0.5,
      yll = yrange[1] - x$cellsize * 0.5,
      nrow = round((yrange[2]-yrange[1]) / x$cellsize  + 1),
      ncol = round((xrange[2]-xrange[1]) / x$cellsize  + 1),
      cellsize = x$cellsize)
    
    # Set the connection
    setconnection(findconnection(path=path, pane=pane))
    ppath <- panepath(path, name=md$name, id=pane, npanes=length(md$panemap) )
    panedesc <- c(y[c("xll", "yll", "nrow", "ncol", "cellsize")],
                  list(type=desc$type, min=NA, max=NA, mean=NA, sd=NA, mosaic=FALSE))
    
    # Read the block
    b <- readblockc(ppath, buffer=0, extent=overlap.extent, as.matrix=as.matrix, 
                    na.value=na.value, as.integer=as.integer, cacheok=cacheok, desc=panedesc)
    
    firstrow <- y2r(yrange[2], res)
    lastrow <- y2r(yrange[1], res)
    firstcol <- x2c(xrange[1], res)
    lastcol <- x2c(xrange[2], res)
    
    res$m[firstrow:lastrow, firstcol:lastcol] <- b$m
  }
  return(res)
}
