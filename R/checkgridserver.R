if(FALSE){
  server <- "ethanxp"
  port <- "3333"
  checkgridserver(server, port)
}
checkgridserver <- function(server, port){
  cleanup()
  io.mode <- 1 # server
  log <- 0
  if(any(is.null(server), is.null(port)))
    stop("NULL arguments aren't allowed")
  if(any(is.na(server), is.null(port)))
    stop("NA arguments aren't allowed")
  st <- system.time({
    r <- .C("R_gridinit",
            integer(1),
            as.integer(io.mode),
            as.character(server),
            as.integer(port),
            as.integer(log),
            PACKAGE = .status$dllname)
  })
  .status$gridwait <- .status$gridwait + as.numeric(st[3])
  return(! r[[1]] %in% .status$gsdowncode) # FALSE is gridserver is down TRUE elsewise
  cleanup()  # for good measure
}
