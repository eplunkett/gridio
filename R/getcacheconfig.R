getcacheconfig <- function(reread=FALSE){
  # Note this is slightly different than Brad's version. I'm returning a named list of
  # on, cachepath, maxsize, and minfree
  # From either the global variable (if it's been set) or the disk if it hasn't 
  
  # Note: I store the path without the trailing "/"
  
  cp <- .status$cacheconfigpath  # config path  
  if(!reread &&!is.na(.status$cache$on)){  # if we've read the cache config file before
    return(.status$cache)
  } else {  # if we've never read the config file or if argument forces rereading the config file
    values <- list(cachepath=NA, maxsize=NA, minfree=NA, nbins = NA, deletesize = NA, logdetails = NA)
    if(!file.exists(cp)) {
      values <- c( list(on=FALSE), values)
    } else{  # file exists
      l <- readLines(cp)
      l <- gsub("[[:blank:]]*($|;.*$)", "", l) # strip comments
      l <- gsub("\\\\", "/", l)
      for(i in names(values)){
        sv <- grepl(paste(i, "[[:blank:]]*=[[:blank:]]*", sep=""), l)
        if(sum(sv) != 1) stop(cp, " is missing an entree for ", i)
        values[[i]] <- gsub(paste(i, "[[:blank:]]*=[[:blank:]]*([^[:blank:]])",   sep=""), "\\1",l[sv])
      }
      values <- c( list(on=TRUE), values)
      
      for(i in 1:length(values)) 
        values[[i]] <- gsub("(^['\"])|(['\"]$)", "", values[[i]]) # drop leading and trailing quotes
    }  # end file exists
    values$cachepath <- gsub("/$", "", values$cachepath) # drop trailing "/"
    
    
    values$maxsize <- as.numeric(values$maxsize)
    values$minfree <- as.numeric(values$minfree)
    values$on <- as.logical(values$on)
    values$nbins <- as.integer(values$nbins)
    values$deletesize <- as.numeric(values$deletesize)
    values$logdetails <- as.logical(as.numeric(values$logdetails))
    if(is.na(values$logdetails)) values$logdetails <- FALSE
    # save the result
    .status$cache <- values
    
    return(values)
  } # end read config file
} # end function definition
