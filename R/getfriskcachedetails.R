

getfriskcachedetails <- function(system = "all", computers = "all",
                                 consolidate.purged = TRUE, max.days = NA, path ){
  
  system <- tolower(system)
  stopifnot(system %in% c("all", "r", "apl"))
  # Function to analyze detailed frisk c
  
  if(!anthillrunning())
    stop("Caching requires anthill\n")
  
  path <- anthill:::.settings$path
  
  
  l <- readLines(paste0(path, "/friskcachedetails.log"))
  
  #ronly <- FALSE
  #computers <- c("all")
  #consolidate.purged <- TRUE
  
  s <- l
  s <- read.table(textConnection(s), sep ="\t", header = TRUE, stringsAsFactors = FALSE)
  
  if(system != "all")(
    s <- s[tolower(s$system) == system, , drop = FALSE ]
  )
  
  if(tolower(computers) != "all")
    s <- s[tolower(s$computer) %in% tolower(computers),  , drop = FALSE]
  
  if(consolidate.purged){ 
    # drops all but first purged grid in each batch
    sv <- s$message == "purged" &  duplicated(s[, c("date", "computer")])
    s <- s[!sv, ]
    sv <- s$message == "purged"
    s$sourcepath[sv] <- "(multiple)"
    s$cachepath[sv] <- "(multiple)"
  }
  
  if(!is.na(max.days) ){
    dates <- as.POSIXct(s$date, format = anthill:::tm.time.format())
    now <- Sys.time()
    elapsed <- as.numeric(difftime(now, dates, units = "days"))
    sv <- elapsed <= max.days
    s <- s[sv, , drop = FALSE]
  }
  
  
  return(s)
  
  }
