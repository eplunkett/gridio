
rastermoddate <- function(x){
  # Function to determine the modification date of a raster file 
  # designed to work with ESRI grids and geoTiffs likely will fail with other formats
  # For tif returns the modificationdate of x for esri grids (really anything that doesn't end in tif or tiff) 
  # it will assume x is a folder and return the modification date of 
  #  the sta.adf file within it. 
  # In both cases it calls fileinfo and returns the date in Posixct format
  # Will throw an error if x doesn't exist or isn't either a grid or tif
  is.tif <- grepl("\\.tif$|\\.tiff)$", x = x, ignore.case = TRUE)
  
  if(!is.tif){ # assuming esri grid  (dir)
    x <- paste0(gsub("/$|\\\\$", "", x ), "/sta.adf")
  }
  return(fileinfo(x)$date)
}