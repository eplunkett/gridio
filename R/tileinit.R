tileinit <-
function(rowsize,  buffer=0, colsize=rowsize){
#--------------------------------------------------------------------------------------------------#
# tileinit                  Ethan PLunkett                                         January 2011
#
# This function sets up the tile structure.  It relies on the global variable header and 
# modifies the global variable tile.  gridinit and either setwindow or makewindow must be
# called prior to tileinit.  It also sets the tile indices(i, h, tileno) to the first tile.
#
#  Changes - March 9, 2012 - now clears subtile scheme
#    Nov 22, 2021 - buffer now defualts to 0
#--------------------------------------------------------------------------------------------------#
  w <- getwindow()
  
  if(!w$set) stop("The reference window must be set prior to calling this function.")
  
  
  .status$tilescheme$rowsize <- rowsize
  .status$tilescheme$colsize <- colsize
  .status$tilescheme$buffer <- buffer
  .status$tilescheme$i <- 1 
  .status$tilescheme$j <- 1
  .status$tilescheme$rows <- ceiling(w$nrow / rowsize)
  .status$tilescheme$cols <- ceiling(w$ncol / colsize)
  .status$tilescheme$lastrows <- w$nrow %% rowsize
  if(.status$tilescheme$lastrows == 0) .status$tilescheme$lastrows <- rowsize
  .status$tilescheme$lastcols <- w$ncol %% colsize
  if(.status$tilescheme$lastcols == 0) .status$tilescheme$lastcols <- colsize
  .status$tilescheme$tileno <- 1
  .status$tilescheme$totaltiles <- .status$tilescheme$rows*.status$tilescheme$cols
  # Clear subtile scheme
  for(i in names(.status$subtilescheme))  .status$subtilescheme[[i]] <- NA
}

