  
# March 26, 2024 (2.138)
  * Replace sp with sf in remaining functions
  * Replace rgdal with sf
  
# 64-bit dev branch (2.137)
 
  * Add `.status`  items `$dllname`  for gridio dll and `$capslib` for
  capslib dll names. On load sets these to either "caps_lib" and "gridio_lib" or
  "caps_lib64" and "gridio_lib64" depending on the architecture.
  * "caps_lib" and "gridio_lib" are loaded from default locations
  "C:/gridio2/<libname>" where lib name is assinned `dllname` and `capslib`.
  * All .C calls now use `.status$dllname` or `.status$capslib` for the dll
  name.
  * a new `.status$echocalls` can be TRUE or FALSE. Current (development) 
  default is TRUE.  Many functions which use `.C()` to interface with the dll's
  now will print R code that reproduces the call independent of gridio 
  functions. This facilitates debugging the dlls and creating test scripts for
  Edi.
  * `onload()` in zzz.R now sets environmental variables for gdal.  The 
  default gdal path is C:/gdal64 and C:/gdal.
  * Add support for conversion to and from **terra**  `SpatRaster`.
  
### Breaking changes
  
  * `guassianinterpolate()` now takes a standard data frame to define the points
  it also gains two new arguments `xcol` and `ycol` to set which columns contain
  coordinate information. 
  * Dropped support for conversion to and from **sp** and **raster** package 
  objects.
  * `samplegrid` drops `loc` argument which specified points using **sp** and 
  replaces it with `pts` which defines points with a data frame.  It also gains
  `xcol` and `ycol` arguments to specify which columns have the coordinates.
  It loses `checkproj` and `proj` arguments. 
  
  
### Misc
  * fix likely bug in `gridkill()` and hopefull not introduce a new one. Two lines
  in gridkill looked like they were meant to call `setcache(TRUE)` and 
  `setcache(FALSE)` but instead were `setcache=TRUE` or `FALSE` which would have
  just created a local variable.  I changed them to what I think was the 
  original intent.  The first I think was designed to make sure the target
  grid was deleted via the gridserver when caching was on.  I'm not sure the
  point of the second.
  

# Nov 27, 2023 (2.136)
  * Converted existing tests to **testthat** and updated tests for TIFFs.
  * Updated .gitignore
  * added .gitattributes

# Oct 19, 2023 (2.135)
  * Converted readme to .md
  * Moved changelog from readme to NEWS.md

# May 26, 2023 (2.134)
  * Fixed bug from tiff transition that prevented refgridinit() working with
  a non-mosaic reference.

# Dec 13, 2022 (2.133)
  * pgridpredict() and pgridfun() work again (broke with transition to TIFFs) and
  are streamlined and should be a little faster.  All the gridserver arguments
  are now deprecated in these functions.

# Nov 18, 2022 (2.132) 
 * .extract.tag( , numeric = TRUE) now maps several odd values onto odd R numerics.
   "1.#IO" to -Inf; "-1.#IO" to Inf, "1.#QO" to NA; "nan" to NA; "-inf" to -Inf;
   "inf" to Inf. This allows rasterinfo to parse them in the same manner.
   
 * scanrasters() now keeps track of tifs for which rasterinfo() fails and 
   returns them as a separate item. Previously errors were thrown and the scan 
   was interrupted without returning any results.
  
# Nov 17, 2022 (2.130) 
 * batchsitch() now ignores case when processing suffixes on 
  mosaics (_T and _TM are treated as mosaic suffixes, same as _m and _tm).
  rasterinfo() now accepts 'nan' as a valid band no data value when parsing 
  output and band no data information can be missing entirely.  
  
 * rasterinfo() now reports NA as the band no data value for both 'nan' and
  absent No Data values (as reported by gdalinfo), previously it threw 
  confusing errors in both cases.

# Nov 9, 2022 (2.129) 
 * Bug fix in scanrasters()

# October 31, 2022 (2.125 - 2.127) 
 * convertmosaic now will delete the source mosaic
  after successful verification if deletesource = TRUE regardless of whether 
  there is a complete (stitched) version of the mosaic.  

# October 19, 2022 (2.123, 2.124) 
 * stitch now creates DEFLATE compressed geoTIFFs.
  Compress and convert mosaic now use DEFLATE compression.  New function scanrasters()
  will look for ESRI grid and grid based mosaics as well as uncompressed tiff mosaics
  in a drive or directory.

# October 18, 2022 (2.120 - 2.122)
 * Add functions for compressing mosaics and geoTIFFs 
  produced by gridio. compress(), launchcompression(), compresstiff() 
  compressmosaic(), makemosaic now adds compression = 'compressed' to the mosaic
  description file.

# October 8, 2022 (2.119)
 * mosaicdescrip.txt now allows compression = 'no' (Default)
  and compression = 'compressed' for converted and explicityl compressed mosaics.

# September 7, 2022 (2.118) 
 * Added implicit priority arguments to launch functions:
  launchconversion(), launchbatchmosaic(), launchbatchstitch() with default of 6. 
  Previously this could be set via ... with the default set by 
  anthill::simple.launch to 5.  The change makes these projects take precedence
  over standard projects.
  Also, copied changes to convertmosaic made in gridio (1.x) so that source
  mosaic can be deleted after veryfing conversion IF there's also a complete
  (stitched or original) version of the raster and so that the stitch log is 
  copied when converting.


# August 8, 2022 (2.117)
 * Added check to readblock to make sure a mosaic is not 
  grid based. Previously, attempting to read from a grid based mosaic failed
  with an uninformive error. Also checktif and checkraster now return error 
  "file missing" rather than "component file missing" if the .tif file is 
  missing. 
  
 * Fixed bug that prevented restoreconnections from working on a local connection
  - setwindow.character now properly sets the refgrid in the connections 
  list.  Previously it didn't and so the local connection was set but could
  not be restored.

# July 29, 2022 (2.116)
 * convertmosaic now has a new argument that controls whether the source (Grid based)
  mosaic will be deleted after creating a tiff based mosaic. Deletion will only
  occur if the parameter is TRUE and there is a valid source grid for the mosaic
  (which won't be deleted).

# July 9. 2022 (2.115) 
 * mosaicinfo now checks to see if readlines returned an empty character string
  in which case it will attempt to reread.  Delays between rereads increased 
  slightly. 

# June 22, 2022 (2.114)
 * pgridpredict no longer checks for missing owner - as it's now OK to have a
  a missing owner if the global owner is set with set.owner. 

# June 10, 2022 (2.113)
 * Mosaic info now attempts to read the mosaic description file 3 times before
  throwing an error with a slight delay after each read. 

# June 10, 2022 (2.112)
 * Updated parallel versions of functions: pgridpredict, pgridtable, pgridstats, 
  pgridfun so that they all load gridio2 in the function that is called by 
  anthill. Previously some loaded gridio and some didn't load either version.

# June 10, 2022 (2.111)
 * Updated makemosaic previously if the referencemosaic was provided it assumed
  that all panes that were present in the reference had data in the raster 
  file that was geing mosaiced.  This caused it to fail with a SLR grid that
  was NA everywhere but near the coast. Now it drops tiles that have no data.

# May 18, 2022 (2.108 - 2.110)
 * Bumped version up by 100 so that version number is consistent with how I 
  imagine it - R treats the number after the period as an integer so drops
  the initial zeros, 2.007 as the same as 2.7.

 * Added support for relative paths in mosaicdesc.txt files for the source grid
  if and ONLY if the mosaic and its source share a directory.  This will allow
  the two to be moved without breaking the connection between them.

 * Added functions getmosaicpath and getsttichedpath that attempt to find the 
  path to one type of file from the path to the other based on the naming 
  conventions we've been using. These are now used by all the mosaicing and 
  stitching functions. 

 * Added function fixmosaicdesc that will update the mosaic descrption files
  source, sourcetimestamp, and/or type to reconnect a mosaic to it's source, 
  assert that the mosaic is up-to-date with respect to that source, or turn of
  frisking for the mosaic entirely.  This includes the addition of two new
  mosaic types "reset_tif" and "reset".

# May 12, 2022 (2.007)
 * I've improved error messages in rasterinfo, stitch, and mosaic (per Brad's
  request). 

 * It should now be clear when the reference is missing, or it doesn't coincide
  with the raster to be stitched or mosaiced. rasterinfo also now sets the 
  type to missing if the file being described is missing or unreadable.

# May 9, 2022  (2.006)
 * Bug fix in launchbatchstitch and launchbatchmosaic to catch up with old 
  change in anthill's config file.  Now just one reference which lacks an 
  extension and is converted on the fly to the grid, geoTIFF, or mosaic versions.

# April 25, 2022 (2.005)
 * friskstitched() updated to work with tifs. It will also
  attempt to play well with grids and gridbased mosaics mixed in with tifs. 

# April 6, 2022 (2.004)
 * More debugging of conversion functions. 
  convertgrid now defensively creates the tif as a temporary file and then 
  renames it to final location, because gdal_translate did not want to make a 
  tif adjacent to a grid with the same name - e.g. DEM.tif couldn't be created 
  next to a grid called DEM.
  
 * convertgrid has a new argument overviews which controls whether oveviews are 
  created for the output file. It defaults to TRUE, but convertmosaic sets it 
  to FALSE, so that mosaics created with convertmosaic won't have overviews 
  (for each pane) but single geoTIFF files will.
  
 * convertgrid now always creates statistics and histograms for the output file.

# March 29, 2022 (2.001 - 2.003)
 * Added ability to pass additional arguments to launchconversion and to the
  functions in the mosaicstitchworkspace.

 * Cleaned up documentation

 * Added launchconversion and other updates to allow launching grid conversion 
  to geoTIFF via anthill
  
 * Updated references to gridio with gridio2, and updated gridio:: to gridio2:: 
  or deleted explicit package reference

 * Fixed bugs in conversion functions

# March 17, 2022 (2.000)
 * Change to gridio2 which will read and write tiff files.

# March 10, 2022 (1.276)
 * Fixed bug where gridkill didn't clear the gridinfo cache 
  for it's target if the target wasn't a mosaic. Made gridinfo caching case 
  insensative based on new .status$casesensativegridinfo which defaults to 
  FALSE. Stitchpy no longer relies on cached gridinfo when verifying the extent
  of the output grid.

# Jan 12, 2022 (1.275)
 * Implemented changes in preparation to switching to geoTIFF
  files (but still working with grids) - dropped calls to makewindow() which 
  will soon be obsolete. connections and servers tables now include a refgrid 
  column that tracks a reference grid for each connection or server which is 
  now used with setwindow when activating connections. New function checkraster
  replaces checkgrid and calls either checkgrid or checktif.  Components of tif
  mosaics are looked for differently than grid mosaics and so minor changes 
  made there. mosaicinit is somewhat simplified.  

# Dec 17, 2021 (1.274)
 * fixed bug in verifyconversion

# Dec 10, 2021  (1.269 - 1.273)
 * stitching and mosaicing now use the grid/tiff status of the target file to 
  decide whether to create a grid or tiff  - for example tiff based mosaics 
  are stitched to tiffs.

 * global variable reference replaces referencegrid and referencemosaic. It 
  represents a reference grid path (without  an extension) extensions ".tif", 
  "_tm", and "_m" are added on the fly for tifs, tif based mosaics, and grid 
  based mosaics.

 * stitching and mosaicing tiffs now uses calls gdalinfo(x,  stats = TRUE, 
  hist = TRUE) on output tiff files so that all have stats and histograms.
   
 * Updating how apl calls stitching and mosaicing so there is more flexibility.  
  
 * new function importantconfig()  (read as import ant config) imports anthill 
  configuration elements that are relevant to gridio. It uses code that used to
  be called on.load which now calls  this function.  Additioanlly, 
  anthill::config now calls gridio::importantconfig()  if gridio is loaded. The 
  end result is that it doesn't matter the load order of anthill and gridio or 
  if parameters change and config() is called again.

# Dec 3, 2021 (1.268)
 * Stitching tiff mosaics no longer relies on griddescribe
  but instead uses rasterinfo. rasterinfo now returns information on the 
  origin and an extent object in gridio format that is derived from the 
  origin. 

# Dec 3, 2021 (1.267)
 * Addressed a ton of issues flagged by rcmd check. Mostly updating documentation
  but also fixed bug introduced in 1.264 that broke subtile processing.
  Dropped old functions gridstitch and gridsplit. They predate stitch
  and makemosaic and I haven't used them in years. They also require 
  makewindow which will soon be depricated.  
  Remaining warnings mostly have to do with the inclusion of compiled
  dlls in the package.  

# Nov  23, 2021  (1.265, 1.266)
 * Moved helper function .extract.tag from  within rasterinfo to standalone
  but non exported function.  
  
 * New function arcdescribe calls arcpy's describe band and returns the pixel 
  type, no data value, and a logical integer (distinct from  pixel type I  think
  this is really analogous to 'catagorical'. 
  It's super slow but yields different results than gdal_info  for  no data  value
  on a few grids that are failing to copy cleanly because of confusion about the 
  No data value.   

# Nov 22, 2021 (1.264)
 * New functions: convertmosaic, and
 gridtotif added to convert grids and grid based mosaics to
 tifs and tif based mosaics.  While verifying conversion and getting
 around some of the limitations of using gdal_translate with grids.  


# Nov 16, 2021 (1.263) 
 * In preparation for switching to geoTIFFs stitching and mosaicing have
  been updated to work on geoTiffs as well as ESRI grids. Stitching and 
  mosaicing commands now have a tiff argument that should be TRUE or FALSE.
  If omitted it defaults to the global value set in .status$tiff but 
  also set on load to values from  anthill's configuration file if that 
  package is loaded and configured prior to gridio.  stitch and makemosaic 
  are  now the recommened functions for stitching and  mosaicing. Both
  call the python versions (stitchpy  and makemosaicpy) if tiff resolves
  to FALSE.  

# Oct 18, 2021 (1.262)
 * fileinfo now incudes a path to the missing file in the error message 
  when called on a missing file. 

# May 18, 2021 (1.261)
 * Declared S3 print and plot methods in namespace. 
  This is now required for them to be found
  Removing RCPP functions as they have been moved to a separate package 
  (gridkernel) and adding dependency on that package.  Long term I'd like to 
  keep gridio focused on the input and output and move the processing to several
  other packages. I'm doing this now because it seems like having RCPP C++
  code and the non-standard pre-compiled binary in the same package doesn't 
  work.
  
  * Dropped functions (now in gridkernel) are:
  calckernel, 
  calckernel.grid, 
  calckernel.matrix, 
  downscale, 
  gaussiansmooth, 
  gaussiansmooth.grid, 
  kernelsmooth, 
  kernelsmooth.grid,
  kernelsmooth.matrix, 
  kernsource, 
  makegaussiankernel, 
  makeparetokernel, 
  upscale
  
 * gaussiansmooth.character and kernelsmooth.character
  are both retained in gridio as the character methods require IO and I don't 
  want to make rasterkernel depend on gridio.
  
# April 26, 2021 (1.260)
 * Fixed bug introduced in 1.258 that broke stitching for Brad.

# April 23, 2021 (1.259)
 * Added verbosity to .gridinit. Strippingo the gridserver
  postfix doesn't seem to be working on the cluster but is on the local
  session.  

# April 22, 2021 (1.258)
 * Updated defualt referencegrid, referencemosaic, and 
   host used by launchbatchstitch and launchbatchmosaic. These
   now have a default value in .status (instead of a default hardcoded in
   the function) also, if anthill is loaded than gridio pulls these from 
   anthill:::.settings so they are ultimately set in anthill/config.txt
   also, makebatchstitch workspace no longer hard codes these but instead
   leaves them undefined and thus set by anthill/config.txt as well.
   Also, fixed, rawspread documentation based on Brad's observation that
   the rawspread is octagonal not hexagonal.  

# March 23, 2021 (1.257)
 * Updated .gridinit to do hackish thing to not use 
   the gridserver postfix when connecting to a server on the same machine. 
   See d.gridinit.R for more details. 

# March 22, 2021(1.256)
 * Updated .dll to work with new cluster. Old versions
  was causing all kinds of network errors. Also Edi added additional logging.

# March 10, 2021 (1.253, 1.254, 1.255)
 * Building for R 4.0 resulted in hardcrashes when loading
   the package in RStudio. This version uses a new .dll from Eduard that 
   hopefully  fixes that. Added verbosity cat statements to C wrappers so I can 
   better track which calls to CAPS_lib are causing crashes. Set verbosity to 
   at least 5 to see them.   The new 2021 caps_lib.dll seems to be broken, but
   I'm updating to a 2019 build that Brad's been using to see if that works.

# March 9, 2020 (1.250 : 1.252)
 * updated makemosaicpy and stitchpy to use 
  .settings$pythonpath stitchpy also uses .settings$arcgisversion as the script
  had to be modified to work with ArcGIS 10.7 vs 10.0 - it selects the right 
  script based on the arcgisversion.  Also on load (in zzz.R) if  anthill is 
  loaded arcgis  
  
 * setpythonpath is run on load and sets the python path 
  based on the ArcGIS version.  
  
 * Ddded extra code to the 10.7 version of the stitching python script to do 
  some kludgy algebra with the reference grid as that was the only way to force 
  the extent to expand when there was a missing row or column of panes in the
  mosaic.  This is turned off by a boolean input to the  scripe (forcetoextent) 
  which is only invoked when needed.
  

# Oct 23, 2019 (1.248, 1.249)
 * gridstats gained a new argument fun  which is 
  applied to the data in  x prior to calculating the statistics. I'm going to 
  use it to calculate statistics on squared IEI without having to create an 
  intermediate squared IEI grid.

# Aug 21, 2019  (1.246, 1.247)
 * gridclip now uses checkcellalignment() rather than
 duplicated and less robust code that does the same thing.

# July 18, 2019 (1.245)
 * fixed bug in writeblock that caused it to not honor 
  as.integer value for non-mosaic writes.

# March 20, 2019 (1.244)
 * movingwindow now replaces infinate values with NA.  This
  allows for max and min to be used in landscapes that have blocks of NA and 
  means the max and min will be NA in those areas not -Inf and Inf.

# Jan 29, 2019 (1.243)
 * Added function polygonizetilescheme which returns a 
  SpatialPolygonsDataFrame with polygons for each tile or subtile in the tile 
  scheme.  

# June 26, 2018 (1.242)
 * Added check to settile that tile represents a single 
  value.

# June 1, 2018 (1.241)
 * Tweeked newmosaic so that it defaults to 
  as.integer = FALSE even when passed a missing argument through ... from 
  writetile.

# Mar 15, 2018 (1.240)
 * Added friskstitched function to check for stitched grids 
  that are out of date.

# Mar 13, 2018 (1.239)
 * Dropped resetting par to original values in plot.grid.
  This reset the plotting window and prevented subsequent plotting (eg points
  on top)
  
# Mar 12, 2018 (1.238)
 * Frisk cache no longer deleted unindexed files when 
  deletecopies is FALSE.  It still deletes them when deletecopies is TRUE.  In 
  readblockc we put a copying line into the index prior to copying but we don't 
  know the Id (folder) the grid will be copied into yet.  Prior to the change 
  in friskcache above this resulted in friskcache deleting grids that were in 
  the process of being copied because that ID wasn't in the index.  This now 
  means that unindexed files will only be deleted during the anthill launch 
  frisk (once a month) but they are rare now (and likely mostly copying grids 
  that we don't actually want deleted.

 * Also, re-ordered columns in friskcachelog to conform to Brad's order.  
  
# Feb 14, 2018 (1.237)
 * Added getfriskcachedetails function for reading and 
  filtering the friskcachedetails.log file.
  
# Feb 12, 2018 (1.236)
 * Updated friskcache to log details to 
  [anthill path]/friskcachedetails.log  when log details is set to 1 in 
  c:/cacheconfig.txt

# Jan 24, 2018  (1.235)
 * Updated makegaussiankenel and makeparetokernel to work 
  with vector based functions instead of looping through cells (they are now 
  much faster) which won't make a big difference
 
# Jan 18, 2018 (1.234)  I now coerce the sum of the grid into a floating point 
 * when calculating tile stats to avoid overrunning the integer maximum value.  

# Dec 3, 2017  (1.233)
 * Fixed bug in eliminatesmallpatches.  

# Nov 30, 2017 (1.232)
 * Added functions nibbleNA, eliminatesmallpatches, 
  findsmallpatches  My need was for eliminatesmallpatches which uses the other 
  two functions to eliminate patches below a minimum mapping unit size.

# Oct 23, 2017 (1.231)
 * edited gridstats function to explicitly call the dplyr
  version of summarize.  Someone loaded plyr in a project masking the 
  dplyr version and causing the summary stats to fail in an obscure way.

# Sept 5, 2017 (1.229, 1.230)
 * friskcache when called with non-zero freeup now 
  double checks how much is free to make sure that a prior call to friskcache 
  hasn't already freed up space.

# June 21, 2017 (1.228) 
 * griddescribe now updates the cached gridinfo for a grid 
  even when cache is set to FALSE. Setting cache to FALSE still causes 
  griddescribe to check the status on disk but now it will update the cached 
  info regardless. I think this makes sense and makes it more useful.

# May 10, 2017 (1.224 - 1.226) 
 * Fixed bug in refreshservers (introduced in 1.220)

# April 26, 2017 (1.222) 
 * Attempted to allow auto recovery from stuck copying of 
  grids into grid cache.  Now if a grid has been copying longer than the 
  timelimit instead of throwing an error we start copying a new copy.  

# April 7, 2017 (1.220) 
 * Edited refreshservers so that the subset of servers tied 
  to a specific drive can be targeted (with new drive argument).
  
 * Added cacheok argument to samplegrid with a default value of TRUE. Previously 
  caching wasn't supported in this function.


# March 8, 2017 (1.219) 
 * Edited merge tables to specify base::union because dplyr's
  union caused a bug.

# Feb. 1, 2017 (1.218) 
 * Edited readblockc so that it uses a copying timeout 
  "cachetimeout" set in the anthill config file.  In anthill this defaults to 
  60 minutes.  Previously it was hard coded to 30 minutes but frisking takes 
  longer than that.

# Oct. 20, 2016	(1.217)	
 * Updated friskcache log output column names and order to 
  match Brad's.  (GBpurged -> GBfreed and moved to last column). 

# Sept 7, 2016 (1.216) 
 * Added makemosaicstitchworkspace() function that creates a 
  workspace that Brad can use from APL to launch R projects to stitch and mosaic
  grids. 

 * Updated batchstitch so that it will stitch derived mosiacs if the complete 
  version is missing.
  
 * Friskcache now logs both start and end of frisk to anthill.log.  

# June 9, 2016 (1.215) 
 * Fixed bug in readblockc that prevented copying of grids 
  into the cache under some circumstances. It was introduced in or after 
  v.1.210.

# June 7, 2016 (1.211 to 1.214)
 * friskcache and cacheinfo function updated to work 
  with hashed index files.

# May 20, 2016 (1.210)
 * Added hashing to gridcaching so that the index is broken 
  up into many small files and reading a grid from the cache doesn't require 
  reading the entire index. This version included a dummy version of friskcache.

# April 25, 2016 (1.205 : 1.209) 
 * Added gridstats, pgridstats, tilestats, and 
  mergestats all for calculating statistics on one grid based on one or more 
  grouping grids. Eg. calculating stats on landscape capability
  by ecosystem and state.

 * Also added assembledataframe which uses dplyr to quickly build a data.frame 
  (of the dplyr flavor) from a list of grids. This introduced a dependency on 
  dplyr.

# April 13, 2016 (1.203, 1.204)  
 * Added movingwindow, movingwindow.grid, 
  movingwindow.character
  Added as.grid method for raster objects.
  Added raster method for grid object.
  Added imports for raster package methods and classes.

# February 23, 2016 (1.202)
 * Added pathonly argument to checkgrid that allows for just checking whether the 
  path is a valid path for a grid (and skips all tests of the griditself)

 * .writeblock and gridcreate now call checkgrid with pathonly = TRUE when an 
  error is thrown. If the error is related to the path it will throw a 
  constructive error instead of EDI's error (which was usually 
  GRID_SERVER_ACCESS_ERR_GRIDCANNOTCREATE).
  
 * .readblock also calls checkgrid after an error but with test = FALSE 
  The end result of all this should be more informative errors. 

# February 22, 2016  (1.201) 
 * Added launchbatchstitch and launchbatchmosaic that rely on new host argument 
  to anthill::simple.launch to allow grids to be stitched from any machine.

 * Added checkgrid function to check for common problems with input grids. 

 * Added ismultitiled function to check if an ESRI grid is multitiled.

# February 8, 2016  (1.200)
 * Bug: Inf as kernel.dim in guassiansmooth and kernsmooth (no upscaling) 
  results in error due to undefined tilesize, fixed by forcing factor to be at 
  least 1. 


# January 12, 2016 (1.199)
 * New function checkcellalignment checks to see if cells from grids or grid 
  descriptions match. This replaces code in matchextent, overlay, and 
  gridintersect and fixes a bug in all three where non-integer cellsize could 
  erroneously lead to an alignment error when the grids were aligned.
  
 * print.grid was updated to include full precision of grid elements and to 
  print 5 random cell values.

# December 11, 2015 (1.198)
 * Updated readblock to avoid fringe case nanometer misalignment bug.

# November 19, 2015 (1.197)
 * Fixed bug in batchmosaic where it would create grids ending in "_s_m" instead 
  of lopping off "_s"
  
 * Fixed bug in batchstitch to prevent restitching when only the capitalization 
  of the mosaic path changed.
  
 * Added na.value argument to kernelsmooth and guassiansmooth (passed onto swap 
  when weights are used.)
  
 * Cleaned up documentation slightly in gaussiansmooth.
  
 * Changed default kernel.dim argument to gaussiansmooth from 9 to 21.

# June 6, 2015 (1.196)
 * added winopen, a simple function to open a path or file using windows 
  explorer.

 * fixed bug in batchstitch, it wasn't rebuilding a stitched file that had been 
  deleted.

# May 15, 2015 (1.195)
 * fixed bug in formatdouble that caused it to fail with negative numbers.

# February 24, 2014 (1.194)
 * Added cleargridinfo cache function.
  friskmosaic no longer uses grandfathered frisking on old mosaics 
  Also, refreshservers now also calls cleargridinfo.  

# January 28, 2015 (1.188 to 1.191)
 * Fixed bug in pgridpredict with groups.

# January 23, 2015 (1.187)
 * Fixed bug in formatdouble that caused it to fail on small numbers.  

# January 21, 2015 (1.186)
 * Added argument to pgridpredict, tilepredict, and gridpredict that allows 
  different models to be applied to different parts of the landscape based on 
  a group grid.

 * makemosaicpy, and batchstitch no longer use the cached grid descriptions. 

 * fixed bug in griddescribe on mosaic with getstats=TRUE (max was wrong)


# December 2, 2014 (1.185)
 * Attempting to fix bug in writeblock introduced by gridinfo caching (v 1.184)

# November 20, 2014 (1.184)
 * overlay now checks cell alignment.
  readblock now checks to make sure the path is a character of length 1
  makemosaicpy now deletes the empty panes prior to finishing as requested by 
  BC
  
 * implemented gridinfo caching - new functions savegridinfo and 
  restoregridinfo allow for saving and restoring the cached info.  
  griddescribe now automatically fills the cache and pulls from it. 
  
 * griddescribe has a new argument cache  which controls whether or not the
  cached info should be used or whether the grid should be reexamined on disk.
  
 * griddescribe now returns an object of class griddescription and new function 
  print.griddescription prints out the result much more cleanly than the 
  standard list printing mechanism.
  
 * print.grid now prints the full digits of all numbers. Previously it printed 
  fewer significant digits on bigger numbers which often meant the x and y
  coordinates lost the digits after the decimal.

# Novemeber 7, 2014 (1.183) 
 * griddescribe( getstats=TRUE) now works properly with mosaics.

# October 24, 2014 (1.182)
 * Bug fix in restore connections.

# September 14, 2014 (1.181)
 * Bug fix in makemosaicpy.

# September 11, 2014 (1.180)
 * modified friskmosaics to check the mosaic timestamp against Sept 10 (noon) 
  and use the old modification date lookup method for old mosaics and the new 
  method for new mosaics.  This essentially grandfathers in the old mosaics. 

# September 10, 2014 (1.179)
 * Modified makemosaicpy and make mosaic to only check the "sta.adf" file when 
  determining the sourcedate.

# September 9, 2014 (1.178)
 * Added function rasterizetiles that converts tile data to grids.

 * Modified friskmosaics so that it only looks at the statistics file 
  ("sta.adf") of the source grid(s) to determine the modification date. This 
  is to avoid problems where doing things like building pyramids and 
  calculating statistics on the grid caused the modification date of component 
  files to change thus making it appear as if the grid had changed when the 
  data was still the same.

# July 30, 2014 (1.177)
 * Changed lock name used for getting a lock while caching to accomidate 
  anthill thread killing.

# June 16, 2014 (1.176)
 * When copying readblockc now uses the max of all prior ID's + 1 as the id of 
  the new grid in the cache. Previously it used the first unused positive 
  integer.
  
 * Updated caps_lib.dll and added new functions refreshservers and 
  .refreshserver.

# June 12, 2014 (1.174, 1.175)
 * gridtable and pgridtable have new argument digits.  It defaults to NA which 
  mimics previous behavior.  If not NA then the contents of the grid are 
  rounded to this many digits (the argument is passed on to round.)

 * updated friskcache it now both deletes directories that exist on disk but 
  aren't in the index and deletes index records that don't have corresponding 
  grids on disk.

# May 19, 2014 (1.173)
 * upscale now implemented in C++ as well.

# May 16, 2014 (1.172) 
 * kernalsmooth and calckernel both now call Rcpp functions for roughly 100 
  fold speedups.

# May 1, 2014 (1.171)
 * Changed readblockc so that when it deletes grids from the cache it uses a 
  shell command. The hope is that this will be more effective than unlink 
  which seems to fail often. 
  
 * Edited python function that's called by stitchpy.  It now should work for 
  most mosaics that contain different bitdepths among the panes by promoting 
  to an appropriate bit depth.
  
 * Edited restoreconnections so that it works when the restored active 
  connection has no window set.
  
 * Edited stitchpy so that it creates a log of the date and path of each stitch 
  operation for the mosaic. This allows us to tell if the stitch is up-to-date.

 * restoreconnection and saveconnection now restore and save .status$as.mosaic

 * Added batchstitch function.

# April 28, 2014 (1.170) 
 * Tracked down bug in getcachedir to the line where I was 
  converting type from 1, 2 to "integer", "real".  APL wrote 0 to type while 
  copying the grid and R didn't handle that well.

# April 28, 2014( 1.168, 1.169) 
 * Fixed bug in pgridtable which caused it to fail 
  if you didn't use the tiles argument.

 * Eliminated redundant call to getcache by passing the cache info to 
  getcachedir from readblockc. 

 * Added code to readblockc to help track down an intermittent error that 
  occurs while reading the file.

 * Changed griddescribe so that it does not use the gridcache to get the 
  description. Currently this is a option ("describefromcache") buried in 
  griddescribe .

# April 23,  2014 (1.167)
 * Changed newmosaic to make it more robust to multiple 
  simultanious requests to create a mosaic.

# April 21, 2014 (1.166)
 * Fixed bug in tilefun that caused it to explode when the 
 function only used one grid.

# April 17, 2014 (1.164)
 * Fixed bug introduced in writeblock.
  Updated documentation.

# April 15, 2014 (1.163) 
 * Added desc argument to refgridinit and mosaicinit.  
  Edited griddescribe so that it returns mosaicinfo when called on a mosaic.
  Changed pyramid type used for floating point grids in stitchpy.R

# April 8, 2014 (1.162)
 * Added new funcion mergetables that merges the result of multiple calls to 
  table.
  
 * Updated gridtable so that it can work with a vector of grids.
  
 * New function pgridtable uses anthill to create tabular summaries of grids in 
  parallel.

 * Increased timelimit on makedir to 60 seconds because of Windows crappiness. 

 * New functions pathR and pathWin edit the path in the clipboard to match the 
  path delimiter used in windows or R. 

# April 3, 2014 (1.161) 
 * Updated refgridinit.

 * Added round() calls to gaussiansmooth.character in hopes of avoiding 

 * nanometer grid induced bug.

 * Tweaked makedir() so that it waits up to 2 seconds for a directory to exist.

 * Updated plot.grid so that it includes a legend bar.  

# March 25, 2014 (1.159) 
 * I switched the default of the transparent argument to FALSE in writeblock 
  (which also controls the default behavior of writetile). I think this is a 
  more sensible default as it is slower to write transparent and generally 
  isn't needed.  Transparency is special and needs to be asked for.

 * When writing tiles or blocks to a non-existent grid or mosaic the default 
  behavior is now to create a mosaic if mosaicing is in effect.  This is 
  achieved via a .status$as.mosaic which is set to FALSE when gridinit is 
  called and TRUE when mosaicinit is called.  The presmise is that if 
  mosaicing is configured you want to write mosaics.  This behavior can be 
  overridden by setting as.mosaic when calling writetile or writeblock.

# March 21. 2014 (1.158)
 * Fixed refgridinit.
  
 * Commented out code in makemosaicpy that makes the referenced mosaic. It 
  occasionally fails and nobody is using them.

# March 20, 2014 (1.157) 
 * Fixed bug created in prior version where calls to
  newmosaic generated a deadlock.

# March 19, 2014 (1.156)
 * If anthill is running mosaic subdirectories are now locked prior to creating 
  the directory to circumvent a problem where directory creation fails when 
  multiple threads attempt to create a directory at the same time. 
  
 * readblock now converts paths with backslashes to forward slashes to avoid 
  problems in readblockc. 
  
 * New functions tilefun, gridfun, and pgridfun allow functions to be applied 
  to grids.
  
 * Fixed bug in writemosaicdesc where type was always forced to "derived".  

# March 17, 2014 (1.155)
 * First attempt to fix super-elusive-nanonmeter-misalignment bug in 
  writeblock.  

# March 14, 2014 (1.154)
 * Fixed bug in pgridpredict when the tiles argument was used. 	

# March 14, 2014 (1.153) 
 * Fixed bug in makemosaicpy (introduced recently in an effort to improve error 
  messages).
  
 * Switched to only using ";" as a comment character in all gridio tables and 
  files (mosaic descriptions, grid cache files, etc. )
  
 * Added friskcache function which should only be called when no threads are 
  running.  Anthill calls this during launch if caching is on and no threads 
  are running.

 * Updated lockstatus so that it doesn't throw an error when the queue doesn't 
  exist (the gridserver no longer maintains queue's that aren't in use.)

 * Changed lock handling around caching in readblockc, getcachedir, and 
  putcachedir. Locks are now always aquired and released within the same 
  function allowing on.exit to be used anwhere to gaurentee the lock is 
  released after errors.  

# March 12, 2014 (1.152)
 * Switched to using the short version of the computer name when getting a lock 
  on the gridcache directory. 

# March 5, 2014 (1.151)
 * Fixed bug in batchmosaic that prevented it from deleting old mosaics.


# Feb 25, 2014 (1.150) (Back to R 2.14)
 * Added friskmosaic, batchmosaic, readshape, and writeshape functions.

 * Dropped some depreciated names: x.to.c etc, points.to.grid, and 
  density.weight.

 * Functions still exist without periods. Other depreciated names are still 
  floating out there but those were ones I changed first and they are the ones 
  that throw warning every time I check the package.
  
 * Fixed bug created while changing argument name in kernelsmooth.  

 * Cleaned up documentation. Down to 1 rcheck error (from including the dll).
  Dropped shortpath function - it didn't work on my Windows 7 machine and 
  wasn't used.  It's in Junk now.

# Feb 13, 2014 (1.149)
 * Attempted to build on Windows 7 against R 3.0.2 (success but not official 
  yet.

# Feb 6, 2014 (1.148)
 * Added functions kernelsmooth.character and makeparetokerne. The first is a 
  generic version of gaussiansmooth.character the second creates a 
  paretokernel which can be used with kernelsmooth.character.
  
 * Changed argument in kernelsmooth from RobjS to RxS for consistency.

# Feb 4, 2014 (1.147) 
 * New Version of caps_lib that doesn't have a memory leak.

# Feb 4, 2014 (1.146)   
 * Updated package to track calls to .gridinit in 
  .status$gridinitcount.  Also new function gridinitcount returns the count. 
  This allows anthill to refresh threads after the count has reached some 
  threshold.  This is all a Kludge to get around the fact that there's a memory
  leak that causes the DLL to fail after many calls to .gridinit. 
 
# Feb 2, 2014 (1.145)
 * Fixed bug in readblock where the buffer was larger than specified.

# Jan 28, 2014 (1.143, 1.144)
 * Fixed bug in makemosaicpy with some grids it was including extra rows or 
	columns in the individual panes. It now uses a slightly shrunk clipping 
	polygon for each pane to avoid this and checks for bad pane extents.

 * Fixed bug in blocksize limit.

# Jan 24, 2014 (1.142)  
 * Fixed bug in path delimiters written by makemosaicpy.
	Added blocksize limit.
	
# Jan 23, 2014 (1.136 - 1.140)
 * Fixed bug in checkwindow which threw an error with nanometer differences in 
  windows.  

 * newmosaic, readblock, and writeblock all throw more meaningful errors when 
  mosaic init hasn't been called (and is needed).
  
 * pgridpredict mosaic and cache debugging
  
 * fixed bug in readblockc where check for timed out copying of grid info cache
  failed.

# Jan 23, 2014 (1.135)
 * pgridpredict, gridpredict, and tilepredict are now mosaic savvy with new 
  arguments refmosaic and as.mosaic.  

 * Fixed bug in newmosaic - it wasn't writing the nodata component of the 
  extent to the mosaicdescp file. 

# Jan 22, 2014 (1.134)
 * readblock now throws an accurate error message when there is no grid at the 
  specified path.

 * nmakemosaic and makemosaicpy now write the correct sourcedate and source 
  delimiter to mosaicdesc.txt.

 * gridpredict and tilepredict now have a cacheok argument which defaults to 
  TRUE.


# Jan 6, 2014 (1.133)
 * tileinit uses getwindow instead of directly accessing 
  .status$referencewindow this fixess a bug when connecting to remote severs 
  and using tileinit.

# Jan 2, 2014 (1.132)
 * getcache now works with brads version of the cache file. 

# Dec 16, 2013 (1.131)
 * Added additional locking when reading and checking for the existence of 
  mosaicdesc.txt.  

# Dec 6, 2013 (1.130)
 * New .dll includes function shortpath which is now embedded in grid copy to 
  circumvent some bugs in ESRI's software with long pathnames. 

# Nov 12 2013 (1.129) 
 * more of the same.
# Sept 2013 (1.128)
 * Added grid caching.
  
 * Added support for reading and writing from mosaic grids on disk.

 * Dropped support for matrix methods of writetile, and writeblock.
  Rearranged global variables significantly to support caching and mosaicing 
  all in the .status environment
		referencewindow (was header)
		mosaic (new)
		mosaicwindow (new)
		connections (new)
		activeconnection (new)
		tilescheme (was tile.scheme)
		subtilescheme (was subtile.scheme)
		workingresolution (new)
	Many functions now have multiple versions.  
		readblock deals with mosaics and calls readblockc which deals with caching 
		  and calls .readblock which interfaces with the dll.
		writeblock deals with mosaics and calls .writeblock
		cleanup cleans up everything and calls .cleanup which just disconnects 
		  from the current gridserver or local connection.
		gridinit has new argument add which allows you to add a connection to the
		  previously defined connections it calls .gridinit as well
		generally the versions that start with a dot are lower level functions not
		  intended for end users.
	New userlevel functions
		mosaicinit - for defining the multiple connections (servers) needed to 
		  connect to a mosaic 
		makecacheconfig - for creating a cacheconfig file on disk - the first step
		  in setting up a caching on a computer.
		cacheinit - for setting up a cache directory on a computer. Use once per 
		  computer not per session.  Will clear preexisting cache. Use with caution) 
		gridioverbosity - for setting how much gridio functions print to the terminal 
		  while running the default 0 means no printing.  Not implemented across all
		  functions yet.  This is intended mostly for debugging.
	
 * gridinit no longer has a source argument. Instead it has a server argument.  
  
 * It is no longer possible to use relative paths in gridio.  Sorry.  This was 
  incompatible with caching and mosaics.

# July 15, 2013 (1.126)
 * Minor change to read.ascii.grid now allows it to work with connections.
 
 * read.table closes the connection so now I'm calling read.ascii.grid.header 
  (which uses scan) prior to read.table.  Scan doesn't close the connection.
  This allows me to use it on compressed ascii grids 
  eg: read.ascii.grid(gzfile(path))

# June 2013  (1.125)
***** Last Pre-Mosaic Version ******

 * New functions:
  gridsplit - splits a grid into subgrids that are either tiled evenly or 
  irregular and based on an index shapefile with polygons.

 * gridstich - assembles a grid from tiles created by gridsplit back into a 
  single grid.

 * gridintersect - uses two grids or extents to create a new grid or extent 
  covering the area that they both overlap.

 * Updates:
  readblock has a new argument "extent" which is an alternative way of 
  specifying what part of the grid should be read using a list with the 
  standard extent items (xll, yll, nrow, ncol, and cellsize) or a full grid.

 * A minor bug in as.grid.SpatialGridDataFrame has been fixed.  Previously the 
  components were named list items which each contained a named number. Now 
  they are just named list items with numbers.  Most functions worked fine 
  before but some checks would fail.

 * Renamed old functions that had "." and clashed with S3 methods.  The old 
  function names currently still work with a warning that they are 
  depreciated.  I'll drop them eventually.  This includes the conversion 
  functions:  x.to.c, r.to.y c.to.fractional.x etc  which were changed to x2c, 
  c2fractionalx etc.  As well as match.extent and density.weight which both 
  lost their ".".

# May 17, 2013 (1.124)
 * Added new function gaussian.interpolate to convert irregular points into a 
  grid by performing a gaussian weighted mean of the points near each cell.  

 * Fixed bug in as.grid.SpatialGridDataFrame which yielded a transposed 
  matrix.  I'm not sure if this is the result of a change in the internal 
  structure of the SpatialGridDataFrame or just something I missed.  That 
  function is called by as.grid.SpatialPointsDataFrame so both were affected. 

 * tilepredict now wastes less time on empty tiles.

 * Changed coincide to be slightly less stringent (it no longer checks for 
  conforming attributes and names).

# April 25, 2013 (1.123)
 * Updated DLL to include new version that is compatible with both ArcView and 
  ArcMap.  
  
 * Moved the dll into inst/libs/i386/  from src/.  


# April 15, 2013 (1.122) 
 * Added function constrained.sample to perform minimum distance point sampling 
  with or without binned point data.

# Sept 25, 2012 (1.120)
 * Updated dll has new version of function MVREP (not used by this library).
  writeblock function should now report proper error code (in server mode) 
  when the grid doesn't exist.  I've updated writeblock.matrix to reflect this. 

# Sept 9, 2012 (1.120)
 * lookup.error fixed. N

# August 21, 2012 (1.119) 
 * write.block.matrix now looks up the error code to see if the error is 
  "GRID_SERVER_ACCESS_ERR_GRIDCANNOTFIND" instead of assuming that error is 
  always associated with a specific error number.  This fixes a bug introduced 
  when the error number of that error changed in Edi's latest dll.  Note: I 
  think Edi's error codes are wrong.  Empirically -10005 indicates a missing 
  grid.

# July 31, 2012 (1.118) 
 * Replaced all tile.scheme() function calls with tilescheme().
  Added gridtable function with generic, grid, and character methods.	

# July 30, 2012 (1.117) 
 * Fixed bug with write.ascii.grid and tweaked read.ascii.grid so that the grid 
  object it returns has the list items in the standard order.

# July 9, 2012 (1.116)
 * Added subtiledetails, tilecenter, subtilecenter.

# June 7, 2012 (1.115)
 * Updated dll from Edi fixes bug in patch scan. 

# May 3, 2012 (1.113, 1.114)
 * Incorporated another updated capslib.dll and added getpid function.

# May 1, 2012 (1.112)
 * Incorporated updated capslib dll.  There should be no change in 
  functionality. 

# April 6, 2012 (1.111)
 * Made downscale robust to improperly ordered elements in grid object. 
  (Previously if "m" wasn't the first element it would fail.

 * Made points.to.grid return a properly ordered grid object. Previously "m" was 
  the last instead of the first element.

# March 28, 2012 (1.110)
 * Fixed bug in stid.  Slight modification to downscale that might help with 
  reported gaussian.smooth bug.

# March, 23, 2012 (1.109)
 * Added function stid which returns a unique id for every subtile in the 
  landscape.
  
 * Modified settile so it checks to make sure the tilescheme is initiatied.

# March 20, 2012 (1.108)
 * swap - more efficient and now has arguments na.value, and no.match that can 
  be used to define replacements for NA values and non-matching values. 

 * added "tests" directory to package for including tests.

 * gaussian.smooth  - added argument no.match and which is passed on to swap 
  (when weights are defined.)  Also fixed potential bug in method 2 which 
  might have prevented weights from being used. 

# March 14, 2012 (1.107) 
 * Fixed bug in writegrid that caused it to not honor as.integer=TRUE

 * Dropped optional internal tileinit call from readtile;  you now must call 
  tileinit before readtile associated arguments rowsize, buffer, colsize, and 
  noread are dropped as well.

 * Added coreonly argument to newtile, readtile, and writetile.  in readtile 
  and newtile it defaults to FALSE in writetile it defaults to TRUE. (The 
  are correct if you are reading in the buffer to calculate values for the 
  core but don't want to write out the buffer which presumably contain the 
  edge effects) The defaults mimic prior behavior. 
  
 * Added as.grid.matrix (matrix method for as.grid function) to convert 
  matrices into grid objects.

# March 6, 2012 (1.106)
 * Added mechanism for working with subtiles   Subtiles allow for reading in a 
  grid in tiles and processing it in fractions of tiles. This is central to 
  speeding up the LCAD model but is probably of minimal utility elsewhere. see 
  ?subtile for an example. 
  
 * New Functions:  subtileinit, stinit, ntiles, nsubtiles, sti, stci, stri, 
  overlay, extractsubtile newtile, setsubtile

 * Modified Functions: tiledetails, tileinit, plotextent, cleanup, settile, 

# March 5, 2012 (1.105) 
 * gridcreate now throws a warning if the grid already exists instead of an 
  error.  The warning can be suppressed by setting the new "silent" argument 
  to TRUE.  Either way the grid on disk is left unchanged.
  
 * The effect of the as.integer argument in readblock, writeblock, readgrid, 
  and writegrid is subtly changed to improve efficiency (most critical when 
  using a gridserver to process in parallel).  Generally speaking the 
  functions now assume that if specified the as.integer argument correctly 
  describes the type of grid on disk.  If this isn't true errors will result. 
  The benefit is a call to griddescribe is no longer necessary for every read 
  and write.  Omitting the argument when reading and writing is generally 
  safer but will necessitate calls to griddescribe. Additionally if as.integer 
  is missing and the grid doesn't already exist on disk than when the grid is 
  created it will be of type "real".  

 * coincide, a new function, tests whether two grids or related objects  
  coincide (all cells of one overlap all the cells of the other).  

 * match.extent now uses isTRUE(all.equal(a, b)) instead of a==b when testing 
  to see if cellsize and grid snapping is the same in its arguments.  This 
  will hopefully fix a problem in gaussian.smooth where it sometimes fails 
  with purported misalignment problems.

 * There are expanded examples in a few functions' help files to facilitate 
  automatic testing.

# Feb 7, 2011 (1.099)
 * Added mgridpredict function to make predictions from grids in memory. 

# Jan 10, 2011 (1.098)
 * Added as.integer argument to tilepredict and gridpredict.  This defaults to 
  FALSE which is identical to prior behavior but now allows for storing 
  predictions in integer grids. 

# Dec 1, 2011 (1.097) 
 * Built under R 2.14.0
  Fixed bug in tiledetails(coreonly=TRUE)
  Added list argument to plotextent.

# Nov 22, 2011 (1.096) 
 * readblock, writeblock, and griddescribe now throw an error when the path 
  argument contains  NA, NULL, or character vectors of lengths other than 1  
  (previously these were passed to C++ and resulted in a hard crash).

# Nov 18, 2011 (1.095) 
 * Added gridoverwrite and tileoverwrite functions. 

# Nov 17, 2011 (1.094) 
 * Added tilehist and plot.tilehist functions to calculate and plot histograms 
  for an entire grid by iteratively reading tiles.

# Nov 15, 2011 (1.093)
 * Revised raw.spread function so that users can modify the use long diagonal 
  argument to Edi's function via the "square" argument to raw.spread 
  (square=FALSE is default).

# Oct 26, 2011 (1.092) 
 * revised spread function (still chasing bug in spread).  

# Oct 25, 2011 (1.091)
 * New version of Edi's dll, updated to fix a problem with the spread function.

# Oct 20, 2011 (1.090)  
 * Fixed tilepredict(and inherently gridpredict) so they work even when the 
  model only has one predictor variable. (Added drop=FALSE)

# Oct 5, 2011 (1.089)
 * Added tilepredict and gridpredict methods.  
  Added sample dataset see example in ?gridpredict

# Sept 30, 2011 (1.088) 
 * Updated package to use new version of edi's DLL.
  Added patch.scan function.
  Changed readgrid and writegrid to use writeblock (avoiding some of the 
  memory limitations of edi's readgrid and writegrid C functions).  Old 
  versions still accessible by setting use.old=TRUE.

# Sept 26, 2011 (1.087, 1.086 )
 * Fixed bug in density.weight function.

# Sept 22, 2011 (1.085)
 * Added density.weight function so Bill can calculate weights.

# Sept 21, 2011 (1.084)
 * Fixed bug in method 3 of gaussian.smooth that prevented sampling from 
  working.


# Sept 16, 2011 (1.083) 
 * In guassian.smooth.character:
#   Decreased buffer.
  
 * Worked to get upscaled cells aligned across tiles,
  
 * Deleted intermediate stuff while working with tiles to minimize memory usage
  
 * Added a third method of calculating the guassian smooth which reads tiles, 
  and upscales to assemble a complete upscaled grid in memory then smooths the 
  whole grid and finally, downscales and writes out in tiles,
  
 * Changed default value of max.p.na to 1.

 * Fixed ugly bug in tiledetails() function where last row was in the wrong 
  place. I think it must have been introduced recently otherwise it wouldn't 
  have passed the tests.

# Sept 13, 2011 (1.082) 
 * Increased buffer again. (gaussian.smooth)

# Sept 6, 2011 (1.081)
 * Increased buffer in the character method for gaussian smooth when working 
  with tiles.

# August 19, 2011 (1.080)
 * Added weights argument to gaussian.smooth.	

# August 17, 2011 (1.079)
 * Added tile.scheme function to return the details of the current tile.scheme.  

 * Added print method for grid objects to suppress printing of the primary 
  matrix. It does print all the other values in the list.

 * Added functions: upscale, downscale, matchextent, and gaussian.smooth
  (methods both for grids and character paths to grids on disk. The 
  guassian.smooth function upscales the grid, smooths it, and then downsamples 
  it back to the original resolution and extent.

 * Changed name of function plotextant to plotextent.		

# August 5, 2011 (1.078)
 * Added settile function to set the tile iterators to a specific tile number.

# July 28, 2011 (1.077)
 * Switched to latest version of Edi's .dll which uses serialization in its 
  socket communications. This shouldn't effect stand alone mode but if you are 
  using it with a gridserver you should be using the latest version of the 
  gridserver as well.

# July 13, 2011, (1.076)
 * Fixed bug in plot.grid that caused it to fail if all cells in the grid had 
  the same value. 

# July 4, 2011, (1.075) 
 * Changed plot.grid to use rasterImage instead of Image.  Old version still 
  available by setting the use.old argument to TRUE.

# June 29, 2011 (1.074)
 * Implemented tracking of gridwait time in all functions that call C++ 
  functions that interact with the gridserver.  Two new R functions : 
  get.gridwait  reset.gridwait

# June 17, 2011 (1.073)
 * added points.to.grid function to create a grid from x, y, z values.

# June 16, 2011 (1.07)
 * getwindow function added to Edi's code and an R wrapper created. 
  gridinit now calls getwindow when used in server mode and updates the header 
  appropriately.

# June 1, 2011 (1.06) 
 * Added namespace.
  Stored "global" variables within an environment called .status.  On load 
  loads  grid.server.status, header, tile.scheme, err.table into this 
  environment. 
  
 * Added brad's function kern.source.

# March 8, 2011 (1.05) 
 * Added spread and raw.spread functions.
  Fixed spelling of gaussian in make.gaussian.kernel function

# February 3, 2010 (1.04)
 * Fixed problem with writing integer grids in writegrid.

# February 2, 2010 (1.03)
 * Added Functions:
    calc.kernel 
    kernel.smooth
    make.guassian.kernel
    replace.nas
  (These are all for non-resistant kernels)

# January 24, 2010 (1.02)
 * Fixed bug in call to parse error in writegrid.grid function which caused 
  that writegrid to report a meaningless error when it failed.

 * Changed default port in server mode to 3336.
