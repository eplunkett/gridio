\name{saveconnections, restoreconnections}
\alias{saveconnections}
\alias{restoreconnections}
\title{
functions to save and restore connections
}
\description{
These functions save and restore the internal state of gridio2 including the connection list, the current active connection, and the subtile and tile schemes.  They are useful when writing higher level functions that must mess with connections but which would like to return everything to the initial state.
}
\usage{
saveconnections()
restoreconnections(x)
}

\arguments{
  \item{x}{
The result of a prior call to \code{saveconnections} containing gridio2 status information to be restored.
}
}
\details{
This function both resets the status information and also reconnects to the previously active server.
}
\value{
\code{saveconnections} returns a list of gridio2 status information suitable for use with \code{restoreconnections}.  \code{restoreconnections} returns nothing.
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{gridinit}} and \code{\link{mosaicinit}} for creating connections. \code{\link{tileinit}} and
\code{\link{subtileinit}} for creating tileschemes.

}
\examples{

oc <- saveconnections()
cleanup()
restoreconnections(oc)

}
\keyword{ spatial} 
