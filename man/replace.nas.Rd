\name{replace.nas}
\alias{replace.nas}
\title{
function to use a kernel to replace NA values in a matrix
}
\description{
this function uses kernels to interpolate the values of missing cells in a matrix. It will not replace \code{NA} cells that have fewer than \code{min.cells} in their kernel window.
}
\usage{
replace.nas(m, k, min.cells = 3)
}
\arguments{
  \item{m}{
a matrix with NA values to be replaced.
}
  \item{k}{
the kernel of weights used to calculate the weighted average value of cells around each NA. It should be a square, odd dimensioned, numeric matrix that sums to 1.
}
  \item{min.cells}{
the minimum number of non-NA cells required (in the kernel window) to calculate a replacement value.  This should never be set to less than one.
}
}

\value{
a matrix of the same dimensions as m.
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{calckernel}}, \code{\link{makegaussiankernel}}
}
\examples{

# Make a kernel (k) 
sr <- sqrt(2)
k <- matrix(c(sr, 1, sr, 1, 0, 1, sr, 1, sr), 3 ,3)
k <- k/sum(k)

# Make a test matrix with an NA value
m <- matrix(1:20, 4, 5) 
row <-  3
col <- 2
m[3,2] <- NA

m <- replace.nas(m, k)

# add more NA's
m[1,] <- NA
replace.nas(m, k)  # corners aren't fixed because they only have 2 neighbors

replace.nas(m, k, min.cells=2) # corners fixed  too 
}
\keyword{ spatial}

