\name{checkgridalignment}
\alias{checkgridalignment}

\title{
function to check if grids are aligned
}
\description{
this function prints and invisibly returns the paths of grids that aren't aligned with the reference grid.  To align the grids must share cellsize, number of rows, number of columns, and x and y coordinates of the lower left corner. 
}
\usage{
checkgridalignment(reference, grids)
}
\arguments{
  \item{reference}{
 path to a reference grid
}
  \item{grids}{
  paths to grids to be evaluated against the reference grid
}
}
\details{
This is a wrapper to coincide which facilitates checking alignment of multiple grids on disk at once.
}
\value{
The function invisibly returns a vector of the subset of the paths in \code{grids} that point to grids did not align with the \code{reference} grid.  
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{coincide}} allows pairwise comparison of the alignment of grids in memory and on disk.
}
\examples{


### Setup a temporary directory with a sample grid in it 
# Get path to example dem grid (included with package)

datapath <- system.file("exampledata", package="gridio2")  # grid path to example grid
datapath <- shortPathName(datapath)
datapath <- paste(datapath, "/.", sep="")

# Make a temporary directory to write new grids to
dir <- tempdir() # get temporary directory
if(!file.exists(dir)) dir.create(dir)

# Copy example data into temporary directory
file.copy(datapath, dir, recursive=TRUE)
# system(paste("open", dir)) # if you want to look at contents of temporary directory
###  Done setting up directory

cleanup() # disconnect from any prior gridservers 

gridinit() # initialize local gridserver
dem <- paste(dir, "/dem", sep="")
setwindow(dem) # setwindow to dem file

copy <- paste(dir, "/copy", sep="")
# Make another similar grid
gridcopy(dem, copy) 

# Check alignment of dem and copy
result <- checkgridalignment(dem, copy)
if(length(result) > 0) stop("dem and copy should align")

# Cleanup
gridkill(c(dem, copy))
cleanup()


}
\keyword{ spatial}

