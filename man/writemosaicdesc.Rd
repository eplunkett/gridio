\name{writemosaicdesc}
\alias{writemosaicdesc}

\title{
Function to write data to the mosaic description
}
\description{
This is intended for internal use only.
}
\usage{
writemosaicdesc(md, path, updatetimestamp  = TRUE, compression = "no")
}

\arguments{
  \item{md}{
A list similar with names identical to that produced by \code{\link{mosaicinfo}}
}
  \item{path}{
The path to a mosaic grid (not the description file)
}
  \item{updatetimestamp}{If \code{TRUE} (the default) the timestamp is updated to the current time prior to writing. If \code{FALSE} the timestamp isn't changed prior to writing. \code{FASE} is used by \code{\link{convertmosaic}} when converting grid mosaics to TIFF mosaics and \code{\link{compress}} }

  \item{compression}{Should be either 'no' or 'compressed', used to indicate
  if the mosaic contains compressed geoTIFFs either due to compression (\code{\link{compress}}),
  conversion (\code{\link{launchconversion}}), or mosaicing (\code{\link{makemosaic}}).
  Standard writes produce uncompressed geoTIFFs}

}
\details{
This function creates mosaicdescrip.txt populating the fields with \code{md} and the current time (for the timestamp). 
Panemap which is stored in memory as a logical vector is converted back to a sting of 0's and 1's prior to writing.
}
\value{
It returns nothing.
}
\author{
Ethan Plunkett
}
\seealso{
\code{\link{mosaicinfo}}
}
\keyword{spatial}
