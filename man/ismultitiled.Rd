\name{ismultitiled}
\alias{ismultitiled}
\title{
function to check if a grid is multitiled
}
\description{
this function checks to see if an ESRI grid (on disk) is multitiled, 
an extension of the format that makes it incompatible with gridio (v1). As
of gridio2 all ESRI grid compatability has been dropped so this is 
not relevant.
}
\usage{
ismultitiled(x)
}

\arguments{
  \item{x}{
The path to a grid on disk.
}
}
\value{
It returns \code{TRUE} if the grid is multitiled and \code{FALSE} otherwise.

}
\references{
ESRI grids can be "multitiled" in which case the API gridio (1) relied on will
not work.  Multitiled grids have multiple files of the format \file{wXXXXXX.adf} (where the X is numeric).  See: 
\url{http://www.digitalpreservation.gov/formats/fdd/fdd000281.shtml}
\url{https://trac.osgeo.org/gdal/ticket/1198}
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{checkgrid}} runs a battery of tests to check for known problems that will make a grid unreadable.
}
\keyword{ spatial }
