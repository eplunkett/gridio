\name{gridpredict, tilepredict, pgridpredict}
\alias{tilepredict}
\alias{gridpredict}
\alias{pgridpredict}
\title{
functions to apply statistical models to gridded data 
}
\description{
These function take a model fit or fits, read prediction data in from multiple grids, generate predictions and write the result out to grids.  tilepredict works on a single tile while gridpredict processes a whole grid in tiles. \code{pgridpredict} uses \pkg{anthill} to process a grid in tiles in parallel.
}
\usage{
tilepredict(fit, grids, vars, outgrid, tilenumber, na.rm = TRUE, list = NULL, 
            force = NULL, na.value = -9999, as.integer=FALSE, cacheok=TRUE,
            as.mosaic, group = NULL, ...)
            
gridpredict(fit, grids, vars, outgrid, tilesize, na.rm = TRUE, list = NULL, 
            force = NULL, na.value = -9999, overwrite = FALSE,as.integer=FALSE, 
            cacheok=TRUE, as.mosaic, group = NULL, ...)
            
pgridpredict(tilepredict.args, tilesize, tiles, refgrid, 
            gridserver, port, name, owner, refmosaic, ...) 
}

\arguments{
  \item{fit}{
	A  model fit to base predictions on.
}
  \item{grids}{
	A vector of grid paths specifying the data to use to make the predictions.
}
  \item{vars}{
	A character vector containing the variable name associated with each grid in \code{grids}.	
}
  \item{outgrid}{
	The path to an output grid. If \code{list} is specified this should be a vector of output grid paths.
}

  \item{tilenumber}{
	The tile number to be processed.
}

  \item{tilesize}{
	The tilesize to use when processing grids.
}

  \item{na.rm}{
	If \code{TRUE} then any cell for which there is missing data in any prediction grid will not be passed to the fitted model but will instead receive NA.  If \code{FALSE} then data for all cells will be passed to the model.
}
  \item{list}{
	A list of fitted objects may be passed with this argument in which case outgrid should be a vector of outgrid paths (one for each fit in list).  If list is specified the fit argument is ignored.
}
  \item{force}{
	A named list containing variable names and associated values for variables that will be forced (to a single value).  We use this to force a sample effort covariate to a constant value when making predictions with new data.  In most cases it will not be needed.
}
  \item{na.value}{
	This is the value used to represent \code{NA} values in the grid when passing data to the underlying C+ functions. It should be set to a value that does not occur in the data.
}

 \item{overwrite}{
  Specifies whether pre-existing grids should be overwritten.  If \code{FALSE} and \code{outgrid} already exists an error will be thrown.
}

 \item{as.integer}{
  indicates format for the new grid(s) if TRUE they will be integer grids;  \code{FALSE} indicates that the new grid will contain floating point numbers.  If you are making predictions from multiple models at once via the \code{list} argument than this should either be a single value which will be used for all output grids or a vector of TRUE and FALSE values that correspond to each output grid. The default value is FALSE which will make all output grids floating point. This is safe but possibly inefficient.
}

\item{cacheok}{if TRUE than the prediction grids are eligible for caching (if caching is configured). See \code{\link{cacheinit}}.
}

\item{as.mosaic}{if \code{TRUE} the output will be a mosaic. To work the user must call \code{\link{mosaicinit}} before calling \code{tilepredict} or \code{gridpredict}.  If calling \code{pgridpredict} and you specify \code{as.mosaic=TRUE} in the \code{tilepredict.args} list make sure to also use the \code{refgrid} argument. Note: this is optional and will default to writing mosaics (\code{TRUE}) when possible.}

\item{group}{ This optional argument allows applying multiple models to different cells in a grid based  on an integer grouping grid.  Group should either be left \code{NULL} or specify a path to a grid that contains grouping variables.  If \code{group} is used then \code{list} should also be used and should be named with classes (integers) that occur in the \code{group} grid. For each cell in the \code{group} grid for which their is a model in \code{list} with a matching name a prediction will be made with that model and output to the corresponding cell in \code{outgrid}. Note when group is used multiple fits (specified in list) will be used to fill a single output grid.

}


  \item{\dots}{
  For \code{tilepredict} and \code{gridpredict}: other arguments to be passed to the predict method.  For pgridpredict other arguments to be passed to \code{\link[anthill]{launch.project}} such as \code{priority}, \code{maxthreads}, and \code{onerror}.
}

  \item{tilepredict.args}{
a named list of arguments and their values to be passed to \code{tilepredict} the \code{tile} argument
should be omitted.
}

  \item{tiles}{
An integer vector indicating a subset of tiles to process (optional). 
}
\item{refgrid}{ Deprecated
}
  \item{gridserver}{ Deprecated
}
  \item{port}{ Deprecated
}

  \item{name, owner}{
the project name and owner. Required unless \code{set.owner} has been called.
}

\item{refmosaic}{ Deperecated}
 

 
}

\value{
The functions returns nothing.  \code{tilepredict} writes out predictions to a tile in \code{outgrid} and \code{gridpredict} and \code{pgridpredict} write their output to a new grid or mosaic.
}
\author{
Ethan Plunkett
}
\note{

\code{grids} and \code{vars} should be of the same length.

If \code{list} is specified and there is no \code{group} grid than 
\code{outgrid} should be a vector of output grids of the same length as \code{list}.

If \code{group} is specified than the names of \code{list} will be coerced to 
integers and compared to the values in the grouping grid. Models will only be 
fit where the two match.

\code{\link{tileinit}} should be called prior to the \code{tilepredict} function.

Data is passed to the \code{\link{predict}} method for the \code{fit} object 
via the \code{newdata} argument (to predict).  If that is not the right argument
for your fit than this will not work for you without further modification and 
you should let me (Ethan) know.

If variables needed by fit are missing or misnamed an error will occur but it 
may not be very informative.

When using a \code{\link{glm}} or \code{\link{lm}} fit to make predictions users
will probably want to include \code{type = "response"} in the arguments.  
See \code{\link{predict.glm}} and \code{\link{predict.lm}} for details.

Prior to calling \code{pgridpredict} you should load \pkg{anthill}, call
\code{\link[anthill]{config}}, define gridservers in 
\code{anthill/gridservers.txt}, and \code{\link[anthill]{launch.gridservers}}.

For \code{pgridpredict} gridserver information  will be retrieved with
\code{\link[anthill]{readgridservers}}. The format for \code{gridservers.txt} 
is defined in \code{\link[anthill]{launch.gridservers}}, and if you are using
mosaics \code{\link{makemosaic}} will create a template gridservers.txt within  
any mosaic you create that isn't based on a reference mosaic. }


\seealso{

\code{\link{gridinit}} must be called prior to both functions and 
\code{\link{tileinit}} must be called prior to \code{tilepredict}. 
\code{\link{mgridpredict}} is similar but works on grids in memory instead of 
grids on disk.

\code{pgridpredict} works in parallel, but requires \pkg{anthill} to be loaded
and running; see \code{\link[anthill]{config}}.

\code{\link{tilefun}}, \code{\link{gridfun}}, \code{\link{pgridfun}} are 
similar but apply a function or list of functions to grids.

}
\examples{

### Setup a temporary directory with a sample grid in it 
# Get path to example dem grid (included with package)
datapath <- system.file("exampledata", package="gridio2")  
datapath <- shortPathName(datapath)
datapath <- paste(datapath, "/.", sep="")

# Make a temporary directory to write new grids to
dir <- tempdir() # get temporary directory
dir.create(dir)

# Copy example data into temporary directory
file.copy(datapath, dir, recursive=TRUE)
# system(paste("open", dir)) # if you want to look at contents of temporary directory
###  Done setting up directory

cleanup() # disconnect from any prior gridservers 
gridinit() # initialize local gridserver with base path in temporary dir

# setup paths
dem <- paste(dir, "/dem", sep="")
gradientpath <- paste(dir, "/gradient", sep="")
obspath <- paste(dir, "/obs", sep="")
predpath <- paste(dir, "/pred", sep="")
predpath2 <- paste(dir, "/pred2", sep="")
predpath3 <- paste(dir, "/pred3", sep="")
grouppath <- paste(dir, "/groups", sep="")

setwindow(dem) # setwindow to dem file
tileinit(150, 0)  # initialize tilescheme 
# NOTE this you probably want 1000 to 3000 as the tilesize NOT 150
#      this is a trivial example

### Make up some sample data
g <- readgrid(dem)
gradient <- seq(20, 100, length.out=g$nrow)
g$m[,] <- gradient 
g$m <- g$m + rnorm(g$nrow*g$ncol)
writegrid(g, gradientpath)
elev <- readgrid(dem)
grad <- g
d <- data.frame(elev=as.numeric(elev$m), grad=as.numeric(grad$m)) 
d$y <- d$elev + d$grad + rnorm(nrow(d), 0, 10)
obs <- g
obs$m <- matrix(d$y, nrow=obs$nrow, ncol=obs$ncol)
plot(obs)
writegrid(obs, obspath)

# Make grouping grid (3 vertical stripes each a different group)
g <- newgrid()  
g$m[, 1:100] <- 1
g$m[, 101:200] <- 2
g$m[, 200:g$ncol] <- 3
plot(g)
writegrid(g, grouppath)

# Make fit objects
f <- lm(y~elev+grad, data=d)
f2 <- lm(y~elev*grad, data=d)
f3 <- lm(y~elev, data=d)  


# call tile predict (one tile only)
tileinit(150, 0)  # 2000, 0 would be more efficient
tilepredict(fit=f, grids=c(dem, gradientpath), vars=c("elev", "grad"), 
            outgrid=predpath, tilenumber=1)
p <- readgrid(predpath)
\dontrun{
plot(p)  # first tile only
}
# call tile predict on another tile with a single parameter model
tilepredict(fit=f3, grids=dem, vars="elev", outgrid=predpath, tilenumber=2)

# And gridpredict (whole grid in tiles)
gridpredict(fit=f, grids=c(dem, gradientpath), vars=c("elev", "grad"),
            outgrid=predpath, tilesize=150, overwrite=TRUE)
p <- readgrid(predpath)
\dontrun{
plot(p)
}
# Call with forced arguments
gridpredict(fit=f, grids=c(dem), vars=c("elev"), force=list(grad=mean(d$grad)),
            outgrid=predpath2, tilesize=200, overwrite=TRUE, na.rm=FALSE)
p2 <- readgrid(predpath2)
\dontrun{
plot(p2)
}
# Call with mutliple fit objects via list argument producting multiple grids
f2 <- lm(y~elev*grad, data=d) # fit object with interactions
gridpredict(list=list(f, f2), grids=c(dem, gradientpath), vars=c("elev", "grad"), 
            outgrid=c(predpath, predpath2), tilesize=1000, overwrite=TRUE)
p2 <- readgrid(predpath2)

\dontrun{
plot(p2)
}
# Call with multiple fit objects which are all applied to different parts of the 
# grid based on a grouping grid.
l <- list(f, f2, f3)
names(l) <- c(1, 2, 3)
gridpredict(list=l, grids=c(dem, gradientpath), vars=c("elev", "grad"), 
            outgrid=predpath3, tilesize=3000, overwrite=TRUE, group = grouppath)
p3 <- readgrid(predpath3) 

\dontrun{
plot(p3)
}


\dontrun{
# Make sure anthill is loaded, config() has been called and a gridserver 
# has been launched before running the this.
  server <- "testserver"  
  port <- 3333  

  tilepredict.args <- list(fit=f, grids=c(dem, gradientpath), 
                           vars=c("elev", "grad"), outgrid=predpath) 
# note this is the same as used with tilepredict except for the tile argument
  
  pgridpredict(tilepredict.args=tilepredict.args, gridserver=server, 
               port=port, path=path, name="pgridpredicttest", 
               refgrid=tilepredict.args$grids[1], tilesize=100, 
               maxthreads=3)  # Do not use 100 as your tilesize!
               
               
               
        } % end dontrun.       


# Cleanup
gridkill( c(dem, predpath, predpath2, obspath, gradientpath))
cleanup()


}
\keyword{ spatial }

