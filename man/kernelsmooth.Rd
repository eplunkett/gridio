\name{kernelsmooth}
\alias{kernelsmooth.character}
\title{
functions to apply a recipient-based kernel to each cell in a grid on disk with edge and NA correction
}
\description{
The character method to kernel smmooth creates a smoothed version of a grid on disk in which each cell of the grid passed to the function (as a path) has been replaced with the result of \code{\link{calckernel}} called on that cell. 
}
\usage{
\method{kernelsmooth}{character} (x, kernelfun, max.r, kernel.dim = 21, outgrid,
              overwrite = FALSE, tilesize = 2000, max.p.na = 1, sample.points, 
              weights, no.match = 0, na.value = NA, tiles, cacheok = TRUE, ...)
}
\arguments{
  \item{x}{
Either a matrix, an object of class "grid" containing the data, or the path to a grid on disk (character).
}

 \item{kernelfun}{
A function that generates a square, odd dimensioned, matrix that sums to 1.  It must have \code{max.r} and \code{cellsize} arguments.  Other arguments to \code{kernelfun} can be passed via \code{...} .
}
  \item{max.r}{
  The maximum radius that weights are calculated for (in map units). 
}
  \item{kernel.dim}{
      The cell dimension of the kernel in the upscaled grid. It must be an odd number.  This controls the amount of upscaling, which is adjusted such that a \code{max.r} radius kernel has this cell dimension.  Smaller values result in more upscaling and faster processing at the expense of precision.  The default value of this argument changed from 9 to 21 on 11/19/2015.
}
  \item{outgrid}{
	For the character method in which \code{x} is a path to a grid this argument specifies the path to the output grid.  If \code{sample.points} is specified this argument is optional.
}
  \item{overwrite}{
	For the character method this determines whether the output grid is overwritten.  If FALSE an error will be thrown if the grid already exists.
}
  \item{tilesize}{
	For the character method this determines the target tilesize.  A bigger number will mean fewer, larger tiles. The realized tilesize will not necessarily match this as the tilesize must be a multiple of the upscaling factor. 
}
  \item{max.p.na}{
	If the proportion of NA values (from the original resolution) in an upscaled cell exceeds \code{max.p.na} than it will receive an NA value during upscaling; otherwise the upscaled cell is given the mean value of all non-NA cells that it covers.
}		
 
 \item{sample.points}{
	(optional) a dataframe or matrix with columns "x" and "y" containing the map unit coordinates of points.  If specified the character method will return the a vector of sample values (from the smoothed surface), and the grid method will return both the samples and a grid (in a list).
}
\item{weights}{
	(optional) a dataframe or matrix with columns "value" and "weight".  If \code{weights} is provided than all the cells in the grid will be replaced with their associated weight; any cell whose value is not in the value column will be replaced with \code{NA}.  If \code{weights} does not contain columns named "value" and "weight" and contains only two columns it will be assumed that the first is "value" and the second is "weight".
}
\item{no.match}{
  If weights are supplied Non-NA values in x that don't match any value in original will be replaced with this value.
}
\item{na.value}{ 
If weights are supplied this determines the value to use when cells in \code{x} 
 are \code{NA}.  Note if the \code{weights} argument contains a value for  \code{NA} as well that will take precedence. 
 }
\item{tiles}{a subset of tiles can be specified with this argument. Note, however, that the associated tilesize and the \code{tilesize} argument must match the realized tilesize \code{\link{kernelscalingdetails}} for a way to precalculate the realized tilesize.}

\item{cacheok}{this is passed to \code{\link{readtile}}}
  
\item{\dots}{
Arguments to be passed to other methods. If x is a character (path) these are passed on to \code{kernelfun}.
}

}
\details{
If a gaussian kernel is used than the result is a smoothed version of the original grid or matrix. Other kernels could potentially be used to enhance edges or increase contrast or fit a different distribution.

These functions adjusts for missing values in the matrix and at the edges and in doing so assume that the kernel sums to 1.  

There are matrix and grid versions in \pkg{gridkernel} that work on those types.  See \code{\link{kernelsmooth.grid}} function for details.

This character version is more complicated.  It reads the grid in in tiles, upscales the tiles to a courser resolution, makes a kernel appropriate for the new cellsize, calls \code{\link{kernelsmooth.grid}}(grid method) on the upscaled data, then loops through tiles, downscaling back to the original resolution and writing to disk.
The upscaling allows for much more efficient processing of large kernels but also incurs some additional smoothing. The degree of upscaling can be controlled with the kernel.dim argument; a larger kernel.dim will result in less upscaling and more precise result.

}
\value{
an object of the same class as \code{obj} in which each cell is the result of applying a kernel to that cell.
}
\author{
Ethan Plunkett
}

\note{

The character version is based on method 3 of \code{\link{gaussiansmooth.character}}; it may be useful to implement methods 1 and 2 as well but 3 is generally best so that's where I started.



From Brad Compton (5/28/2011)
Ethan and I spent a while on the theory and practice of non-resistant kernels yesterday.  Here's what we came up with:

There are two approaches to estimating kernels: source-based and recipient-based.  Both give the same results, but there are significant differences in efficiency depending on the situation.  I'll use an example: we're building kernels around developed cells with weights for each of the four development classes. The source-based approach, familiar from home-range analysis, centers a kernel on each developed cell, multiplying the kernel by the cell's weight.  It then adds the kernel to the result, cell-by-cell.  The recipient-based approach (commonly used in image processing), on the other hand, visits each cell in the landscape for which you want a result, centers a kernel on that cell, multiplies the kernel value by the weights it overlaps, and then sums the volume of result and places it in the focal cell.  Both give you the same results, but when you have a small number of source cells (as in a home range analysis or the development example), a source-based approach is far more efficient, because run-time is proportional to the number of source cells.  If, on the other hand, you have a restricted set of cells that you want a result for and lots of source cells (say you want values for each vernal pool based on how much forest surrounds it), then the recipient-based approach is faster.

The other issue is edge correction.  This needs to be done if you have hard data edges in your landscape so you don't bias results downward near the edges.  I think this will not be an issue for the LCC project, except on the Maine-Quebec/New Brunswick border, and this won't really matter for development modeling, because there's nothing much on either side.  Edge correction is fairly costly, so we don't want to do it if it's not necessary.

I've attached \code{\link{kernsource}} , which is a source-based version without edge correction.  This is a version of my old kern.r with the arguments changed to be understandable by a human-being, instead of matching bkde2D's incomprehensible arguments.  Someday, I'll add edge-correction to this, but I don't think we need it now.  Liz, I presume you've got data beyond the Hampden County edge and will then clip the results to Hampden County.  I hope this version is acceptably fast for large kernels.  I'd try bandwidths up to 10 or 20 km or so.  Let me know how long this ends up taking.  I just did a quick test on a 1000x2000 matrix with 200 1's and a bandwidth of 10 km; it took a few seconds.

Ethan has a version that's recipient-based, with edge correction.  For applications where you only want to get results for a restricted set of cells and there are lots of source cells, this will be faster.  And where we need edge correction, this is the only version to use until I add edge correction.

If you're calling either version in blocks for a large landscape, you'll need to read a buffer around each block of at least bandwidth (in cells) * search distance.  For kernsource, you'll write  the entire block+buffer in transparent summation mode (results are added to the grid), for kern.recipient, you'll throw away the buffer when writing in normal replacement mode.

At some point, Ethan and I will integrate these two functions into one function with options for the various approaches.

Brad
}

\seealso{
\code{\link{calckernel}}, \code{\link{makegaussiankernel}}, \code{\link{kernsource}}
}
\examples{
m <- matrix(1:60, 6, 10)
k <- matrix(1, 3, 3)
k[2,2] <- 3
k <- k/sum(k)
m2 <- kernelsmooth(m, k)
}
\keyword{ spatial }

