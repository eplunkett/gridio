\name{setwindow, makewindow}
\alias{makewindow}
\alias{setwindow}
\alias{setwindow.grid}
\alias{setwindow.character}
\alias{setwindow.list}
\title{
Functions to make or set a new access window
}
\description{
These function allow users to change the access window
}
\usage{
setwindow(x, ...)
\method{setwindow}{grid}(x, \dots)
\method{setwindow}{list}(x, \dots)
\method{setwindow}{character}(x, \dots)
makewindow(nrow, ncol, xll, yll, cellsize)
}

\arguments{
  \item{x}{Either a path to a grid on disk, a \code{\link{grid}} object, or a list containing the elements: \code{nrow}, \code{ncol}, \code{xll}, \code{yll}, and \code{cellsize}.}
  \item{nrow, ncol}{ the number of rows and column in the access window.}
  \item{xll, yll}{the coordinates of the lower left corner of the lower left cell of the access window. 
}
  \item{cellsize}{the height and width of each cell in map units. (cells must be square.)
}
 \item{\dots}{Arguments to be passed to other methods.}
}
\details{
The access window must match the grids you intend to read and write to and from. Generally it is set by referencing an existing grid on disk or grid object in memory with \code{setwindow}.  However, if you need to create a new window from scratch and do not have an exiting grid you may use \code{makewindow}.
If you are operating in server mode, which is determined by the first call to \code{\link{gridinit}} then you may only set the access window once.  These functions update information stored globaly and invisibly return the same information.  To get details on the current window use \code{\link{getwindow}}.
}
\value{
These functions invisibly return a list specifying the location and extent of the access window. They set the access window to the a list with items:
\item{nrow, ncol }{The number of rows and column in the access window}
\item{xll, yll}{The x and y coordinates of the lower left corner of the lower left cell in the access window.}
\item{cellsize }{The height and width of each cell in map units.}
\item{tileonly}{A logical flag indicating whether the grid can be written and read in one piece (\code{FALSE}) or only in tiles (\code{TRUE})}
}
\author{
Ethan Plunkett
}
\seealso{
\code{\link{gridinit}} must be called prior to this function. 
\code{\link{getwindow}} returns the current window.
\code{\link{readgrid}}, \code{\link{writegrid}}, \code{\link{readblock}},and \code{\link{writeblock}}, \code{\link{tileinit}}, \code{\link{readtile}} ,  \code{\link{writetile}}   

}
\examples{
# setwindow("dem")
# setwindow(grid)
}
\keyword{ spatial }

