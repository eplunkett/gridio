\name{launchbatchstitch and launchbatchmosaic}
\alias{launchbatchstitch}
\alias{launchbatchmosaic}
\title{
functions to launch anthill projects to perform stitching and mosaicing.
}
\description{
These functions launch an anthill project to stitch or mosaic one or more grids or mosaics. The 
default values are for the landscape ecology cluster and the LCD Northeast Region extent. 
}
\usage{
launchbatchstitch(x, owner, reference, host, parallel = FALSE, 
                  name = "batchstitch", priority = 6, ...)
launchbatchmosaic(x, owner, referencemosaic, host, parallel = FALSE, 
                  name = "batchmosaic", priority = 6, ...)

}

\arguments{
 \item{x}{
Paths to to one or more mosaics (for \code{launchbatchstitch}) or grids (for \code{launchbatchmosaic}).
}
 \item{owner}{
This is the owner of the anthill project that is launched. It should be your
name or initials.
}
 \item{reference}{
This is the path to a reference grid with the proper cellsize, extent, and
snapping that the stitched mosaic will align with.  If anthill is loaded and
configured prior to gridio2 than this will be set from anthill's configuration
file.  
}
 \item{referencemosaic}{
This is the path to a reference mosaic with the proper number of panes,
panesize, cellsize, extent, and snapping. If anthill is loaded and configured
prior to gridio2 than this will be set from anthill's configuration file.
}

\item{host}{
This is the host computer on which the stitching or mosaicing should be
performed. This is necessary because ArcPy throws errors when stitching and
mosaicing from network drives. The default is gridio2::.status$arcgishost. If
anthill is loaded and configured prior to gridio2 than this will be set from 
anthill's configuration file.  
 
}
\item{parallel}{
If \code{TRUE} then the stitching or mosaicing will be done in parallel with
one task per grid or mosaic in \code{x}. If \code{FALSE} then the stitching
will be done sequential as a single task. Note, do not use
\code{parallel = TRUE} if the grids or mosaics are in a common directory
(there will be collisions on the info directory and things will get ugly).
Also don't use \code{TRUE} if most of the grids or mosaics already exist and
are up-to-date; if you are primarily verifying it is better not to run in
parallel.
}
 \item{name}{
The project name used on the anthill cluster.
}
\item{priority}{The project's priority on anthill, defaults to 6 to have priority over standard projects.}
 \item{...}{
Other arguments to be passed to \code{\link[anthill]{simple.launch}} such as \code{priority} nd \code{maxthreads}
 }
}
\details{
These functions primarily exist to facilitate stitching and  making mosaics on the landscape ecology cluster where (grid based) conversions can only be done on the raid  server because of ArcPy's problems with networked drives.
}
\value{
Nothing is returned they are called for the side effects of stitching and mosaicing grids on disk. 
An error will be thrown if anything fails.
}
\author{
Ethan Plunkett
}

\seealso{
 These functions launch tasks which call either \code{\link{batchstitch}} or \code{\link{batchmosaic}}.
 If you need to troubleshoot call those functions directly on the proper machine.
}


% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ spatial } 

