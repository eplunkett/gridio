\name{mosaicinfo}
\alias{mosaicinfo}
\title{
Function to read and format the mosaic description file 
}
\description{
A mosaic is a folder containing grids and a mosaic description file. This function when given the path to a mosaic returns a list of the data contained in the accompanying mosaic description file.
}
\usage{
mosaicinfo(path, lock=anthillrunning(), cache = TRUE)
}

\arguments{
  \item{path}{
The path to a mosaic (which is a directory) NOT the path to the mosaic description file.
}
  \item{lock}{
If TRUE a lock is made, the file is read, and the lock is released. If FALSE the file is read without getting or releasing locks.
  }
  \item{cache}{
If \code{TRUE} cached information stored during a prior call to \code{\link{griddescribe}} or \code{mosaicinfo} will be used. If \code{FALSE} then the files will be checked on disk.  
  
  }
  
}
\details{
This function is mostly for internal use but you might find it useful too. 
}
\value{
It returns a list:
\item{name}{the name of the mosaic - usually the same as the last part of the path}
\item{type}{The type of mosaic either 'derived' which indicates it was created from 
a complete grid or 'source' - it was created from scratch. }
\item{timestamp}{the date the mosaic was last edited}
\item{source}{ if type is 'source' this contains the path to the source grid}

\item{sourcetimestamp}{ the modification date of the source grid at the time the mosaic was created
}
\item{extent}{a list with items \code{xll}, \code{yll}, \code{nrow}, \code{ncol}, and \code{cellsize}
  defining the extent of the entire mosaic.
}
\item{rowsize}{
  the number of rows of cells within each pane of the mosaic.
}
\item{colsize}{
  the number of columns of cells within each pane of the mosaic.
}
\item{rows}{
the number of rows of panes in the mosaic
}
\item{cols}{
the number of columns of panes in the mosaic
}
\item{panemap}{
A logical vector indicating whether each pane is included in the mosaic (has data).
Panes are numbered sequentially starting in the northwest with 1 and then moving eastward across
the top row of panes before proceeding with the next row of panes to the south.  The last pane is always in the southeast.  
}
\item{compression}{
A string with value either 'no' or 'compressed'. Normal writes do not create 
optimally compressed mosaics. compressmosaic() will support converting to 
compressed geoTIFFs for more efficient long term storage. This defaults to 'no'
when reading legacy mosaic info (which doesn't include this item).

}
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{writemosaicdesc}}
}
\keyword{spatial}
