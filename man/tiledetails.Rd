\name{tiledetails}
\alias{tiledetails}
\alias{subtiledetails}
\title{
Functions to list the spatial details of the current tile or subtile as defined in the tile scheme and subtilescheme.
}
\description{
These functions return a list that describes the location and extent of the current tile.
}
\usage{
tiledetails(coreonly = TRUE, ts, w)
subtiledetails(coreonly = TRUE)
}
\arguments{
  \item{coreonly}{if set to TRUE (the default) the spatial details will describe the core of the tile it set to FALSE they will describe the tile plus the buffer.}
  \item{ts}{(optional and usually not used) a tilescheme in the same format as returned by \code{\link{tilescheme}}}
  \item{w}{(optional and usually not used) the access window in the same  format  as returned by \code{\link{getwindow}}}
}
\details{
This is intended primarily for internal use.
For \code{tiledetails} normally \code{ts} and \code{w} are ommitted and it returns the details based on the globally defined window and tilescheme. Optionally  these two arguments can be supplied to bipass the global settings. This was added to allow the function to work with \code{\link{makemosaic}(tiff = TRUE)} prior to having a reference mosaic. 
}
\value{
It returns a list:
\item{llx, lly }{The x and y coordinates of the lower left corner of the lower left cell of the tile.}
\item{startrow, startcol}{The row and column in the access window of the first, top-left, northwest, corner of the tile.}
\item{nrow, ncol}{The number of rows and columns in the tile.}
}
\author{
Ethan Plunkett
}
\note{
By default the returned information describe just the core of the tile which is the proper format for use in the
\code{\link{readblock}} and \code{\link{writeblock}} function definitions.
}
\seealso{
For conversion from row and column index to spatial location in map units (x and y) see:  \code{\link{x2c}}, \code{\link{y2r}}, \code{\link{c2x}}, \code{\link{r2y}} 
\code{\link{x2fractionalc}}, and \code{\link{y2fractionalr}}.
Functions \code{\link{readtile}}, and \code{\link{writetile}} rely on this function.
 \code{\link{tileinit}} sets up a tile scheme and \code{\link{tilescheme}}  returns information on the overall scheme.
Functions \code{\link{ntiles}}, and \code{\link{nsubtiles}} list the number of tiles and subtiles in the current tilescheme and tile.
Function \code{\link{stid}} returns the unique id of the current subtile.
}
\examples{

cleanup()
gridinit()
makewindow(1000, 2000, 0, 0, 30)
tileinit(300, 10)
settile(1)
tiledetails() # details for tile core
td <- tiledetails(coreonly=FALSE) # tile with buffer
plotextent(list=td)


subtileinit(50, 10 ,100)
settile(6)
setsubtile(2)
plotextent(list=tiledetails())
plotextent(list=subtiledetails())

}
\keyword{ spatial }
