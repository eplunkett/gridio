\name{gridinit}
\alias{gridinit}
\title{
Function to initialize the grid server
}
\description{
This function sets up a connection to a gridserver  or initializes a local connection. It does not set a reference window unless the server already has one. 
}

\usage{
gridinit(server = NA, port = 3333, log = FALSE, drive=NA)
}

\arguments{
  \item{server}{
If not NA indicates the name of the server to connect to in server mode.  NA indicates a local connection.
}
  \item{port}{
Specifies to the port to be used.  This is only relevant in server mode.   
}
  \item{log}{
if set to \code{TRUE} a log file will be stored on disk.
}

\item{drive}{
If specified the drive (or beginning of the path) that the gridserver is associated with. This allows establishing connections with several gridservers and then connecting on the fly to the gridserver associated with the path (drive) of the grid that is being read or written.
}

}
\details{
Gridinit sets up a connection to a new server or a new local connection. It is only recommended for setting up local connections in which case it will often be followed by a call to \code{\link{setwindow}}.  

It can also be used to set a connection to a server if there isn't a mosaic and there's a single server (no mosaics, one drive).However, \code{refgridinit} is recommended for setting up server connections and required if there's more than one one server.

}
\value{
This function is called for its side effects and returns nothing.
}
\author{
Ethan Plunkett
}
\seealso{
\code{\link{setwindow}} or \code{\link{makewindow}} are usually called right after this.
\code{\link{cleanup}} reverses the effect of this call by deleting all connections.
\code{\link{mosaicinit}} is an analogous function for initiating a set of connections for the panes in a mosaic. 
\code{\link{refgridinit}} to initialize connections based on a reference grid or mosaic and a server table.
}
\examples{
# gridinit() 
}
\keyword{ spatial }
