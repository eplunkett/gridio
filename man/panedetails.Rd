\name{panedetails}
\alias{panedetails}
\title{
function to return information about the extent of a pane
}
\description{
This function returns information about the pane specified by \code{id}. 
}
\usage{
panedetails(id)
}
\arguments{
  \item{id}{
The id of the pane of interest. Panes are identified based on their position in the mosaic.  
Starting at the top left and progressing left to right within rows and then top to bottom by row. 
}
}
\value{
\item{xll, yll, nrow, ncol, cellsize}{the standard extent info}
\item{startrow, startcol}{the row and column in the reference window of the first row and column of the pane}
}
\author{
Ethan Plunkett
}

\keyword{ spatial}

