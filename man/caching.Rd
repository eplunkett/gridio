\name{internal functions that support caching}
\alias{getcacheconfig}
\alias{getcachedir}
\alias{putcachedir}
\alias{getcachepath}
\alias{fileinfo}
\alias{readblockc}

\alias{lookupgrid}

\title{
these functions all support grid caching and are intended for internal use
}
\description{
\code{getcacheconfig} returns the caching configuration information either by reading
 "c:/cache_config.txt" (on the first call) or by returning cached values (on subsequent calls).

\code{putcachedir} and \code{getcachedir} are for reading and writing information on the cached grids themselves. They also handle locking before reading and unlocking after writing. 

\code{getcachepath}  hashes the grid to determine the path to the index file where the grid should be tracked  if grid is a vector it will return a vector of properly hashed grids
  # Argument:
  #   grid : Either a character indicating the path to a grid OR
  #     a numeric with the hash ID that should be looked up.
  #  justhash : FALSE (the default) and the function returns the path to the
  #    cache index file.  TRUE and it returns just the hash.  

\code{fileinfo} returns information about the size of the target, the edit date of the target, and the free space on the disk it resides on.



}
\usage{
fileinfo(x, use.old = FALSE)
getcacheconfig(reread = FALSE)
getcachedir(grid, locking = FALSE)
putcachedir(d, grid, unlock=FALSE)
getcachepath(grid, justhash = FALSE)
lookupgrid(source, d)
readblockc(path, startrow, startcol, nrow, ncol, buffer=0, extent, 
          as.matrix=FALSE, na.value=-9999, as.integer, cacheok, desc)
          
}

\arguments{
  \item{reread}{if TRUE rereading from the file is forced even when the information is cached in memory}
  \item{unlock}{If TRUE a lock on the cachedir will be returned after writing the file.}
  \item{grid}{ Is used to determine which of the index files is used.  It can either be the path to a source grid which will be hashed to determine the file or an integer which will be assumed to be the hash.}
  \item{justhash}{ If TRUE don't return the whole path and instead return just the integer hash relevant to the file.}
  \item{source}{the path to the original (uncached) version of the grid}
  \item{d}{a data.frame with the cache file information}
  \item{x}{the path to a file}
  \item{use.old}{forces use of the native R version of the function instead of the much faster C version}
  \item{lookupgrid}{Called from readblockc, this function returns the row index of the source grid in the cachedata d.}
  \item{path, startrow, startcol, nrow, ncol, buffer, extent, 
          as.matrix, na.value, as.integer, cacheok, desc}{Arguments to readblockc are the same as those to \code{\link{readblock}}. Look there for descriptions.}
\item{locking}{if \code{TRUE} the directory is locked prior to reading and unlocked after, otherwise no additional locks are requested. }


}
\details{
  These functions support caching. None of them are intended as user functions and they may not be exported in the future.
  readgrid, readtile, and readblock are user level functions that will use caching  if it's configured. 
}
\value{

\code{fileinfo} returns a list:
\describe{
\item{size}{the size of the files directly inside of the the dir \code{x} in MB or the size of \code{x} if it is a file. This is not recursive.}
\item{date}{The most recent modification date of files in \code{x} in POSIXct format}
\item{free}{The free space on the disk in MB}
}

\code{getcacheconfig} returns a list:
\describe{
\item{on}{TRUE if caching is on, FALSE otherwise.}
\item{cachepath}{The path to the cache directory.}
\item{maxsize}{the maximum size allowed for the cache directory in GB}
\item{minfree}{the minimum amount of free space on the cache drive in GB}
\item{nbins}{the number of bins to be used by the hashing algorithm and consequently the number of index files used on disk}
\item{deletesize}{the amount of space that will be created in the cache when the cache is too full to add a grid GB.  Should be set much larger than a single grid as deleting is done by \code{\link{friskcache}} which takes a long time. We don't want to do it often.}
}

\code{getcachedir} returns \code{NA} if caching is off and otherwise the cache information, a \code{data.frame} with columns:
\describe{
\item{source}{the path to the non-cached, original grid}
\item{cache}{the path to the cached version of the grid - if caching in place this may be the same as the source}
\item{size}{the status of the grid in the cache either 'ready' or 'copying'}
\item{sourcedate}{the modification date of the source grid}
\item{reads}{the date and time of the last four reads of the cached grid}
\item{size}{the size of the cached grid in MB}
}
\code{putcachedir}{returns nothing}

\code{lookupgrid}{returns the row index of the source grid in the cachedata d where d is a table with the standard gridcache info and source is the path to the source grid that is being cached. If  there are any ready versions of the grid it returns the last ready instance. If no ready versions and one or more copying it returns the last copying instance. Otherwise it return NA.}

\code{readblockc}{returns a grid}


}
\author{
Ethan Plunkett
}
\note{
I have documented these here to facilitate package development.  In the future they may not be part of the public namespace of the package.  
}

\seealso{
\code{\link{makecacheconfig}} creates a new "c:/cache_config.txt" turning caching on in the process.
\code{\link{cacheinit}} initializes a new cachedir - deleting prior contents in the process.
\code{\link{cacheinfo}} prints information about the current state of the cache.
\code{\link{friskcache}} cleans the cache by deleting stuff that shouldn't be there and making sure everything in the index is there.
}
\keyword{spatial}
