\name{gridtable}
\alias{gridtable}
\alias{gridtable.grid}
\alias{gridtable.character}
\alias{pgridtable}
\title{
Function to calculate a table of frequency of occurrances of values in a grid or across grids.
}
\description{
This function calculates a table of values (including NA) and their frequencies of occurrance in a grid either in memory or on disk. If the grid is on disk the calculations are done in tiles if necessary.
}

\usage{
gridtable(x, ...)
\method{gridtable}{grid}(x, digits=NA, ...)
\method{gridtable}{character}(x, tilesize = 2000, tiles, digits=NA, ...)
pgridtable(x, result, owner, name, tilesize = 2000, tiles, cacheok=TRUE, digits=NA, ...)
}
\arguments{
  \item{x}{
	Either a grid object in memory, the path to a grid object on disk, or a vector of paths to grids on disk.  If a vector is supplied than the result will be counts for each unique combination of values from the grids with dimensions for each element in x.
}
  \item{tilesize}{ The tilesize to be used when reading grids in tiles.}
  \item{tiles}{ A vector of specific tiles to run. If omitted all tiles are run.}
  \item{\dots}{Arguments to be passed to other methods.}
  \item{result}{The path to an \file{.Rdata} file where the results will be written.  The file will contains a single object \code{d}.}
  \item{owner, name}{The owner, and name of the project that will be launched on anthill.  }
  \item{cacheok}{Passed to \code{\link{readtile}}. If \code{TRUE}, the default, the grid will be cached prior to reading.}
  \item{digits}{If not NA then the grid is rounded prior to creating the table with the \code{digits} argument passed on to \code{\link{round}}}.
}
\value{
  \code{gridtable} returns an object of class "table", the same class as returned by \code{\link{table}}.  \code{pgridtable} returns nothing but saves an \file{.Rdata} file at the path \code{result} with a single object \code{d} of class "table".
  
}
\author{
Ethan Plunkett
}


\seealso{
\code{\link{tilehist}} calculates histogram data from a grid and is likely to be more useful if the grid is continuous.  
 This function returns the same data structure as \code{\link{table}} which calculates tabular data on more standard objects.
}
\examples{
### Setup a temporary directory with a sample grid in it 

datapath <- system.file("exampledata", package="gridio2")  # grid path to example grid
datapath <- shortPathName(datapath)
datapath <- paste(datapath, "/.", sep="")
dir <- tempdir() # get temporary directory
dir.create(dir)
# Copy example data into temporary directory
file.copy(datapath, dir, recursive=TRUE)
###  Done setting up directory


cleanup() # disconnect from any prior gridservers 
gridinit() # initialize local gridserver 
dem <- paste(dir, "/dem", sep="") # path to dem file.
setwindow(dem) # setwindow to dem file

g <- readgrid(dem)

# Note the dem isn't a great grid for this function because it is continuous
# (but that's my sample data set)
a <- gridtable(g)
b <- gridtable(dem)
print(a)
print(b)
if(!all(a == b))stop("These should be identical")

# a slightly better example
g$m[, ] <- round(g$m/10)* 10
gridtable(g)

### Cleanup
gridkill(dem)
cleanup()

}
\keyword{ spatial }
