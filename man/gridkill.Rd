\name{gridkill}
\alias{gridkill}

\title{
Function to delete grids from disk
}
\description{
this function deletes the grid(s) specified by x from disk.
}
\usage{
gridkill(x)
}

\arguments{
  \item{x}{
a single path or a vector of paths indicating grids to delete.
}
}
\details{
Warnings but not errors will be reported for grids that are not found.  Relative paths will be interpreted relative to the basepath specified when \code{\link{gridinit}}is first called.
} 
\value{
Nothing is returned.
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{gridinit}} and \code{\link{setwindow}} must be called prior to this function.   \code{\link{gridlist}}, and \code{\link{griddescribe}} provide information about grids on disk.
\code{\link{gridcreate}} and \code{\link{gridcopy}} are other functions that manipulate grids on disk.
}
\examples{
#
}
\keyword{ spatial }

