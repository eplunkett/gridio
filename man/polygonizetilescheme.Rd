\name{polygonizetilescheme}
\alias{polygonizetilescheme}

\title{
function to create a SpatialPolygonsDataFrame containing either subtiles or tiles for the current tile scheme
}
\description{
This function returns a SpatialPolygonsDataFrame with the rectangular polygons for each tile or subtile in the tilescheme.  The data consists of a single "ID" columns with the tile or subtile id.

}
\usage{
polygonizetilescheme(subtiles, coreonly = TRUE )
}

\arguments{
  \item{subtiles}{ (optional) defaults to TRUE if subtiles are defined and FALSE otherwise. This determines whether the polygons and the ID column represent tiles or subtiles. 
}
\item{coreonly}{ (optional) If TRUE, the default, then the polygons will only cover the core of each tile. If FALSE then the polygons will also include the buffer. If the tilescheme has a buffer of 0 then TRUE and FALSE will produce the same result.
}
}
\value{
This function returns polygons in a format defined in \pkg{sf}).
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{tileinit}},\code{\link{subtileinit}} set up the tile scheme.
}
\examples{

cleanup()
gridinit()
makewindow(5000, 10000, xll = 0, yll = 0, cellsize = 30)
tileinit(1000, 0)
p <- polygonizetilescheme()
plot(p)

}
\keyword{spatial }

