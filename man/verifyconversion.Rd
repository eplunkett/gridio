\name{verifyconversion}
\alias{verifyconversion}
\title{
function to verify that the contents of a TIFF file match that of an ESRI grid
}
\description{
 This function is to verify whether the data in a grid is preserved in 
  a tif copy. In our testing copying with both gdal_translate and 
  arcpy can result in corrupted no data values. This reads the grid
  with gridio and the tif with terra (backed by gdal) and compares the result.
}
\usage{
verifyconversion(grid, tif, err = FALSE)
}
\arguments{
  \item{grid}{ the path to a grid or grid based mosaic
}
  \item{tif}{
  the path to a tif or tif based mosaic containing the same information as \code{grid} 
}
  \item{err}{
If TRUE throw an error if the grid and tif don't contain the same information. 
}
}

\value{
 Either a list (for single rasters)  or a data.frame (for mosaics) with these items or columns:
 \item{problem}{TRUE if the tif isn't identical to the grid}
 \item{NA.only}{TRUE if the only difference is that \code{NA} (No Data) values in the grid have value in the TIFF.}
  \item{NA.out}{If NA.only is TRUE. This is the value that \code{NA} was converted to.}
In the case where \code{grid} and \code{tif} are mosaics, \code{verifyconversion} will return a data.frame with one row per pane.
}

\author{
Ethan Plunkett
}


\seealso{
\code{\link{convertmosaic}}, \code{\link{convertgrid}}
}
