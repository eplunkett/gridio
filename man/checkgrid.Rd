\name{checkraster}
\alias{checkraster}
\alias{checkgrid}
\alias{checktif}
\title{
functions to verify and diagnose problems with ESRI grids and geoTiff files
}
\description{
These functions tests to see if a raster file is properly stored, named, and formatted for use with gridio2.  

\code{checkgrid} is for ESRI grid files and checks for comon problems with that format including problems with paths that might prevent them from being a valid place to write that format (Set \code{pathonly = TRUE}). 

\code{checktif} is for geoTIFF files and has less to check. 

\code{checkraster} will call either \code{checkgrid} or \code{checktif} depending on whether there's a .tif extenion on x. 

If \code{test = TRUE} all versions will check to see if \code{x} works with \code{\link{griddescribe}} and \code{\link{readblock}}.

}
\usage{
checkraster(x, test = TRUE, pathonly = FALSE)
checktif(x, test = TRUE, pathonly = FALSE)
checkgrid(x, test = TRUE, pathonly = FALSE)
}


\arguments{
  \item{x}{
The path to an ESRI grid or geoTIFF file.
}
  \item{test}{
If \code{FALSE} only preliminary tests will be performed (without using ESRI's API).  If \code{TRUE} the preliminary tests are performed and the grid is described and read from if the prelimanary tests don't reveal problems.  These last tests take more time and are more likely to lead to a hard crash. 
}
\item{pathonly}{ If \code{TRUE} then only the path will be checked. This is called internally by \code{\link{gridcreate}} prior to creating and writing to a raster file and allows much more informative error messages.
}

}
\value{
A list, the name of each item is an error condition and its value is a logical indicating whether that error occurred.  The list will include a subset of the following that depends on the format of x and the arguments. 
  \item{file missing}{(geoTIFF or grid) \code{TRUE}if the grid directory is missing, or if the geoTIFF .tif file is missing.}
  \item{parent directory missing)}{(geoTIFF or grid) \code{TRUE}  if the directory that holds the raster file is missing.}
  \item{name too long}{(grid) \code{TRUE} if the name of the grid exceeds 13 character}
  \item{is multitiled}{(grid) \code{TRUE} (grid) if the grid is a multitiled grid (see note below)}
  \item{illegal charater in name}{(geoTIFF or grid) \code{TRUE} Grids are limited to alpha, numeric, '_', and '-'. For geoTIFF files we exlcude  * : ? " < > |  (which are illegal characters in most file systems.)}
  \item{space in path}{ (grid) \code{TRUE} if there is a space anywhere in the grid path}
  \item{path too long}{ (grid) \code{TRUE} if the full path is longer than 128 characters (see note below)}
  \item{path dangerously long}{(grid) \code{TRUE} if the full path is longer than 115 characters (see note below)}
  \item{info dir missing}{(grid)\code{TRUE} if there is no info directory associated with the grid}
  \item{component files missing}{(grid)\code{TRUE} if the grid directory doesn't contain the standard, minimal files: \file{dblbnd.adf}, \file{hdr.adf}, \file{sta.adf}, \file{w001001.adf}, and \file{w001001x.adf}.}

The following tests will only be performed and returned if \code{test} is \code{TRUE}, \code{pathonly} is \code{FALSE}, and all the relevant preceding tests have passed.
  \item{failed describe}{(geoTIFF or grid) \code{TRUE} if \code{\link{griddescribe}} reports an error}
  \item{grid describe reports missing grid}{(geoTIFF or grid) \code{TRUE} indicates that \code{\link{griddescribe}} (and thus ESRI's API) report that there is no grid at path \code{x}}
  \item{failed read test}{(geoTIFF or grid) \code{TRUE} indicates that reading the grid \code{x} produced an error}
)
This last item will always be present:
\item{problem}{(geoTIFF or grid) \code{TRUE} if any of the tests reported problems and \code{FALSE} if all tests indicate the rasterfile is OK}

} 

\author{
Ethan Plunkett
}
\note{

ESRI grid names are restricted to 13 characters and the full path is restricted to 128 characters.  See: 
\url{https://desktop.arcgis.com/en/arcmap/latest/extensions/spatial-analyst/performing-analysis/output-raster-formats-and-names.htm}
With networked drive the full (not the mapped) path to the drive seems to count against the limit thus I test both for a path longer than 128 (too long) and longer than 115 which is likely to be problematic if you are using a network drive.  Ideally, I would resolve the full name of mapped drives to test, but currently I do not.

ESRI grids always have at a minimum these components: \file{dblbnd.adf}, \file{hdr.adf}, 
\file{sta.adf}, \file{w001001.adf}, and \file{w001001x.adf}.  See:
\url{http://www.digitalpreservation.gov/formats/fdd/fdd000281.shtml}

ESRI grids can be "multitiled" in which case the API gridio (1) relied on will
not work.  Multitiled grids have multiple files of the format \file{wXXXXXX.adf} (where the X is numeric).  Generally, this occurs with larger grid file sizes and thus is effected both by the number of cells in the grid and the storage type of the grid. See:
\url{http://www.digitalpreservation.gov/formats/fdd/fdd000281.shtml}
\url{https://trac.osgeo.org/gdal/ticket/1198}

geoTIFFs are much less constrained than ESRI grids.  Right now this funciton checks whether the tif file exists and then if 
\code{test = TRUE} checks whether it be described and read.

These functions are used to return more useful errors when \code{\link{readblock}}, \code{\link{writeblock}}, and \code{\link{gridcreate}} fail. They are also used by \code{\link{friskcache}} to check raster files in the cache.

}


\seealso{
  \code{\link{ismultitiled}} checks to see if a grid is multitiled (which is also checked by this function).
  \code{\link{makemosaicpy}} can be used to make a gridio (not ESRI) mosaic from a grid which is one way of getting around ESRI's internal tiling (multitiled grids) so that they can be used with gridio.
  \code{\link{griddescribe}} is the standard way of determining geoTIFF or grid attributes.  
  \code{\link{rasterinfo}} uses gdalinfo to return very detailed information about any raster file.
}
\keyword{ spatial}

