\name{getfriskcachedetails}
\alias{getfriskcachedetails}

\title{
function to read and filter the friskcachedetails.log file
}
\description{
The function reads, formats, and filters the contents of the friskcachedetails.log file to facilitate debugging of grid caching.
}
\usage{
getfriskcachedetails(system = "all", computers = "all", 
                     consolidate.purged = TRUE, max.days = NA, path)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{system}{
  should be one of "R", "APL", or "All" (case insensitive) to indicate the filterning based on system.
}
  \item{computers}{
  should be either "all" or a vector of computer short names to indicate the filtering based on computer.
}
  \item{consolidate.purged}{
  if TRUE (the default) all purged lines created by a single cachefrisk will be replaced by a single line.
}
  \item{max.days}{
  if not NA then only records from the most recent max.days will be returned.  Fractional day differences are calculated and not rounded.
}
  \item{path}{
  path to anthill directory.  Typically not needed.
}
}
\value{
A dataframe based on the friskcachedetails.log file structure with columns date, computer, system, cachepath, sourcepath, and message.
}
\author{
Ethan PLunkett
}

