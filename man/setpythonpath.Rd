\name{pythonpath}
\alias{setpythonpath}
\alias{getpythonpath}
\title{

functions to set and retreive the path to ESRI's installed version of python
}
\description{
This  function controls where gridio2 expects to find python.
If you are using  anthill as well as gridio2 use the anthill configure file to
change the default python path.  Otherwise you may run this at the start of 
a session if the default path doesn't work for you. 
}
\usage{
setpythonpath(arcgisversion, pythonpath)
getpythonpath()
}

\arguments{
  \item{arcgisversion}{
The arc gis version as as a string with a single decimal (major and minor but not patch)  eg: "10.7"
}
  \item{pythonpath}{
(optional)  The path to python.exe associated with the ArcGIS installation;  with standard installations of ArcGIS this shouldn't need to be set, and will  instead be  determined  from the version.
}
}
\details{
This is called by onload  using  just the arcgisversion argument which defaults 
to one of two things. If Anthill is loaded and configured it will use the 
arcgisversion set in Anthill's config file, otherwise it will use the hardwired default  in this
package set by status.R

Currently this is only used by \code{\link{makemosaicpy}} and \code{\link{stitchpy}}


}
\value{
setpythonpath does not return anything but sets the .status$arcgisversion  and .status$pythonpath.
getpythonpath returns the currently configured path to ArcGIS's python installation.
}

