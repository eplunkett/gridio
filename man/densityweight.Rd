\name{densityweight}
\alias{densityweight}
\title{
function to calculate weights for observations based on their spatial density
}
\description{
This function calculates a weight for each observation in a dataset that is equal to 1 over the number of observations occurring in the same raster cell that the observation occurs in.
}
\usage{
densityweight(x, cellsize, make.grid = TRUE)
}
\arguments{
  \item{x}{
a dataframe containing the data for which weights are to be calculated.  It must have x and y columns containing projected coordinates of the observations.
}
  \item{cellsize}{
the cellsize of the grid used to bin the data and calculate the weights.
}
  \item{make.grid}{
If TRUE a grid covering the full extent of the data with the specified cellsize will be returned. Each cell will contain a count of the observations that fell within it.
}
}

\value{
   \item{data}{A dataframe with the same data as input with an additional column \code{"weights"} containing the calculated weights}
   \item{grid}{A grid covering the extent of the data with the specified cellsize in which each cell contains a count of the observations that fell within it. 
Only returned if \code{make.grid} is \code{TRUE}. }
}
\author{
Ethan Plunkett
}
\note{
The lower left corner is always situated such that it is a multiple of the cellsize.
}


\examples{

# Make bogus data
n <- 1000
cellsize <- 30
xmin <- 1000
xmax <- 2000
ymin  <- 4000
ymax <- 5000
d <- data.frame(x=runif(n, xmin, xmax), y=runif(n, ymin, ymax), d=runif(n))
cellsize <- 30

# Call function 
res <- densityweight(d, cellsize)
d <- res$data
g <- res$grid

\dontrun{
# Plot the grid and the points (the size of the points increases with increasing weight)
plot(g)
with(d, points(x=x, y=y, cex=.2 + weight))

# Save to disk
 gridinit()
 setwindow(g)
 writegrid(g, "C:/mygridname")
}
}
\keyword{ spatial}
