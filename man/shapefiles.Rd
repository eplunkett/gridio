\name{shapefiles, writeshape, readshape}
\alias{shapefiles} 
\alias{writeshape} 
\alias{readshape} 

\title{
functions to write and read shapefiles
}
\description{
These functions are convenience wrappers, to functions in \pkg{sf}. 


}
\usage{
writeshape(obj, file, ...)
readshape(file, ...)
}

\arguments{
  \item{obj}{
A vector GIS object. For new code this should be an object from \pkg{sf}.
}
  \item{file}{
The full path to the shapefile to be read or written. The ".shp" extension was
optional but is now recommended.  If missing it will be added with a warning.
}
  \item{\dots}{
Other arguments to be passed to \code{\link[sf]{st_write}} and \code{\link[sf]{st_read}}. These functions both supply  \code{dsn} and \code{layer} arguments.  \code{writeshape} also supplies the \code{obj} and \code{driver} arguments. 
}
}
\value{
\code{writeshape} doesn't return anything.  \code{readshape} returns an object of a class defined in the \pkg{sf} package.  
}
\author{
Ethan Plunkett
}
\examples{


# Make SpatialPointsDataFrame
pts <- data.frame(x= runif(20, 1, 1000), y =runif(20, 1000, 2000), a=1:20)
coordinates(pts) <- c("x", "y")

file <- tempfile()
writeshape(pts, file=file)  #  write to a shapefile
a <- readshape(file)        # read the shapefile

file.remove(paste(file, c(".shp", ".dbf", ".shx" ), sep=""))  # cleanup


}

\seealso{
 \code{\link[sf]{st_write}} and \code{\link[sf]{st_read}}
}

\keyword{ spatial}

