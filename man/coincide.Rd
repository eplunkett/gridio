\name{coincide}
\alias{coincide}
\title{
A function to test if two grid (or similar) objects are coincident
}
\description{
This function tests to make sure that x and y are of the same extent, cellsize, and grid alignment: 
that all the cells of x line up perfectly with a cell of y.  Its not necessary that x and y are grid 
objects as long as all the components of a grid object that define it's spatial alignment are present
(xll, yll, nrow, ncol, and cellsize). 
}
\usage{
coincide(x, y)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x, y}{
 grids or other list objects with elements xll, yll, nrow, ncol, and cellsize such as the objects
 returned from griddescribe and tiledetails.
}
}
\value{
\code{TRUE} if x and y are coincident, \code{FALSE} otherwise.
}
\author{
Ethan Plunkett
}
\note{
The function uses \code{\link{all.equal}} to check if the grids have nearly the same dimensions, 
cellsize, and lower left corner. 
}
\seealso{
\code{\link{griddescribe}},  \code{\link{tiledetails}}, and \code{\link{getwindow}} all return
objects that are suitable for use with this function (as is of course any \code{\link{grid}} object).
\code{\link{checkgridalignment}} is a wrapper that accepts paths to multiple grids and returns the paths of grids that don't coincide.

\code{\link{checkcellalignment}} checks to see if the cell alignment is the same but does not check for matching extent.

}
\examples{


### Setup a temporary directory with a sample grid in it 
# Get path to example dem grid (included with package)

datapath <- system.file("exampledata", package="gridio2")  # grid path to example grid
datapath <- shortPathName(datapath)
datapath <- paste(datapath, "/.", sep="")
dir <- tempdir() # get temporary directory
dir.create(dir)
# Copy example data into temporary directory
file.copy(datapath, dir, recursive=TRUE)
# system(paste("open", dir)) # if you want to look at contents of temporary directory
cleanup() # disconnect from any prior gridservers 
gridinit() # initialize local gridserver 
dem <- paste(dir, "/dem", sep="")
setwindow(dem) # setwindow to dem file
###  Done setting up directory

g <- readgrid(dem)
f <- crop(g, 5)

coincide(g, f) # FALSE 
coincide(g, getwindow()) # TRUE

# Same thing but produce errors if results aren't as expected 
stopifnot(coincide(g, getwindow())) 

# Cleanup 
gridkill(dem)  # delete grids
cleanup()


}
\keyword{spatial}

