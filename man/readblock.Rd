\name{read and write arbitrary blocks}
\alias{readblock}
\alias{writeblock}
\alias{writeblock.grid}
\alias{writeblock.matrix}
\title{
Functions to read and write arbitrary blocks of grids
}
\description{
These functions read and write arbitrary rectangular blocks of grids.  While writing NA values can be treated as transparent or not, and the contents of the grid can optionally be added to the grid on disk.
}
\usage{
readblock(path, startrow, startcol, nrow, ncol, buffer=0, extent, 
          as.matrix = FALSE, na.value = -9999, as.integer, cacheok = FALSE, desc)

writeblock(x, path, buffer=0, transparent = FALSE, add = FALSE, as.integer, 
           na.value = -9999, as.mosaic, desc)
}

\arguments{
  \item{x}{either a grid or a matrix containing the data to be written}

  \item{path}{
the name and path of the grid to read or write from.
}
  \item{startrow, startcol}{
The index in the access window of the first row and column to write to.
}

  \item{nrow, ncol}{
The number of rows and columns in the grid.
}

\item{extent}{a list with the elements xll, yll, nrow, ncol, and cellsize. This allows specifying the extent to read by using an existing grid or the result from a call to tiledetails, griddescribe, or gridintersect.}
  
  \item{buffer}{
This many additional cells will be included around the outside of the block when reading and deleted from the outside of the block prior to writing.
}
  
  \item{as.matrix}{
If set to \code{TRUE} the function will return a matrix rather than a grid.
}
  \item{na.value}{
This value is used to represent \code{NA} values in communications with the underlying C code.  These values are converted back into \code{NA} in the objects \code{readblock} returns and in the grids that \code{writeblock} writes.  The only reason to set this to something other than the default -9999 is if there are legitimate (non-NA) -9999 values in some cells in your grid.
}

 \item{transparent}{If \code{TRUE} \code{NA} values will be skipped and existing values for those cells will be retained in the grid. Defaults to \code{FALSE}.}
  
  \item{add}{If TRUE non-NA values will be added to the existing grid. Defaults to \code{FALSE} }
  
  \item{as.integer}{Logical TRUE indicates that the grid is (or should be created as) an integer grid FALSE indicates real.
  In readgrid \code{as.integer} is optional but if supplied the function won't first check the type
  of the grid on disk and will instead assume that the argument correctly indicates the type of grid
  on disk. This is quicker but will produce an error if wrong. 
  In writegrid \code{as.integer} is only used if the grid specified by \code{path} does not exist in
  which case this arguments controls the type of the grid that is created; either integer or real depending on whether the argument is \code{TRUE} or \code{FALSE} }
 
 \item{as.mosaic}{Logical, \code{TRUE} indicates that if a new grid needs to be created than it should be
 a mosaic grid. \code{FALSE} indicates standard grid output. This argument is optional.  It will default to \code{TRUE} when mosaicinit has been called. 
 }
 \item{desc}{The result of griddescribe on the target grid, \code{x}. This is intended for internal use. 
 }

\item{cacheok}{if TRUE than this grid is eligible for caching (if caching is configured). See \code{\link{cacheinit}}.
}

}
\value{
\code{readblock} returns either a \code{\link{grid}} or matrix with the contents of the block depending on the value of the \code{as.matrix}  argument.
\code{writeblock} does not return anything.
}
\author{
Ethan Plunkett
}

\seealso{
 \code{\link{readtile}}, \code{\link{writetile}}, \code{\link{readgrid}}, and \code{\link{writegrid}}
}
\examples{
#
#


}
\keyword{ spatial }
