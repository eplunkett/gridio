
\name{refgridinit}
\alias{refgridinit}

\title{
Function to initialize connections with gridservers based on a reference grid or mosaic.
}
\description{
This function initializes connection(s) with gridservers that match the reference grid or mosaic.  If ref is a mosaic and both is TRUE it will attempt to connect to gridservers for each pane of the mosaic as well as a gridserver that covers the entire extent.

}
\usage{
refgridinit(ref, both = FALSE, servers, desc)
}

\arguments{
  \item{ref}{
The path to either a reference grid or a reference mosaic that defines the extent and mosaic scheme that the gridserver(s) need to be able to read and write from.
}
  \item{both}{
If \code{both} is \code{TRUE} and \code{ref} is the path to a mosaic then an attempt will be made to connect to a server that matches the entire extent as well as the servers for each pane of the mosaic.  Otherwise only servers for the panes will be initialized.
 }
  \item{servers}{
(optional) a list of gridservers as returned by \code{\link[anthill]{readgridservers}} in \pkg{anthill}.  If not supplied then \code{\link[anthill]{readgridservers}} will be called if anthill is running.  If \code{gridservers} is omitted and anthill is not running an error will be thrown.
}
\item{desc}{ The description of the reference grid (as returned by griddescribe). This is optional and intended for use within parent functions that have already called griddescribe to reduce calls to griddescribe.
}
}
\value{
This function returns nothing. It is called for the side effect of initializing connections with gridservers.
}
\author{
Ethan Plunkett
}
\note{
This function does not initialize local connections.  Use \code{\link{gridinit}} and \code{\link{mosaicinit}} for that. 
}
\seealso{
\code{\link{gridinit}}, \code{\link{mosaicinit}}
}
\keyword{ spatial}

