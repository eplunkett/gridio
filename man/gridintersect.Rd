\name{gridintersect}
\alias{gridintersect}
\title{
function to find the intersection of two grid extents
}
\description{
this function identifies the extent of overlap of two grids or extents (named lists with xll, yll, nrow, ncol, cellsize items.)  
}
\usage{
gridintersect(x, y)
}
\arguments{
  \item{x, y}{ Either a grid in memory, the path to a grid on disk, or a list containing the named 
  elements xll, yll, nrow, ncol, and cellsize.  
  }
}

\value{
If \code{x} is a grid object in memory this function returns a grid with the contents taken from x and 
an extent matching the region of overlap between \code{x} and \code{y} otherwise the function returns a 
named list defining the region of overlap with the elements \code{xll}, \code{yll}, \code{nrow}, \code{ncol}, \code{cellsize}.
}
\author{
Ethan Plunkett
}

\seealso{
\code{\link{matchextent}}, \code{\link{clip}}, and \code{\link{crop}}
}
\examples{

  # Without data
  x <- list(xll=0, yll=0, nrow=10, ncol=15, cellsize=10)
  y <- list(xll=50, yll=20, nrow=5, ncol=40, cellsize=10)
  gridintersect(x, y)  
  
  # With Data
  x$m <- matrix(1:150, nrow=10, ncol=15)
  class(x) <- c("grid", class(x))  
  i <- gridintersect(x, y)
  x$m
  i$m  
  
}
\keyword{ spatial }
