\name{overlay}
\alias{overlay}
\alias{overlay.grid}

\title{
function to overlay data from one grid onto another
}
\description{
this function takes the data from a source grid (\code{x} and overlays it on a destination grid (\code{y}).  The source data can be added to the destination with add=TRUE.  If add is FALSE than transparent=TRUE will only overlay non-NA cells; and transparent=FALSE will overlay NA values in x on y.  
}
\usage{
\method{overlay}{grid}(x, y, transparent = TRUE, add = FALSE, coreonly = TRUE, buffer=0, ...)
}
\arguments{
  \item{x}{ A grid object containing the source data.

}
  \item{y}{ A grid object onto which \code{x} will be overlaid.
}
  \item{transparent}{
If \code{TRUE} then NA values in \code{x} will not be transferred to \code{y}. If \code{FALSE} then NA values in \code{x} will overwrite any pre-existing data in corresponding cells of \code{y}.  \code{transparent} is only relevant if \code{add} is \code{FALSE}.
}
  \item{add}{
If \code{TRUE} then the values in \code{x} are added to the values in corresponding cells of \code{y}.  NA values are removed in the process. If \code{add} is \code{TRUE} then \code{transparent} is ignored.
}
\item{coreonly}{
This is intended for use when working with \code{\link{subtiles}}. If \code{coreonly} is \code{TRUE} then a buffer 
of cells is removed from out outside edge of \code{x}.  The width of the buffer is taken from  \code{\link{tilescheme}} prior to overlaying on \code{y}. If \code{coreonly} is \code{TRUE} then the \code{buffer} argument is ignored.
}
\item{buffer}{An integer number of cells to be removed from the edge of \code{x} prior to overlaying on
\code{y}.

}
\item{...}{ Theoretically this is for additional arguments to be passed to other functions.  They 
 are not used by this functions but are required to be present as arguments in the definition to be 
 consistent with the generic method.  Ignore them.}
}
\details{
\code{x} and \code{y} need not coincide but they must have the same grid snapping and at least partially overlap (some cells in x must perfectly overlap some cells in y). 
}
\value{
a grid that coincides with y containing a data from x overlaid on data from y. 
}
\author{
Ethan Plunkett
}
\note{
This was written to facilitate processing in subtiles.  In particular it is used to get results data for a subtile into a tile. 
}

\seealso{
  \code{\link{matchextent}} is similar but only retains data from x.  \code{\link{subtileinit}} is used to initialize processing in subtiles. \code{\link{extractsubtile}} is used to extract a subtile from a tile. 
}
\examples{
# overlay()

?subtiles # for example code

}
\keyword{ spatial }

