\name{clipna}
\alias{clipna}

\title{
Function to identify unnecessary NA cells around the edge of a matrix 
}
\description{
This function returns coordinates describing the smallest rectangle that contains all of the non-NA cells in the grid. If \code{expand} is set this minimal box is expanded by \code{expand} cells.
}
\usage{
clipna(matrix, expand = 0, as.integer = FALSE)
}
\arguments{
  \item{matrix}{
A matrix of numeric and NA cells.
}
  \item{expand}{
\code{expand} sets how many extra cells to include beyond the non-NA cells. 
}
  \item{as.integer}{
if TRUE the matrix is passed to the underlying C code as an integer matrix.
}
}
\value{
Returns a named numeric vector with four elements: "r1", "r2", "c1", "c2" corresponding to the first and last rows and columns of the data portion of the matrix.
}
	
\author{
Ethan Plunkett
}
\note{
I have included this function because it was defined in the .dll.  I suspect that it might not save much time to do the calculation via the .dll.
}
\examples{
#
}
\keyword{ spatial }
