\name{as.grid and rast}
\alias{as.grid}
\alias{as.grid.matrix}
\alias{rast,grid-method}
\alias{as.grid,SpatRaster-method}
\title{Functions to convert matrices and SpatRast objects to grids.}

\description{
Make grids from objects of type: matrix and SpatRaster (\pkg{terra}), 
}
\usage{
as.grid(x, ...)   # generic
\method{as.grid}{matrix}(x, xll, yll, cellsize, ...)
\method{as.grid}{SpatRaster}(x, ...)
\S4method{rast}{grid}(x)


}
\arguments{
  \item{x}{an object of class \code{matrix}, \code{SpatRast} or \code{grid} (for \code{rast()}).}
  
  \item{xll, yll}{the x and y coordinates of the lower left corner of the lower left cell of the grid.}
  \item{cellsize}{the cellsize of the grid}
  \item{...}{Arguments to be passed to other methods.}
}

\value{
An object of class \code{grid} which is a list containing the following elements.
  \item{m}{a matrix containing the data from \code{data.col}. Rows progress from north to south and columns from east to west.}
  \item{nrow, ncol}{the number of rows and column in \code{m}. }
  \item{xll, yll}{ the x an y coordinates of the lower left corner of the lower left cell in the matrix.}
  \item{cellsize}{ the horizontal and vertical dimension of cells in the grid; cells must be square.}
}

\examples{

# Matrix method
mat <- matrix(1:20, 4, 5)
g <- as.grid(mat, cellsize=10, xll=0, yll=0)
g

}

