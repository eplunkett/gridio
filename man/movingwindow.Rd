\name{movingwindow}
\alias{movingwindow}
\alias{movingwindow.character}
\alias{movingwindow.grid}
\title{
function to generate a grid where each cell's value is the result of applying a function to all cells within a window centered on the cell
}
\description{
This function extracts a matrix centered on each cell in the grid and applies FUN to that matrix.  Other arguments can be passed to FUN with ...  
}
\usage{
movingwindow(x, radius, FUN, ...)
\method{movingwindow}{character}(x, radius, FUN, outgrid, tilesize, 
        overwrite = FALSE, tiles, cacheok, as.integer = FALSE, ...)
\method{movingwindow}{grid}(x, radius, FUN, buffer = 0, ...)
}

\arguments{
  \item{x}{
A grid object (in memory) or the path to a grid on disk.
}
  \item{radius}{
The radius of the window in cells (beyond the focal cell).  A radius of 2 will result in a 5 x 5 window.
}
  \item{FUN}{
A function to apply to windows in x.
}
  \item{outgrid}{
The path where a grid should be created.
}
  \item{tilesize}{
  The tilesize that should be used to process the grid.  Defaults to 2000.
}
  \item{overwrite}{
Indicates whether a grid at outgrid should be overwritten.  If FALSE an error will be thrown if the outgrid already exists.
}
  \item{tiles}{
Allows specifying a subset of tiles to run.  This saves processing time if you have an irregular landscape.  You must also specify tilesize if using the tiles argument.

}
  \item{cacheok}{
If TRUE then copy the grid to the local gridcache before reading.  (Requires anthill to be running).
}
  \item{as.integer}{
If TRUE the output grid will be an integer grid.  If FALSE (the default) it will be a floating point 
grid.}

\item{buffer}{don't bother calculating values for cells in a band this wide (in cells) around the edge this is used when processing in tiles to save some computation time (b/c only the core will be used)}

  \item{\dots}{
Other arguments to pass to \code{FUN}. For example often you will porbably want to use \code{na.rm = TRUE} here.
}
}
\details{
Currently the analysis is done on square windows.  I could add an option that would allow for either round windows or arbitrarily shaped windows (by passing a matrix to use as a mask within each window) these would probably mean that the first argument we pass to \code{FUN} would be a vector (currently it is a matrix).  If either of those options would be useful to you let me know. 
}
\value{
The grid method returns a grid where each cell is the value of the function applied to a window 
around the corresponding cell in the input grid.
The character method produces a grid on disk and returns nothing.

}
\author{
Ethan Plunkett  
}

\seealso{
\code{\link{gaussiansmooth}} and \code{\link{kernelsmooth}} allow replacing each cell with a weighted mean of nearby cells. The first uses gaussian weights while the second allows for any weighting scheme.  Thus \code{\link{kernelsmooth}} can be used to calculate a moving window average with round or square windows as well and with really large kernels using \code{\link{kernelsmooth.character}} with upscaling may be quicker than using this function to calculate a movingwindow mean.
}

\examples{

 ### Setup a temporary directory with a sample grid in it 
  # Get path to example dem grid (included with package)
  
  datapath <- system.file("exampledata", package="gridio2")  # example tif
  datapath <- shortPathName(datapath)
  datapath <- paste(datapath, "/.", sep="")
  
  # Make a temporary directory to write new grids to
  dir <- tempdir() # get temporary directory
  if(!file.exists(dir)) dir.create(dir)
  
  # Copy example data into temporary directory
  file.copy(datapath, dir, recursive=TRUE)
  ###  Done setting up directory
  
  # Setup paths
  dem <- paste(dir, "/dem", sep="") # path to dem file.
  mwsd <- paste(dir, "/sd", sep="") # path to dem file.
  
  # initialize
  cleanup() # disconnect from any prior gridservers 
  gridinit()
  setwindow(dem) # setwindow to dem file
  
  grid <- readgrid(dem)
  
  
  ### Example usage:
  
  # Calculate a moving window mean on a grid in memory
  gmean <- movingwindow( grid, radius = 10,  FUN = mean, na.rm = TRUE)
  
  # Calculate a moving window sd on a grid on disk
  # Note: 300 is a silly small tilesize 2000 would make more sense.
  movingwindow(x = dem, radius = 10, FUN = sd, na.rm = TRUE, outgrid = mwsd,
               overwrite = TRUE, tilesize = 300)  
  gsd  <- readgrid(mwsd)
  

}
\keyword{ spatial }

