\name{istif}
\alias{istif}

\title{
function to determine if a path has a geoTIFF extension (.tif .tiff)}
\description{
Returns TRUE if x is the path to a geoTIFF FALSE otherwise. Doesn't check anything on disk.
}
\usage{
istif(x)
}
\arguments{
  \item{x}{
A (character) path to a raster file. 
}
}
\details{
This is intended mainly for internal use. In making the transition to geoTIFFs from grid files there are lots of places where we check to see if a path is a tif or not.
}
\value{
TRUE if x is a path to a geoTIFF FALSE otherwise. 
}


\examples{
istif("C:/data/dem.tif")
istif("C:/data/dem")
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory (show via RShowDoc("KEYWORDS")):
% \keyword{ ~kwd1 }
% \keyword{ ~kwd2 }
% Use only one keyword per line.
% For non-standard keywords, use \concept instead of \keyword:
% \concept{ ~cpt1 }
% \concept{ ~cpt2 }
% Use only one concept per line.
