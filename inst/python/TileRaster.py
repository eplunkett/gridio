#TileRaster.py
#J. Grand, 11/07/13
# Edited E. Plunkett
#Clips large grid into smaller tiles specified by a shapefile 

import sys,arcpy,os
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")

#parameters
destination = "[newmosaic]"
inputRaster = "[input]"
inputShapefile = "[shapefile]"
refGrid = "[referencegrid]"
basename = "[basename]"  # this is used for subdirectories if the grid name is long or there are many tiles the basename will be truncated version of the name 
tileList = [[tilelist]]

# eg : tileList = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20"]

#environment settings
arcpy.env.workspace = destination
arcpy.env.snapRaster = refGrid
arcpy.env.outputCoordinateSystem = refGrid
arcpy.env.overwriteOutput = True

#make mosaic directory
mosaicPath = destination 
if not os.path.isdir(mosaicPath):
    os.mkdir(mosaicPath)

#clip raster by each tile in shapefile and store in separate folder inside mosaic directory
arcpy.MakeFeatureLayer_management(inputShapefile,"tilelyr")

for tile in tileList:
    tilePath = mosaicPath + "/" + basename + tile
    if not os.path.isdir(tilePath):
        os.mkdir(tilePath)
    arcpy.SelectLayerByAttribute_management("tilelyr","NEW_SELECTION",'"Tile" =' + "'" + tile + "'") 
    arcpy.Clip_management(inputRaster,"#",tilePath + "/" + basename + tile,"tilelyr","#","ClippingGeometry")

open(destination + "/pycompleted", 'a').close()  #  so that R can verify the script completed
