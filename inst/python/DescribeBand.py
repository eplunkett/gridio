
import arcpy


file = "[file]"
complete = "[complete]"
desc = arcpy.Describe(file  + "/Band_[band]")

print "Integer Raster: %s" % desc.isInteger
print "No Data Value: %s" % desc.noDataValue
print "Pixel Type: %s"  %  desc.pixelType

open(complete, 'a').close()  #  so that R can verify the script completed
