# Copy raster template script

import arcpy
arcpy.env.workspace = r"[workspace]"

source = "[source]"
destination = "[destination]"
complete = "[complete]"
arcpy.CopyRaster_management(source, destination)
open(complete, 'a').close()  #  so that R can verify the script completed

