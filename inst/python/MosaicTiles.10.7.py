#MakeMosaic.py
#J. Grand, 11/07/13
#mosaicks raster tiles into one raster
# Edited 12/17/2013 E. Plunkett
# Edited 6/5/2017 E. Plunkett : Updated logic on what to do with mixed types (old logic failed)
# Edited 3/9/2020 E/ Plunkett: changed work flow to work with ArcGIS 10.7
#  added logical control [forcetoextent]  which if set to 1 will 
# perform grid algebra by adding the reference grid * 0 to the output raster
#  this was the only way I could find to force the extent to expand to 
# the full reference when the mosaic was missing a  whole row of panes.
#  I'm now setting this to TRUE only when a complete first or last column  or row
# isn't used in the mosaic.
#


import arcpy, sys,os
from arcpy import env
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")

#parameters
outDir = "[outdir]"
refGrid = "[refgrid]"
tempName = "temp_stitch"  #  Used to write intermediate if forceToExtent is True
outName = "[outname]"

tilePaths = [[tiles]]
inputBaseDir = "[inmosaic]"
gdbDir = inputBaseDir  # the directory the geodatabase is created within
gdbName = "GDB.gdb" # the geodatabase name
inputName = "[inputname]"
forceToExtent = [forcetoextent] # if True add 0 * reference grid  to the result
#  This is necessary because neither clip_managment  nor projectRaster_management
# nor mosaic to new raster seem to honor the extent enviromental variable
#  when the extent is bigger than the data.  I'm setting this to TRUE if
# there isn't a  pane ineach of the first and last column; and top and
# bottom row

forceToExtent = bool(forceToExtent)

buildPyramids = [pyramids]
pyramidEnvString = "[pyramidstring]"


verbose = False  # Set to TRUE for debugging
# Find the pixelType string associated with the panes of the mosaic and promote pixel type
#  if multiple types are present
first = 1
unsigned = 0
signed = 0
floating = 0
maxType = 0
multipleTypes = 0
typeFound = [0] * 11  # Array to track occurances of types
for tile in tilePaths:
    thisTypeResult = arcpy.GetRasterProperties_management(tile, "VALUETYPE")
    thisType = int(thisTypeResult.getOutput(0))
    if(verbose):
      print(thisType)
    typeFound[thisType] = 1
    maxType = max(maxType, thisType)
    if first == 1:
        type = thisType
        first = 0
    else:
        if type != thisType:
            multipleTypes = 1
    if type == 3 or type == 5 or type == 7:
        unsigned = 1
    if type <=2:
        signed = 1
    if type == 4 or type == 6 or type == 8:
        signed = 1
    if type == 9 or type == 10:
        floating = 1


type = maxType

if(verbose):
    print("typeFound:")
    print(typeFound)
    print("maxType:")
    print(maxType)
    print("multipleTypes")
    print(multipleTypes)

    print("signed")
    print(signed)

    print("unsigned")
    print(unsigned)

if multipleTypes == 1:
    if(verbose):
       print("Executing outer if")
    if floating == 1:
        if unsigned == 1 or signed == 1:
            print("mixed floating point and integer panes")
            sys.exit("mixed floating point and integer panes")
    if(verbose):
       print("Multiple types")
    if maxType == 3:
        type = 3
    if maxType == 4:
        type == 6
    if maxType == 5:
        if bool(typeFound[4]):
            type = 8
        else:
            type = 5
    if maxType == 6:
        if bool(typeFound[5]):
            type = 8
        else:
            type = 6
    if maxType == 7: #
        if bool(typeFound[6]) or bool(typeFound[4]):
            print("Data contains 32 bit unsigned integer and signed integers; no valid pixel type")
            sys.exit("Data contains 32 bit unsigned integer and signed integers; no valid pixel type")
        else:
            type = 7
    if maxType == 8:
        if bool(typeFound[7]):
            print("Data contains 32 bit signed integer and unsigned integer; no valid pixel type")
            sys.exit("Data contains 32 bit signed integer and unsigned integer no valid pixel type")
        else:
            type = 8

if(verbose):
    print("Type is:")
    print(type)


typeDict = {
    0:"1_BIT" ,
    1:"2_BIT",
    2:"4_BIT",
    3:"8_BIT_UNSIGNED",
    4:"8_BIT_SIGNED",
    5:"16_BIT_UNSIGNED",
    6:"16_BIT_SIGNED",
    7:"32_BIT_UNSIGNED",
    8:"32_BIT_SIGNED",
    9:"32_BIT_FLOAT",
    10:"64_BIT"}


pixelType = typeDict[int(type)]


#environment settings
arcpy.env.workspace = outDir
arcpy.env.snapRaster = refGrid
arcpy.env.outputCoordinateSystem = refGrid
arcpy.env.overwriteOutput = True
arcpy.env.extent = refGrid
arcpy.env.cellsize = refGrid



if forceToExtent:
    out = tempName
else:
    out = outName


#stiching
arcpy.MosaicToNewRaster_management(tilePaths,outDir, out,"#",pixelType,"#","1")
# The above would work in ArcGIS 10.1 or 10.2 but ArcGIS 10.0 only processes the first 10 panes.


final = os.path.join(outDir, outName)

if forceToExtent:
    print "Using Map Algebra Kludge to expand extent"
    temp = Raster(out) + Raster(refGrid) * 0
    temp.save(final)
    print("Deleting intermediate")
    arcpy.Delete_management(tempName)



if buildPyramids:
    print("Building pyramids")
    arcpy.env.pyramid = pyramidEnvString
    arcpy.BuildPyramids_management(final)


open("[completedfile]", 'a').close()   #  so that R can verify the script completed
