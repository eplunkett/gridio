test_that("mergetables works", {
  
  # One dimensional
  a <- table(1:10)
  b <- table(1:20)
  expect_no_error(r1 <- mergetables(a, b))

  r2 <- table(c(1:10, 1:20))
  
  
  expect_no_error(r3 <- mergetables(a, b, numeric=TRUE))
  expect_equal(r3, r2)
  
  # Three dimensonal
  r4 <- mergetables(table(20:29, rep(1, 10), rep(1, 10)),
              table(20:29, c(1:5, 1:5), rep(1, 10)), 
              table(20:29, rep(2, 10), rep(2, 10)),
              numeric=TRUE)
  
  r5 <- table(c(20:29,20:29,20:29), 
               c(rep(1, 10),c(1:5, 1:5), rep(2, 10)), 
               c( rep(1, 10),  rep(1, 10),  rep(2, 10))
               )
  expect_equal(r4, r5)
    
  # With names 1d
  a <- table(a = 1:10)
  b <- table(a = 1:20)
  expect_no_error(r1 <- mergetables(a, b, numeric = TRUE))
  r2 <- table(a = c(1:10, 1:20))
  expect_equal(r1, r2)
  
  # With names 3d
  r4 <- mergetables(table(a = 20:29, b = rep(1, 10), c = rep(1, 10)),
                    table(a = 20:29, b = c(1:5, 1:5), c = rep(1, 10)), 
                    table(a = 20:29, b = rep(2, 10), c = rep(2, 10)),
                    numeric=TRUE)
  
  r5 <- table(a = c(20:29,20:29,20:29), 
              b = c(rep(1, 10),c(1:5, 1:5), rep(2, 10)), 
              c= c( rep(1, 10),  rep(1, 10),  rep(2, 10))
  )
  expect_equal(r4, r5)
  
})
