


#' Helper function to lookup PID's associated with grd_server and svr_guard
#' Used to help cleanup tests.
#'
#' @return
#' @export
#'
#' @examples
lookupgridserverpids <- function(){
  
  a <- system2("pslist",  stdout = TRUE) 
  a <- a[8:length(a)]
  a[1] <- gsub("CPU Time", "CPU_Time", a[1])
  a[1] <- gsub("Elapsed Time", "Elapsed_Time", a[1])
  
  
  a <- gsub("[[:blank:]]+", "\t", a)
  a <- a[ c(1, grep("grd_server|svr_guard", a))]
  
  b <-  read.table(text = a, sep = "", header = TRUE)
  return(b)
}