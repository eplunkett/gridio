test_that("gridlist works", {
  f <- init_example()
  example_dir <- dirname(f)
  
  # Floaing point
  expect_no_error(l <- gridlist(example_dir))
  expect_equal(l, "dem.tif")
  
  # Empty directory
  dir <- local_test_dir()
  expect_equal(gridlist(dir), character(0))
  
  # Text file - is ignored
  writeLines("test", file.path(dir, "lines.txt"))
  expect_equal(gridlist(dir), character(0))
  
  # Fake tif - shows up in list
  writeLines("test", file.path(dir, "lines.tif"))
  expect_equal(gridlist(dir), "lines.tif")
  
})
