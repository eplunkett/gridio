test_that("getwindow works in local mode", {
  f <- init_example()
  expect_no_error(w <- getwindow())
  expect_true(w$set)
  expect_equal(as.numeric(w[-6]), c(104130, 890190, 256, 324, 30))
})
