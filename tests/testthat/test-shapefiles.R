test_that("reading and writing point shapefiles works", {
  dir <-  withr::local_tempdir("shapefiles")
  pf1 <- system.file("exampledata/points.shp", package = "gridio2")
  expect_no_error(pts1 <- readshape(pf1, quiet = TRUE))
  pf2 <- file.path(dir, "points2.shp")
  expect_no_error(writeshape(pts1, pf2))
  expect_no_error(pts2 <- readshape(pf2, quiet = TRUE))
  expect_equal(pts1, pts1)
})


test_that("writing and reading polygon shapefiles works", {
  dir <-  withr::local_tempdir("polyshapefiles")
  dem <- init_example()
  tilepolys <- polygonizetilescheme()  

  skip_if_not_installed("sf")
  out <- file.path(dir, "tiles.shp")
  expect_no_error(writeshape(tilepolys, out))
  expect_no_error(tilepolys2 <- readshape(out, quiet = TRUE))

  # Note their are subtle differences in the wkt CRS
  # That don't constitute a different projection
  sf::st_crs(tilepolys) <- sf::st_crs(tilepolys2)
  
  expect_equal(tilepolys, tilepolys2)
  
})
