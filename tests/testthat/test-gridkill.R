test_that("gridkill works on floating points", {
  f <- init_example()
  dir <- local_test_dir("gridkill")
  out <- file.path(dir, "kill.tif")
  t <- readtile(f)
  l1 <- list.files(dir, all.files = TRUE)
  writetile(t, out) 
  l2 <- list.files(dir,all.files = TRUE)
  griddescribe(f, getstats = TRUE)
  l3 <- list.files(dir, all.files = TRUE)
  gridkill(out)
  l4 <- list.files(dir, all.files = TRUE)
  expect_equal(l4, l1)
  
})
