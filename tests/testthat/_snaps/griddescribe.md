# griddescribe works with floating point

    Code
      description
    Output
      A grid description with elements:
        nrow     : 256 
        ncol     : 324 
        yll      : 890190 
        xll      : 104130 
        cellsize : 30 
        type     : real 
        min      : 30.065311431885 
        max      : 307.58804321289 
        mean     : 71.39495148028 
        sd       : 49.143446259288 
        mosaic   : FALSE 

# griddescribe works with integer TIFF

    Code
      description
    Output
      A grid description with elements:
        nrow     : 256 
        ncol     : 324 
        yll      : 890190 
        xll      : 104130 
        cellsize : 30 
        type     : real 
        min      : NA 
        max      : NA 
        mean     : NA 
        sd       : NA 
        mosaic   : FALSE 

