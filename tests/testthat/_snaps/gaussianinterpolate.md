# gaussianinterpolate works with object in memory

    Code
      result_extent1
    Output
           xll      yll     nrow     ncol cellsize 
             0        0        5        5       10 

---

    Code
      vals1
    Output
      [1] 0.000527 0.002689 0.035735

---

    Code
      result_extent2
    Output
           xll      yll     nrow     ncol cellsize 
             0        0        5        5       10 

---

    Code
      vals2
    Output
      [1] 0.000527 0.002689 0.035735

# gaussianinterpolate works with files

    Code
      round(values, 2)
    Output
       [1] 104.66  90.62  85.57  84.52 124.10  88.06  89.06 157.50  88.85 149.03
      [11] 124.20 136.35  90.87  82.76  98.13 125.45 162.36  84.64 117.87 101.43
      [21]  85.82 123.61  92.35 114.65 112.85  82.88 139.08  79.33  91.69  79.33

