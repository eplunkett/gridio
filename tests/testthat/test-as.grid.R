test_that("Grid to terra::SpatRaster conversion works", {
  p <- init_example()
  g <- readtile(p)
  expect_silent(r <- rast(g))
  expect_s4_class(r, "SpatRaster")
  expect_silent(g2 <- as.grid(r))
  expect_equal(g, g2, ignore_attr = TRUE)
  expect_s3_class(g, "grid")
  expect_equal(terra::values(r)[1000:1010], t(g$m)[1000:1010])
})

