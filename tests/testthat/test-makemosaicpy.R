test_that("makemosaicpy works with sf", {
  dir <- local_test_dir("makemosaic")
  dem <- init_example(logdir = dir)
  outmosaic <- file.path(dir, "outmosaic")
  
  #setup for line by line execution
  source = dem
  rowsize = 100
  colsize  = 100
  mosaic = outmosaic

  skip("Testing makemosaicpy requires ESRI software. Skipped.")
  # Note I think we don't need this function anymore but am keeping it around
  # in case we run into old ESRI grid based mosaics.  
  # I updated it to use sf instead of sp but haven't fully tested.
  # I did manually test the changed code though.
  
  expect_no_error(makemosaicpy(dem, outmosaic, rowsize, colsize))  
  
})
