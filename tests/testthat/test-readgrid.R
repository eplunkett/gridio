test_that("readgrid works with floating point", {
  f <- init_example()
  a <- readtile(f)
  expect_equal(a$m[1:5], c(33.5461120605469, 33.7385940551758,
                           31.9955787658691, 31.9955787658691, 
                           31.9955787658691) )
    
})

test_that("readgrid works with integer", {
  f <- init_example()
  dir <- local_test_dir("readgrid")

  t1 <- newtile()
  t1$m[, ]<- seq_len(prod(t1$nrow, t1$ncol))
  p <- file.path(dir, "int.tif")

  expect_no_error( writetile(t1, path = p, as.integer = TRUE))
  
  expect_true(file.exists(p))
  
  expect_no_error(t2 <- readtile(path = p))
  expect_equal(t1, t2)
})


