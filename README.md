
# gridio2

<!-- badges: start -->
<!-- badges: end -->

The goal of the package is to facilitate calculations and parallel
processing on raster data that may be too large to fit into memory.

Functions are provided to read and write entire grids (`readgrid`, `writegrid`),  
arbitrary portions of grids (`readblock`, `writeblock`); and to break grids into 
uniform, regularly spaced tiles and read and write one tile at a time 
optionally with a buffer (`tileinit`, `readtile`, `writetile`).

**gridio2**  was built to support ecological modeling involving many 
raster files, with large extents.  It requires that
all the grids are coincident (matching is extent, cellsize, and cell alignment),
thus it doesn't perform well with mixed extents and provides no means for 
reprojecting grids.

Parallel processing of grids is permitted (with scaling limitations) by 
optionally setting up a gridserver to which multiple R threads are able to 
connect for interlaced read and write operations via a queue.
 
Ethan Plunkett

